<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

  <?php if ($page == 0): ?>
    <!--  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>  -->
  <?php endif; ?>
  <?php print _ult_main_returnlink($node, 'Leagues'); ?>
  <div id="ult-leagueinfo-container">
    <fieldset class="ult-fieldset"><legend><?php print $title . ' - ' . t('League Information'); ?></legend>
      <div class="ult-fieldset-content">
        <div class="content clear-block">
          <?php print $content ?>
        </div>
      </div>
    </fieldset>
  </div>
  <?php if ($ult_division_list): ?>
    <div id="ult-divisionlist-container">
      <fieldset class="ult-fieldset"><legend><?php print t('Divisions'); ?></legend>
        <div class="ult-fieldset-content">
          <?php print $ult_division_list; ?>
        </div>
      </fieldset>
    </div>
  <?php endif; ?>
  <?php if ($ult_team_list): ?>
    <div id="ult-teamlist-container">
      <fieldset class="ult-fieldset"><legend><?php print t('Teams'); ?></legend>
        <div class="ult-fieldset-content">
        <?php print $ult_team_list; ?>
        </div>
      </fieldset>
    </div>
  <?php endif; ?>
  <div class="clear-block"></div>
</div>


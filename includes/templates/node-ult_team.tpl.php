<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

  <?php if ($page == 0): ?>
   <!--  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>  -->
  <?php endif; ?>

  <?php print _ult_main_returnlink($node, 'TeamParent'); ?>
  <div id="ult-team-container">
    <fieldset class="ult-fieldset"><legend><?php print $title . ' - ' . t('Team Information'); ?></legend>
      <div class="ult-fieldset-content">
        <div class="content clear-block">
          <?php print $content ?>
          <?php if ($ult_team_captains) { print $ult_team_captains; } ?>
          <?php if ($ult_team_schedule) { print '<br /><br />' . $ult_team_schedule; } ?>
        </div>
      </div>
    </fieldset>
  </div>
  <div class="clear-block"></div>
</div>

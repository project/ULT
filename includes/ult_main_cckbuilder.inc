<?php
/**
 * @file
 * Installation helper function for the ULT suite of modules
 */


/**
 * Private function which creates a custom content type
 *
 * This module leverages the 'install_content_copy_import_from_file' function
 * from the Install_Profile_API module, which in turn depends on the
 * content_copy module included in CCK.  Content definitions are stored in their
 * individual contenttype.cck files within the /contenttypes directory.
 *
 * @param $contenttype
 *   The machine name of the content type to be created
 * @param $modulename
 *   The name of the module calling the function.  Is used to determine the
 *     correct module path for finding the content type cck definition
 *     file.
 *   Defaults to 'ult_main'
 *
 */
function _ult_main_create_ccktype($contenttype, $modulename = 'ult_main') {
  // CCK content_copy.module may not be enabled, so make sure it is included
  require_once './' . drupal_get_path('module', 'content') . '/modules/content_copy/content_copy.module';
  // Check for ult_profile content type
  $types = content_types($contenttype);
  module_load_include('inc', 'install_profile_api', 'contrib/content_copy');
  $filename = drupal_get_path('module', $modulename) . '/contenttypes/' . $contenttype . '.cck';
  if ($types['type']) {
    // Cck content type already exists.  Update it to the new definition
    install_content_copy_import_from_file($filename, $contenttype);
  }
  else {
    // Cck content type doesn't exist.  Create one.
    install_content_copy_import_from_file($filename);
  }
}

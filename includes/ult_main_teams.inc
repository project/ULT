<?php

/**
 * @file
 * Page generation functions related to ULT_Team nodes.
 */

/**
 * Generates the 'Create New Team' page
 *
 * @param $node
 *   The node object representing the league/division that the team is
 *     being created within
 *
 * @return $output
 *   The rendered page output
 */
function ult_main_team_create_page($node) {
  $output .= _ult_main_returnlink($node);
  $output .= '<div class="ult-teamcreate-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Create New Team') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  global $user;
  $node = new stdClass();
  $node->name = $user->name;
  $node->uid = $user->uid;
  $node->type = 'ult_team';
  module_load_include('inc', 'node', 'node.pages');
  $output .= drupal_get_form('ult_team_node_form', $node);
  $output .= '</div></fieldset></div>';
  return $output;
}

/**
 * Generates the 'edit team' page for an individual 'ULT Team' node
 *
 * @param $node
 *   The node object representing the team being edited
 *
 * @return
 *   The rendered 'edit team' page
 *
 */
function ult_main_team_edit_page($node) {
  _ult_main_checktype($node, 'ult_team');
  $output .= _ult_main_returnlink($node);
  $output .= '<div class="ult-teamedit-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Edit Team Information') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  module_load_include('inc', 'node', 'node.pages');
  $output .= drupal_get_form('ult_team_node_form', $node);
  $output .= '</div></fieldset></div>';
  return $output;
}

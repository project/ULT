<?php

/**
 * @file
 * Administrative settings forms for the "ULT Core" module
 */

/**
 * Implementation of hook_form().
 *
 * @return An array containing the form elements to be displayed in the settings form.
 */
function ult_main_config_form() {
  $form = array();
  $form['ult'] = array(
    '#type' => 'fieldset',
    '#title' => 'ULT Administrative settings',
  );
  $form['ult']['ult_orgname'] = array(
    '#type' => 'textfield',
    '#title' => t('Organization Name'),
    '#default_value' => variable_get('ult_orgname', t('Ultimate League Tool')),
    '#size' => 60,
    '#required' => TRUE,
    '#description' => t('Please enter the name of the organization or an overall title which describes the leagues managed on this site.'),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_form().
 *
 * @return An array containing the form elements to be displayed in the settings form.
 */
function ult_main_configteam_form() {
  $form = array();
  $form['ult_captains'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Team Captain settings'),
    '#weight' => -3,
  );
  $description = t('Team captains, if used, are granted a higher level of access which allows them to manage their own teams.');
  $description .= '<br />';
  $description .= t('Users with the "administer ULT leagues" permission will have captain-level access on all teams, regardless of the value of this setting.');
  $form['ult_captains']['ult_enablecaptains'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Team Captains'),
    '#description' => $description,
    '#default_value' => variable_get('ult_enablecaptains', 1),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  if (module_exists('contact')) {
    $form['contactinfo'] = array(
      '#type' => 'fieldset',
      '#title' => t('ULT Contact information settings'),
      '#weight' => -2,
    );
    $options = array('all' => t('Show link for all authenticated users'), 'none' => t('Do not display contact link'));
    if (module_exists('ult_rosters')) {
      $options['team'] = t('Show link for team members only');
    }
    $description = t('Determines whether and to whom the captains "contact" link will be shown to on the team page.');
    $description .= '<br />';
    $description .= t('Note: The user must have their personal contact form enabled for the contact link to appear.');
    $form['contactinfo']['ult_contactcaptains'] = array(
      '#type' => 'select',
      '#title' => t('Show captains "contact" link'),
      '#options' => $options,
      '#description' => $description,
      '#default_value' => variable_get('ult_contactcaptains', 'all'),
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
  }
  return system_settings_form($form);
}









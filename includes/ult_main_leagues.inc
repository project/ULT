<?php

/**
 * @file
 * Page generation functions related to ULT_League nodes.
 */

/**
 * Generates the 'Create New League' page
 *
 * @return $output
 *   The rendered 'create new leagues' page
 */
function ult_main_league_create_page() {
  $output .= _ult_main_returnlink($node, 'Leagues');
  $output .= '<div class="ult-leaguecreate-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Create New League') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  global $user;
  $node = new stdClass();
  $node->type = 'ult_league';
  $node->name = $user->name;
  $node->uid = $user->uid;
  module_load_include('inc', 'node', 'node.pages');
  $output .= drupal_get_form('ult_league_node_form', $node);
  $output .= '</div></fieldset></div>';
  return $output;
}

/**
 * Generates the 'ULT Leagues' summary page
 *
 * @return $output
 *   The rendered 'leagues' summary page
 */
function ult_main_leagues_page() {
  $output .= '<div class="ult-leaguelist-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . variable_get('ult_orgname', t('Ultimate League Tool')) . t(' Leagues') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  $output .= views_embed_view('ult_leaguelist');
  $output .= '</div></fieldset></div>';
  return $output;
}

/**
 * Generates the 'edit league' page for an individual 'ULT League' node
 *
 * @param $node
 *   The $node object for the league being edited
 *
 * @return $output
 *   The rendered 'edit league' page
 */
function ult_main_league_edit_page($node) {
  _ult_main_checktype($node, 'ult_league');
  $output .= _ult_main_returnlink($node);
  $output .= '<div id="ult-leagueedit-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Edit League Information') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  module_load_include('inc', 'node', 'node.pages');
  $output .= drupal_get_form('ult_league_node_form', $node);
  $output .= '</div></fieldset></div>';
  return $output;
}

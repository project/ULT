<?php
/**
 * @file
 * Administrative settings for the ULT_Logos module
 */

// ********************************************************
// *                                                      *
// *                  Form Definitions                    *
// *                                                      *
// ********************************************************
/**
 * Implements hook_form().
 *
 * @return
 *   An array containing the form elements to be displayed in the 'Logos' settings form.
 */
function ult_logos_config_form() {
  $form['logos'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Logo Settings'),
  );
  $form['logos']['changetext'] = array(
    '#type' => 'markup',
    '#value' => t('Note: Changes to the logo settings may only take effect once a node has been re-saved.'),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  // Verify required modules exist
  if (!module_exists('imagefield') || !module_exists('imagecache')) {
    $form['logos']['modulescheck'] = array(
      '#type' => 'markup',
      '#value' => t('Module Check:'),
      '#weight' => 1,
      '#prefix' => '<span class="form-item"><label>',
      '#suffix' => '</label></span>',
    );
    $form['logos']['modules'] = array(
      '#type' => 'markup',
      '#value' => t('Required modules not enabled'),
      '#weight' => 2,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
    $form['logos']['modulesfix'] = array(
      '#type' => 'markup',
      '#value' => '<div>' . t('To activate support for league/team logo images, <a href="!url">enable</a> the <a href="http://drupal.org/project/cck">Content</a>, <a href="http://drupal.org/project/imagefield">CCK Image field</a>, and <a href="http://drupal.org/project/imagecache">Imagecache</a> modules.', array('!url' => url('admin/build/modules'))) . '</div>',
      '#weight' => 3,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
    // Missing modules.  Return form prompting user to install required modules.
    return system_settings_form($form);
  }
  // Module check passed
  // Verify required Imagecache presets exist
  if (!(_ult_logos_checklogopresets())) {
    $form['logos']['prefixcheck'] = array(
      '#type' => 'markup',
      '#value' => t('Imagecache Presets Check:'),
      '#weight' => 4,
      '#prefix' => '<span class="form-item"><label>',
      '#suffix' => '</label></span>',
    );
    $form['logos']['presets'] = array(
      '#type' => 'markup',
      '#value' => t('ImageCache presets do not exist.'),
      '#weight' => 5,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
    $form['logos']['presetfix'] = array(
      '#type' => 'markup',
      '#value' => t('Please ensure that the following ImageCache presets have been created on this site:  ult_logothumb, ult_logosmall, and ult_logofull'),
      '#weight' => 6,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
    // Missing presets.  Return form prompting user to generate required Imachecache presets.
    return system_settings_form($form);
  }
  // Verify requied Imagefield fields exist on each content type
  module_load_include('inc', 'content', 'includes/content.crud');
  // Verify League Logo Image Support (does imagefield exist for content type?)
  $form['logos']['league'] = array(
    '#type' => 'fieldset',
    '#title' => t('League Logo Image Support'),
    '#collapsible' => FALSE,
    '#weight' => 7,
  );
  if (!((bool) count(content_field_instance_read(array('field_name' => 'field_ult_logoimage', 'type_name' => 'ult_league'))))) {
    // Missing 'ult_logoimage' field on 'ult_league' content type.  Show as 'inactive'.
    $form['logos']['league']['Leaguesupport'] = array(
      '#type' => 'markup',
      '#value' => t('Not active.  To enable league logo image support, <a href="!url">click here</a>.', array('!url' => url('admin/settings/ult/logos/enableleague'))),
      '#weight' => 8,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
  }
  else {
    // 'ult_logoimage' field exists on 'ult_league' content type.  Display league checkbox.
    $form['logos']['league']['ult_leaguelogos'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable League Logos'),
      '#default_value' => variable_get('ult_leaguelogos', FALSE),
      '#weight' => 9,
    );
    if (user_access('administer content types')) {
      // Link to field configuration page to set default league logo image
      $form['logos']['league']['ult_leaguedefaultlogo'] = array(
        '#type' => 'markup',
        '#value' => t('Use the <a href="!url">field configuration page</a> to set/modify the default league logo image.', array('!url' => url('admin/content/node-type/ult-league/fields/field_ult_logoimage'))),
        '#weight' => 10,
      );
    }
  }
  // Verify Division Logo Image Support (does imagefield exist for content type?)
  if (module_exists('ult_divisions')) {
    $form['logos']['division'] = array(
      '#type' => 'fieldset',
      '#title' => t('Division Logo Image Support'),
      '#collapsible' => FALSE,
      '#weight' => 11,
    );
    if (!((bool) count(content_field_instance_read(array('field_name' => 'field_ult_logoimage', 'type_name' => 'ult_division'))))) {
      // Missing 'ult_logoimage' field on 'ult_division' content type.  Show as 'inactive'.
      $form['logos']['division']['Divisionsupport'] = array(
        '#type' => 'markup',
        '#value' => t('Not active.  To enable division logo image support, <a href="!url">click here</a>.', array('!url' => url('admin/settings/ult/logos/enabledivision'))),
        '#weight' => 12,
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
    }
    else {
       // 'ult_logoimage' field exists on 'ult_division' content type.  Display division checkbox.      // Display divisioncheckbox
       $form['logos']['division']['ult_divisionlogos'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable Division Logos'),
        '#default_value' => variable_get('ult_divisionlogos', FALSE),
        '#weight' => 13,
      );
      if (user_access('administer content types')) {
        // Link to field configuration page to set default division logo image
        $form['logos']['division']['ult_divisiondefaultlogo'] = array(
          '#type' => 'markup',
          '#value' => t('Use the <a href="!url">field configuration page</a> to set/modify the default division logo image.', array('!url' => url('admin/content/node-type/ult-division/fields/field_ult_logoimage'))),
          '#weight' => 14,
        );
      }
    }
  }
  // Verify Team Logo Image Support (does imagefield exist for content type?)
  $form['logos']['team'] = array(
    '#type' => 'fieldset',
    '#title' => t('Team Logo Image Support'),
    '#weight' => 15,
    '#collapsible' => FALSE,
  );
  if (!((bool) count(content_field_instance_read(array('field_name' => 'field_ult_logoimage', 'type_name' => 'ult_team'))))) {
    // Missing 'ult_logoimage' field on 'ult_team' content type.  Show as 'inactive'.
    $form['logos']['team']['Teamsupport'] = array(
      '#type' => 'markup',
      '#value' => t('Not active.  To enable team logo image support, <a href="!url">click here</a>.', array('!url' => url('admin/settings/ult/logos/enableteam'))),
      '#weight' => 16,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
  }
  else {
      // 'ult_logoimage' field exists on 'ult_team' content type.  Display team checkbox.
     $form['logos']['team']['ult_teamlogos'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Team Logos'),
      '#default_value' => variable_get('ult_teamlogos', FALSE),
      '#weight' => 17,
    );
    if (user_access('administer content types')) {
      // Link to field configuration page to set default team logo image
      $form['logos']['team']['ult_teamdefaultlogo'] = array(
        '#type' => 'markup',
        '#value' => t('Use the <a href="!url">field configuration page</a> to set/modify the default team logo image.', array('!url' => url('admin/content/node-type/ult-team/fields/field_ult_logoimage'))),
        '#weight' => 18,
      );
    }
  }
  if (user_access('configure ULT settings')) {
    $form['logos']['moderation'] = array(
      '#type' => 'fieldset',
      '#title' => t('ULT Logo Moderation'),
      '#weight' => 19,
      '#collapsible' => FALSE,
    );
    $form['logos']['moderation']['modlink'] = array(
      '#type' => 'markup',
      '#value' => t('Visit the <a href="!url">ULT Logo Moderation Page</a> to manage all of the logos currently active on the site.', array('!url' => url('admin/settings/ult/logos/moderate'))),
      '#prefix' => '<div>',
      '#suffix' => '</div>',
      '#weight' => 20,
    );
  }
  return system_settings_form($form);
}

/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 *
 * @return
 *    An array containing the form elements to be displayed in the node edit form.
 */
function ult_logos_moderation_form($form_state) {
  $form = array();
  // Call private function which loads all logos on site
  $logos = _ult_logos_retrievelogos();
  // Generate display of each logo with checkbox (and parent node information?)
  $preset = 'ult_logothumb';
  $form['logos'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Logos'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  foreach ($logos as $logo) {
    $form['logos']['logo-' . $logo['nid']]['image' . $logo['nid']] = array(
      '#type' => 'markup',
      '#value' => '<div class="ult_modlogoimage">' . theme('imagecache', $preset, $logo['file'], $logo['title'], $logo['title']) . '</div>',
      '#prefix' => '<div class="ult_modlogocontainer">',
    );
    $form['logos']['logo-' . $logo['nid']]['checkbox' . $logo['nid']] = array(
      '#type' => 'checkbox',
      '#title' => '<span class="ult_modlogo_nodetype">' . $logo['nodetype'] . ':&nbsp;&nbsp;&nbsp;</span>' . $logo['title'] ,
      '#suffix' => '<div class="clear-block"></div></div>',
    );
  }
  // 'Delete selected' button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Remove selected images'),
    '#prefix' => '<div class="clear-both"></div>',
  );
  return $form;
}

/**
 * Validates the ult_logos_moderation form
 *
 * @param $form
 *    form to validate
 * @param $form_state
 *    A keyed array containing the current state of the form.
 */
function ult_logos_moderation_form_validate($form, &$form_state) {

}

/**
 * Submits the ult_logos_moderation form
 *
 * @param $form
 *    form to submit
 * @param $form_state
 *    A keyed array containing the current state of the form.
 */
function ult_logos_moderation_form_submit($form, &$form_state) {
  // Retrieve list of images to be deleted
  $removelogos = array();
  foreach ($form_state['values']['logos'] as $key => $item) {
    $nid = str_replace("logo-", '', $key);
    if ($item['checkbox' . $nid] == 1) {
      $removelogos[] = $nid;
    }
  }
  foreach ($removelogos as $nodeid) {
    $node = node_load($nodeid);
    unset($node->field_ult_logoimage);
    node_save($node);
    // Need to override the custom redirect routines when saving ULT node types
    unset($_SESSION['ult_redirect']);
  }
}

/**
 * Generates the ult_logos_moderation page
 */
function ult_logos_moderation_page() {
  $output .= drupal_get_form('ult_logos_moderation_form');
  return $output;
}

// ********************************************************
// *                                                      *
// *                 Private Utilities                    *
// *                                                      *
// ********************************************************
/**
 * Private function used to verify required Imagecache presets are present on the site
 */
function _ult_logos_checklogopresets() {
  $presets = array('ult_logothumb', 'ult_logosmall', 'ult_logofull');
  foreach ($presets as $preset_name) {
    $preset = imagecache_preset_by_name($preset_name);
    if (empty($preset)) {
      return FALSE;
      break;
    }
  }
  return TRUE;
}

/**
 * Private function used to generate list of all logos currently active on the site
 */
function _ult_logos_retrievelogos() {
  $logos = array();
  $sql = "Select a.nid, a.field_ult_logoimage_fid as fid, b.filepath, c.title, c.type from {content_field_ult_logoimage} a, {files} b, {node} c where a.field_ult_logoimage_fid = b.fid and a.nid = c.nid and a.field_ult_logoimage_fid is not NULL group by fid order by type, title";
  $result = db_query($sql, $activetypes);
  // Loop through nodes, retrieving data on logo images
  while ($data = db_fetch_object($result)) {
    $logo['nid'] = $data->nid;
    $logo['fid'] = $data->fid;
    $logo['title'] = $data->title;
    $logo['file'] = $data->filepath;
    $logo['nodetype'] = ucwords(str_replace('ult_', '', $data->type));
    $logos[] = $logo;
  }
  // Return array of logo data
  return $logos;
}


<?php

/**
 * @file
 * Teams by Player page and functions related to the ULT_Rosters component of the Ultimate League Tool.
 */

/********************************************************
 *                                                      *
 *                 Page Declarations                    *
 *                                                      *
 ********************************************************/
/**
 * Generates the 'teams by player' page on a user profile
 *
 * @param $player
 *   A $user object representing the player
 *
 * @return $output
 *   The rendered page output
 */
function ult_rosters_teamsbyplayer_page($player) {
  $output .= '<div class="ult-playerteams-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t("@player's Teams", array('@player' => $player->name)) . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  $teams = ult_rosters_getteamsbyplayer($player);
  if (count($teams) > 0) {
    $headers = array(t('Team'), t('Role'));
    $output .= theme('table', $headers, $teams);
  }
  else {
    $output .= t("No teams found for this user.");
  }
  $output .= '</div>';
  $output .= '</fieldset>';
  $output .= '</div>';
  return $output;
}

/********************************************************
 *                                                      *
 *                 Utilities                            *
 *                                                      *
 ********************************************************/

/**
 * Utility function which generates the list of a user's teams
 *
 * @param $player
 *   A User object representing the given player
 */
function ult_rosters_getteamsbyplayer($player) {
  $teams = array();
  $captains = array();
  $sql = "Select distinct nid from {content_field_ult_teamcaptains} where field_ult_teamcaptains_uid = %d";
  $result = db_query($sql, $player->uid);
  while ($data = db_fetch_object($result)) {
    $teams[] = array(ult_teamname_theme($data->nid, TRUE), t('Captain'));
    $captains[] = $data->nid;
  }
  $sql = "Select b.field_ult_rosterteam_nid as teamnid from {content_type_ult_roster} b where b.nid IN (SELECT DISTINCT nid from {content_field_ult_rosterplayers} where field_ult_rosterplayers_uid = %d)";
  $result = db_query($sql, $player->uid);
  while ($data = db_fetch_object($result)) {
    if (!(in_array($data->teamnid, $captains))) {
      $teams[] = array(ult_teamname_theme($data->teamnid, TRUE), t('Player'));
    }
  }
  return $teams;
}



<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

  <?php if ($page == 0): ?>
   <!--  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>  -->
  <?php endif; ?>

  <?php print _ult_main_returnlink($node, 'RosterParent'); ?>
  <div id="ult-team-container">
    <fieldset class="ult-fieldset"><legend><?php print $title; ?></legend>
      <div class="ult-fieldset-content">
        <div class="content clear-block">
          <?php if ($ult_roster_status) { print $ult_roster_status; }?>
          <?php print $content ?>
          <?php if ($ult_roster_list) { print $ult_roster_list; } ?>
          <?php if ($ult_roster_submit) { print '<br />' . $ult_roster_submit; } ?>
        </div>
      </div>
    </fieldset>
  </div>
  <div class="clear-block"></div>
</div>

<?php

/**
 * @file
 * Roster management functions related to the ULT_Roster component of the Ultimate League Tool.
 */

// ********************************************************
// *                                                      *
// *                 Form Definitions                     *
// *                                                      *
// ********************************************************
/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 * @param $rosternode
 *   A node object representing the current team's roster
 *
 * @return
 *   An array containing the form elements to be displayed in the node edit form.
 */
function ult_rosters_manageroster_form($form_state, $rosternode) {
  if (!isset($form_state['storage']['confirm'])) {
    _ult_main_checktype($rosternode, 'ult_roster');
    $form = array();
    // If captains disabled, and roster locked, deny access to manage roster (nothing to do!)
    // This prevents rendering of the form with an empty 'options' box.
    if ((variable_get('ult_enablecaptains', 1) != 1) && ($rosternode->field_ult_rosterstatus[0]['value'] == 'Locked')) {
      $form['disabled'] = array(
        '#type' => 'fieldset',
        '#title' => t('Roster Status'),
        '#collapsible' => FALSE,
      );
      $form['disabled']['text'] = array(
        '#type' => 'markup',
        '#value' => t('This roster is currently locked.  No further changes (including player additions/removals) are allowed.'),
      );
      return $form;
    }
    $options = array();
    if (variable_get('ult_enablecaptains', 1) == 1) {
      $options = array('promote' => t('Promote to Captain status'), 'demote' => t('Remove Captain status'));
    }
    if ($rosternode->field_ult_rosterstatus[0]['value'] != 'Locked') {
        $options['delete'] = t('Remove from Roster');
    }
    $form['actions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Roster Update Actions'),
      '#collapsible' => FALSE,
      '#weight' => -2,
    );
    $form['actions']['options'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('With Selected'),
      '#size' => 1,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );
    $players = array();
    foreach ($rosternode->field_ult_rosterplayers as $player) {
      $players[$player['uid']] = '';
      $form['name'][$player['uid']] = array(
        '#value' => ult_playername_theme($player['uid'], FALSE),
      );
      if (variable_get('ult_enablecaptains', 1) == 1) {
        $teamnode = node_load($rosternode->field_ult_rosterteam[0]['nid']);
        $form['captain']['cap' . $player['uid']] = array(
          '#type' => 'checkbox',
          '#value' =>  _ult_main_iscaptain($teamnode, $player['uid']),
          '#disabled' => TRUE,
        );
      }
    }
    $form['players'] = array(
      '#type' => 'checkboxes',
      '#options' => $players,
    );
    $form['teamnid'] = array(
      '#type' => 'hidden',
      '#value' => $teamnode->nid,
    );
    $form['rosternid'] = array(
      '#type' => 'hidden',
      '#value' => $rosternode->nid,
    );
    return $form;
  }
  else {
    return confirm_form($form, '', 'ult/team/' . $rosternode->field_ult_rosterteam[0]['nid'] . '/roster/manageroster', t('Are you sure you wish to remove these players from your roster?') . '<br />' . t('This action can not be undone.') . '<br /><br />', t('Remove players from roster'), t('Cancel'));
  }
}

// ********************************************************
// *                                                      *
// *          Theme Hooks & Functions                     *
// *                                                      *
// ********************************************************
/**
 * Themes the ult_rosters_manageroster_form form.
 */
function theme_ult_manageroster_form($form) {
  // If we've prevented rendering of the form with an empty $options box, return the form.
  if (isset($form['disabled'])) {
    return drupal_render($form);
  }
  $header = array(
    '',
    t('Player'),
  );
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $header[] = t('Captain');
  }
  if (isset($form['players']) && is_array($form['players'])) {
    foreach (element_children($form['players']) as $key) {
      $row = array();
      $row[] = array('data' => drupal_render($form['players'][$key]), 'width' => '30');
      $row[] = drupal_render($form['name'][$key]);
      if (variable_get('ult_enablecaptains', 1) == 1) {
        $row[] = drupal_render($form['captain']['cap' . $key]);
      }
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No players on roster.'), 'colspan' => '2'));
  }
  $output .= drupal_render($form['actions']);
  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

// ********************************************************
// *                                                      *
// *                 Form Validation                      *
// *                                                      *
// ********************************************************
/**
 * Validates the ult_rosters_manageroster_form form
 *
 * @param $form form to validate
 * @param $form_state A keyed array containing the current state of the form.
 */
function ult_rosters_manageroster_form_validate($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    // Retrieve desired action
    $action = $form_state['values']['options'];
    // Retrieve list of checked players
    $players = array();
    foreach ($form_state['values']['players'] as $key => $value) {
      if ($value == $key) {
        $players[] = $key;
      }
    }
    if (count($players) < 1) {
      form_set_error('players', t('No players selected'));
    }
    else {
      switch ($action) {
        case 'promote':
          // Ensure player is not already a captain
          $teamnode = node_load(check_plain($form_state['values']['teamnid']));
          foreach ($players as $player) {
            if (_ult_main_iscaptain($teamnode, $player)) {
              form_set_error($player, t('Cannot promote a player who is already a captain'));
            }
          }
          break;
        case 'demote':
          // Ensure all players are already captains
          $teamnode = node_load(check_plain($form_state['values']['teamnid']));
          foreach ($players as $player) {
            if (!(_ult_main_iscaptain($teamnode, $player))) {
              form_set_error($player, t('Cannot demote a player who is not a captain'));
            }
          }
          // Ensure there is at least one captain remaining
          if (count($teamnode->field_ult_teamcaptains) == count($players)) {
            form_set_error('players', t('You must leave at least one player as captain.'));
          }
          break;
        case 'delete':
          // Ensure valid roster node has been passed
          // This will only happen if someone jacks with the form

          // TODO:  While added for security, is this really necessary?
          // $rosternode = node_load(check_plain($form_state['values']['rosternid']));
          // if (!(_ult_main_checktype($rosternode, 'ult_roster'))) {
          //  form_set_error('form', t('Invalid submission - possible hack attempt'));
          // }

          // Do not allow a captain to remove themself from the roster (to ensure
          // there is always at least one captain and to avoid permission-related
          // 404 redirect results)

          if (variable_get('ult_enablecaptains', 1) == 1) {
            $teamnode = node_load(check_plain($form_state['values']['teamnid']));
            global $user;
            if (_ult_main_iscaptain($teamnode, $user->uid)) {
              if (in_array($user->uid, $players)) {
                form_set_error('form', t('A captain may not remove themselves from the roster.'));
              }
            }
            // Ensure not all team captains have been deleted
            if (count($teamnode->field_ult_teamcaptains > 0)) {
              $allcaptains = TRUE;
              foreach ($teamnode->field_ult_teamcaptains as $captain) {
                if (!(in_array($captain['uid'], $players))) {
                  // At least one captain is left on the team
                  $allcaptains = FALSE;
                  break;
                }
              }
              if ($allcaptains) {
                form_set_error('form', t('You must leave at least one player as captain.'));
              }
            }
          }
          break;
      }
    }
  }
}

// ********************************************************
// *                                                      *
// *                 Form Submission                      *
// *                                                      *
// ********************************************************
/**
 * Processes the submitted ult_rosters_manageroster_form form
 *
 * @param $form
 *   form to submit
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function ult_rosters_manageroster_form_submit($form, &$form_state) {
  // Retrieve desired action
  if (isset($form_state['storage']['confirm'])) {
    $action = $form_state['storage']['values']['options'];
  }
  else {
    $action = $form_state['values']['options'];
  }
  // Provide confirmation form if removing players from roster
  if (($action == 'delete') && (!isset($form_state['storage']['confirm']))) {
    $form_state['storage']['values'] = $form_state['values'];
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  else {
    $uids = array();
    $players = array();
    if ($action == 'delete') {
      $uids = $form_state['storage']['values']['players'];
    }
    else { $uids = $form_state['values']['players']; }
    // Retrieve list of checked players
    $players = array();
    foreach ($uids as $key => $value) {
      if ($value == $key) {
        $players[] = $key;
      }
    }
    switch ($action) {
      case 'promote':
        $teamnode = node_load(check_plain($form_state['values']['teamnid']));
        foreach ($players as $player) {
          $teamnode->field_ult_teamcaptains[] = array('uid' => $player);
        }
        $_SESSION['ult_rosters_skipnodeapi'] = TRUE;
        node_save($teamnode);
        unset($_SESSION['ult_rosters_skipnodeapi']);
        drupal_set_message(t('Successfully promoted !count player(s) to captain.', array('!count' => count($players))), 'status');
        break;
      case 'demote':
        $teamnode = node_load(check_plain($form_state['values']['teamnid']));
        $captains = array();
        foreach ($teamnode->field_ult_teamcaptains as $captain) {
          if (!(in_array($captain['uid'], $players))) {
            $captains[] = array('uid' => $captain);
          }
        }
        $teamnode->field_ult_teamcaptains = $captains;
        $_SESSION['ult_rosters_skipnodeapi'] = TRUE;
        node_save($teamnode);
        unset($_SESSION['ult_rosters_skipnodeapi']);
        drupal_set_message(t('Successfully removed captain status for !count players.', array('!count' => count($players))), 'status');
        break;
      case 'delete':
        $rosternode = node_load(check_plain($form_state['storage']['values']['rosternid']));
        $teamnode = node_load(check_plain($form_state['storage']['values']['teamnid']));
        $uids = array();
        foreach ($rosternode->field_ult_rosterplayers as $uid) {
          if (!(in_array($uid['uid'], $players))) {
            $uids[] = array('uid' => $uid['uid']);
          }
        }
        $rosternode->field_ult_rosterplayers = $uids;
        // If any captains deleted, update the team node
        $uids = array();
        $updateflag = FALSE;
        foreach ($teamnode->field_ult_teamcaptains as $uid) {
          if (in_array($uid['uid'], $players)) {
            $updateflag = TRUE;
          }
          else {
            $uids[] = array('uid' => $uid);
          }
        }
        if ($updateflag) {
          $teamnode->field_ult_teamcaptains = $uids;
        }
        // Save updated nodes if necessary
        $_SESSION['ult_rosters_skipnodeapi'] = TRUE;
        node_save($rosternode);
        if ($updateflag) {
          node_save($teamnode);
        }
        unset($_SESSION['ult_rosters_skipnodeapi']);
        drupal_set_message(t('Successfully removed !count player(s) from roster.', array('!count' => count($players))), 'status');
        $form_state['rebuild'] = '';
        unset($form_state['storage']);
        break;
    }
  }
}



<?php

/**
 * @file
 * Roster management functions related to the ULT_Roster component of the Ultimate League Tool.
 */

// ********************************************************
// *                                                      *
// *                 Form Definitions                     *
// *                                                      *
// ********************************************************
/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 * @param $rosternode
 *   A node object represneting the current roster
 * @param $method
 *   A flag used to determine whether to treat the application as an application or direct join.
 *   Valid values are 'apply' (default) and 'direct'
 *
 * @return An array containing the form elements to be displayed in the node edit form.
 */
function ult_rosters_joinroster_form($form_state, $rosternode, $method = 'apply') {
  _ult_main_checktype($rosternode, 'ult_roster');
  $teamnode = node_load($rosternode->field_ult_rosterteam[0]['nid']);
  global $user;
  if ((module_exists('ult_waivers')) && (variable_get('ult_enablewaiver', 0) > 0)) {
    ult_waivers_check_waiver();
  }
  $form = array();
  $confirmtext = '';
  if ($method == 'direct') {
    $confirmtext = t('By clicking the button below, you will add yourself to the !team roster.', array('!team' => $teamnode->title));
  }
  else {
    $confirmtext = t('By clicking the button below, you will be submitting an application to join the !team roster.', array('!team' => $teamnode->title));
  }
  $confirmtext .= '<br /><br />';
  $confirmtext .= t('Are you sure you wish to continue?');
  $form['confirmtext'] = array(
      '#type' => 'markup',
      '#value' => $confirmtext,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
  );
  $form['user'] = array( '#type' => 'hidden', '#value' => $user->uid, );
  $form['rosternid'] = array( '#type' => 'hidden', '#value' => $rosternode->nid, );
  $form['teamnid'] = array( '#type' => 'hidden', '#value' => check_plain($rosternode->field_ult_rosterteam[0]['nid']));
  if ($method == 'direct') {
    $form['submit'] = array( '#type' => 'submit', '#value' => t('Join Roster'), );
  }
  else {
    $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit Application to Join Roster'), );
   }
  $form['cancel'] = array( '#type' => 'button', '#value' => t('Cancel'), '#executes_submit_callback' => TRUE, '#submit' => array('ult_rosters_joinroster_form_cancel'));
  return $form;
}

// ********************************************************
// *                                                      *
// *                 Form Validation                      *
// *                                                      *
// ********************************************************
/**
 * Validates the ult_rosters_joinroster_form form
 *
 * @param $form
 *   form to validate
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function ult_rosters_joinroster_form_validate($form, &$form_state) {
  // Confirm that user is allowed to join team
  $method = variable_get('ult_userrosteradd', 'apply');
  if (($method != 'apply') && ($method != 'direct')) {
    form_set_error('form', t('Current site settings do not allow for players to join rosters.'));
  }
  else {
    // Validate uid
    global $user;
    if (($user->uid) && ($form_state['values']['user'] != $user->uid)) {
      form_set_error('form', t('Invalid User ID'));
    }
    if (!(_ult_main_checktype(node_load(check_plain($form_state['values']['rosternid'])), 'ult_roster'))) {
      form_set_error('form', t('Invalid Roster ID'));
    }
  }
}

// ********************************************************
// *                                                      *
// *                 Form Submission                      *
// *                                                      *
// ********************************************************
/**
 * Processes the submitted ult_rosters_joinroster_form form
 *
 * @param $form
 *   form to submit
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function ult_rosters_joinroster_form_submit($form, &$form_state) {
  $method = variable_get('ult_userrosteradd', 'apply');
  if ($method == 'apply') {
    _ult_rosters_joinroster_apply($form, $form_state);
  }
  elseif ($method == 'direct') {
    _ult_rosters_joinroster_direct($form, $form_state);
  }
}

/**
 * Process application (called from ult_rosters_joinroster_form_submit)
 *
 * @param $form
 *   form to submit
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function _ult_rosters_joinroster_apply($form, &$form_state) {
  global $user;
  $rosternid = check_plain($form_state['values']['rosternid']);
  $teamnid = check_plain($form_state['values']['teamnid']);
  $teamnode = node_load($teamnid);
  $uid = check_plain($form_state['values']['user']);
  $rosternode = node_load($nid);
  // Check if an application or invitation exists
  if ($existing = _ult_rosters_hasinvite($uid, $rosternid)) {
    // Exisiting invite
    if ($existing == $user->uid) {
      drupal_set_message(t("You have already applied to join this team.  Check the status of your application on your player profile page."));
    }
    else {
      drupal_set_message(t("You already have an invitation to join this team.  You may accept the invitation via your player profile page."));
    }
  }
  else {
    // No invite - process submission
    $invite = new stdClass();
    $invite->uid = $uid;
    $invite->roster = $rosternid;
    $invite->referrer = $uid;
    $invite->invitedate = time();
    $invite->response = NULL;
    $invite->rejectdate = NULL;
    drupal_write_record('ult_roster_invites', $invite);
    drupal_set_message(t("Application submitted.  To view the status of your application, check the 'Invites' link on your Player Profile."));
    // Determine whether 'new application' notifications are required
    if (variable_get('ult_enablecaptains', 1) == 1) {
      $targets = variable_get('ult_appnotify', array('captain'));
    }
    else {
      $targets = variable_get('ult_appnotify', array('player'));
    }
    if (($targets['player'] == 'player') || ($targets['captain'] == 'captain') || ($targets['team'] == 'team')) {
      $params['teamnid'] = $teamnid;
      $params['teamname'] = check_plain(node_page_title(node_load($teamnid)));
      $params['name'] = ult_playername_theme($uid, FALSE);
      $params['userid'] = $uid;
      _ult_rosters_sendnotifications($targets, $params, 'app', $rosternode, $teamnode);
    }
  }
  $form_state['redirect'] = 'ult/team/' . $teamnid . '/roster';
}

/**
 * Process a direct roster join request (called from ult_rosters_joinroster_form_submit)
 *
 * @param $form
 *   form to submit
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function _ult_rosters_joinroster_direct($form, &$form_state) {
  global $user;
  $rosternid = check_plain($form_state['values']['rosternid']);
  $teamnid = check_plain($form_state['values']['teamnid']);
  $teamnode = node_load($teamnid);
  $uid = check_plain($form_state['values']['user']);
  $rosternode = node_load($rosternid);
  if (!(in_array(array('uid' => $uid), $rosternode->field_ult_rosterplayers))) {
    $rosternode->field_ult_rosterplayers[] = array('uid' => $uid);
    $_SESSION['ult_rosters_skipnodeapi'] = TRUE;
    node_save($rosternode);
    unset($_SESSION['ult_rosters_skipnodeapi']);
    drupal_set_message(t("You have been successfully added to the @team roster.", array('@team' => $teamnode->title)));
    // Determine whether 'new application' notifications are required
    $targets = array();
    if (variable_get('ult_enablecaptains', 1) == 1) {
      $targets = variable_get('ult_joinnotify', array('player', 'captain'));
    }
    else {
      $targets = variable_get('ult_joinnotify', array('player'));
    }
    if (($targets['player'] === 'player') || ($targets['captain'] === 'captain') || ($targets['team'] === 'team')) {
      drupal_set_message(t('Email notifications sent.'), 'status');
      $params['teamnid'] = $teamnid;
      $params['teamname'] = check_plain(node_page_title(node_load($teamnid)));
      $params['name'] = ult_playername_theme($uid, FALSE);
      $params['userid'] = $uid;
      _ult_rosters_sendnotifications($targets, $params, 'accept', $rosternode, $teamnode);
    }
  }
  $form_state['redirect'] = 'ult/team/' . $teamnid . '/roster';
}

/**
 * Custom 'cancel' submit function for ult_rosters_joinroster_form
 *
 * Does nothing but redirect to the roster page.
 *
 */
function ult_rosters_joinroster_form_cancel($form, &$form_state) {
  // Do nothing, and redirect to the roster page.
  $form_state['redirect'] = 'ult/team/' . check_plain($form_state['values']['teamnid']) . '/roster';
}


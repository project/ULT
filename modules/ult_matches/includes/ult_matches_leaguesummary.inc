<?php
/**
 * @file
 * 'League Summary' page generation function for the ULT_Matches module.
 */

/**
 * Generates the league/division 'Games Summary' page
 *
 * @param $node
 *   A node object representing the league/division being referenced
 *
 * @return $output
 *   The rendered 'league summary' page content
 */
function ult_matches_leaguesummary_page($node) {
  $output .= _ult_main_returnlink($node);
  $output .= '<div class="ult-upcominggames-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Upcoming Games within the next week') . '</legend>';
  $output .= views_embed_view('ult_upcominggames', 'block_1', $node->nid);
  $output .= '</fieldset></div>';
  $output .= '<div class="ult-pendinggames-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Recent Games still pending results') . '</legend>';
  $output .= views_embed_view('ult_pendinggames', 'default', $node->nid);
  $output .= '</fieldset></div>';
  $output .= '<div class="ult-recentgames-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Recent Game Results') . '</legend>';
  $output .= views_embed_view('ult_recentgames', 'block_1', $node->nid);
  $output .= '</fieldset></div>';
  return $output;
}

<?php
/**
 * @file
 * 'Challenge Summary' page generation function for the ULT_Matches module.
 */

/**
 * Generates the league/division 'Challenge Summary' page
 *
 * @param $node
 *   A node object representing the league/division being referenced
 *
 * @return $output
 *   The rendered 'challenge summary' page content
 */
function ult_matches_challengesummary_page($node) {
  $output .= _ult_main_returnlink($node);
  $output .= '<div class="ult-challengedgames-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Challenged Game Results') . '</legend>';
  $output .= views_embed_view('ult_challengedgames', 'default', $node->nid);
  $output .= '</fieldset></div>';
  return $output;
}

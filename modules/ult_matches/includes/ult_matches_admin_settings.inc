<?php

/**
 * @file
 * Administrative settings for ULT_Matches module
 */

/**
 * Implementation of hook_form().
 *
 * @return An array containing the form elements to be displayed in the settings form.
 */
function ult_matches_config_form() {
  $form = array();
  // Ult Game Results Settings Fieldset
  $form['ultresults'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Game Results settings'),
    '#collapsible' => FALSE,
    '#weight' => -1,
  );
  // Allow Ties
  $form['ultresults']['ult_allowties'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow tied scores'),
    '#default_value' => variable_get('ult_allowties', 1),
  );
  // Allow Defaults
  $form['ultresults']['ult_allowdefaults'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow defaults'),
    '#default_value' => variable_get('ult_allowdefaults', 1),
  );
  // Defaulted Game Scores fieldset
  $form['ultresults']['defaults'] = array(
    '#type' => 'fieldset',
    '#title' => t('Score to assign defaulted games'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Score for default win
  $form['ultresults']['defaults']['ult_defaultwinscore'] = array(
    '#type' => 'textfield',
    '#title' => t('Score to assign to winning team'),
    '#default_value' => variable_get('ult_defaultwinscore', 1),
    '#description' => t('What score should be entered for a default win'),
    '#size' => 5,
  );
  // Score for default loss
  $form['ultresults']['defaults']['ult_defaultlossscore'] = array(
    '#type' => 'textfield',
    '#title' => t('Score to assign to defaulting team'),
    '#default_value' => variable_get('ult_defaultlossscore', 0),
    '#description' => t('What score should be entered for a default loss'),
    '#size' => 5,
  );
  // Result Submission Fieldset
  $form['ultresultsub'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Game Result Submission settings'),
  );
  // Display submitter details on game node
  $form['ultresultsub']['ult_showresultsubmitinfo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Result Submission details on Game Node'),
    '#description' => t('Displays the time and date a result was added, as well as the user who added it.'),
    '#default_value' => variable_get('ult_showresultsubmitinfo', 1),
  );
  // Who may submit Game Results
  $options = array( 0 => t('Administrators only'), 1 => t('Anyone with "submit results" permission'));
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options[2] = t('Team captains only');
  }
  if (module_exists('ult_rosters')) {
    $options[3] = t('Any player from either team\'s roster');
  }
  $form['ultresultsub']['ult_resultsubmitter'] = array(
    '#type' => 'radios',
    '#title' => t('Who may submit game results'),
    '#options' => $options,
    '#default_value' => variable_get('ult_resultsubmitter', 0),
  );
  // Allow Cancels
  $form['ultresultsub']['ult_allowcancelgame'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable "cancel game" functionality'),
    '#description' => t('Allows users with the proper access to submit "game cancelled" results'),
    '#default_value' => variable_get('ult_allowcancelgame', 1),
  );
  // Allow Postponed games
  $form['ultresultsub']['ult_allowpostponegame'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable "postpone game" functionality'),
    '#description' => t('Allows users with the proper access to submit "game postponed" results'),
    '#default_value' => variable_get('ult_allowpostponegame', 1),
  );
  // Allow Reschedules
  $form['ultresultsub']['ult_allowreschedulegame'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable "reschedule game" functionality'),
    '#description' => t('Allows users with the proper access to submit "game rescheduled" results'),
    '#default_value' => variable_get('ult_allowreschedulegame', 1),
  );
  // Who may Cancel/Reschedule Games
  $options = array( 0 => t('Administrators only'), 1 => t('Anyone with "submit results" permission') );
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options[2] = t('Team Captains');
  }
  if (module_exists('ult_rosters')) {
    $options[3] = t('Any player from either team\'s roster');
  }
  $form['ultresultsub']['ult_maycancelmatch'] = array(
    '#type' => 'radios',
    '#title' => t('Who may cancel/postpone/reschedule games'),
    '#options' => $options,
    '#default_value' => variable_get('ult_maycancelmatch', 0),
  );
  // Create new game node for rescheduled games
  $form['ultresultsub']['ult_autocreaterescheduledgame'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically create game nodes for "rescheduled" games'),
    '#description' => t('Placeholders will be created to represent rescheduled games.'),
    '#default_value' => variable_get('ult_autocreaterescheduledgame', 0),
  );
  // Mark overdue games as 'default' after how many days?
  $description = t('Games which have not had results posted within this many days of the scheduled date ');
  $description .= t('will be considered "stale" and marked as a default for both teams.');
  $description .= t('<br />Note:  Enter "0" to disable this feature.');
  $form['ultresultsub']['ult_resultautoforfeit'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Game Result Submission window (days)'),
    '#default_value' => variable_get('ult_resultautoforfeit', 0),
    '#description' => $description,
  );
  // Game Result Challenge Settings
  $form['ultresultchal'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Game Result Challenge settings'),
  );
  // Who may challenge a game result?
  $options = array( 0 => t('Noone (Disable "challenge score" feature)'));
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options[1] = t('Team captains only');
  }
  if (module_exists('ult_rosters')) {
    $options[2] = t('Any player from either team\'s roster');
  }
  $form['ultresultchal']['ult_resultcontester'] = array(
    '#type' => 'radios',
    '#title' => t('Who may challenge a submitted game result'),
    '#options' => $options,
    '#default_value' => variable_get('ult_resultcontester', 0),
  );
  // Who may accept challenged game result?
  $options = array( 0 => t('League Admins only'), 1 => t('League Admins and original result submitter'), );
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options[2] = t('Opposing team captain');
  }
  if (module_exists('ult_rosters')) {
    $options[3] = t('Any player from the opposing team\'s roster');
  }
  $form['ultresultchal']['ult_resultaccepter'] = array(
    '#type' => 'radios',
    '#title' => t('Who may approve a game result challenge, thus replacing the originally submitted result?'),
    '#options' => $options,
    '#default_value' => variable_get('ult_resultaccepter', 1),
  );
  // How long after a result is posted may it be challenged?
  $form['ultresultchal']['ult_challengewindow'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Challenge Result window duration (days)'),
    '#default_value' => variable_get('ult_challengewindow', 15),
    '#description' => t('Results may only be challenged within a certain number of days after being posted.'),
  );
  // How long can an unaccepted challenge exist on the site?
  $form['ultresultchal']['ult_challengeexpires'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('Challenge expiry timer (days)'),
    '#default_value' => variable_get('ult_challengeexpires', 15),
    '#description' => t('Challenges which have not been accepted after a certain time period will expire and be deleted.  Set to "0" to disable this feature.'),
  );
  // Clear other challenges when a challege is accepted?
  $form['ultresultchal']['ult_deletechallengesonaccept'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delete other outstanding challenges when a challenge is accepted'),
    '#default_value' => variable_get('ult_deletechallengesonaccept', 0),
    '#description' => t('If there are more than one result challenges for a given game, determines whether to remove the others when one is accepted.'),
  );
  // Notifications Fieldset
  $form['ultnotify'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Game Notification Settings'),
    '#collapsible' => FALSE,
  );
  // League Coordinator Email Address
  $form['ultnotify']['ult_leaguecoordemail'] = array(
    '#type' => 'textfield',
    '#size' => 60,
    '#title' => t('League Coordinator Email addresses'),
    '#description' => t('An email address which will be used wherever "League Coordinator" is selected in the options below.'),
    '#default_value' => variable_get('ult_leaguecoordemail', variable_get('site_mail', ini_get('sendmail_from'))),
  );
  // Send form League Coordinator address?
  $form['ultnotify']['ult_sendfromleagueemail'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send notifications from League Coordinator address'),
    '#default_value' => variable_get('ult_sendfromleagueemail', 1),
    '#description' => t('If checked, uses the email address listed above in the "from" field of outgoing notifications.<br />If not, the default site email address is used.'),
  );
  // Hide addresses in sent notifications?
  $form['ultnotify']['ult_hidenotificationemails'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide user email addresses in sent notifications'),
    '#default_value' => variable_get('ult_hidenotificationemails', 1),
    '#description' => t('If checked, uses the "bcc" field to send notifications, thus preventing recipients from seeing other users email addresses.'),
  );
  // Provide 'game result due' notification
  $options = array();
  $options['league'] = t('League Coordinator');
  $default = array();
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options['homecaptain'] = t('Home team captain(s)');
    $options['awaycaptain'] = t('Away team captain(s)');
    $default = array('homecaptain' => 'homecaptain', 'awaycaptain' => 'awaycaptain');
  }
  $form['ultnotify']['ult_resultduenotify'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Provide "Game Result Due" notifications to:'),
    '#options' => $options,
    '#description' => t('Who should be notified when a game result is due for submission?'),
    '#default_value' => variable_get('ult_resultduenotify', $default),
  );
  // Provide 'game result due' reminders
  $form['ultnotify']['ult_resultduereminder'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => t('"Game Result Due" Reminder window'),
    '#description' => t('Provide an additional reminder notification when a game result is this many days overdue.  (Set to "0" to disable this feature.)'),
    '#default_value' => variable_get('ult_resultduereminder', 0),
  );
  // "Result Submitted" notification
  $options = array();
  $options['league'] = t('League Coordinator');
  $options['submitter'] = t('Result submitter');
  $default = array('');
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options['subcaptain'] = t('Submitting team captain(s)');
    $options['oppcaptain'] = t('Opposing team captain(s)');
    $default = array('subcaptain' => 'subcaptain', 'oppcaptain' => 'oppcaptain');
  }
  $form['ultnotify']['ult_resultnotify'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Provide "Game Result Submitted" notifications to:'),
    '#options' => $options,
    '#description' => t('Who should be notified when a game result is submitted'),
    '#default_value' => variable_get('ult_resultnotify', $default),
  );
  // "Result Challenged" Notifications
  $options = array();
  $options['league'] = t('League Coordinator');
  $options['submitter'] = t('Challenge submitter');
  $options['original'] = t('Original result submitter');
  $default = array('original' => 'original');
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options['subcaptain'] = t('Challenging team captain(s)');
    $options['oppcaptain'] = t('Opposing team captain(s)');
    $default = array('original' => 'original', 'subcaptain' => 'subcaptain', 'oppcaptain' => 'oppcaptain');
  }
  $form['ultnotify']['ult_challengenotify'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Provide "Game Result Challenged" notifications to:'),
    '#options' => $options,
    '#description' => t('Who should be notified when a game result challenge is submitted'),
    '#default_value' => variable_get('ult_challengenotify', $default),
  );
  // "Result Challenge Accepted" Notifications
  $options = array();
  $options['league'] = t('League Coordinator');
  $options['submitter'] = t('Challenge submitter');
  $options['accepter'] = t('Challenge accepter');
  $options['original'] = t('Original result submitter');
  $default = array('submitter' => 'submitter', 'original' => 'original');
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options['subcaptain'] = t('Challenging team captain(s)');
    $options['oppcaptain'] = t('Opposing team captain(s)');
    $default = array('submitter' => 'submitter', 'original' => 'original', 'subcaptain' => 'subcaptain', 'oppcaptain' => 'oppcaptain');
  }
  $form['ultnotify']['ult_acceptchallengenotify'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Provide "Game Challenge Accepted" notifications to:'),
    '#options' => $options,
    '#description' => t('Who should be notified when a game result challenge is accepted'),
    '#default_value' => variable_get('ult_acceptchallengenotify', $default),
  );
  // "Result Challenge Expired" Notifications
  $options = array();
  $options['league'] = t('League Coordinator');
  $options['submitter'] = t('Challenge submitter');
  $options['original'] = t('Original result submitter');
  $default = array('league' => 'league', 'submitter' => 'submitter');
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options['subcaptain'] = t('Challenging team captain(s)');
    $options['oppcaptain'] = t('Opposing team captain(s)');
    $default = array('league' => 'league', 'submitter' => 'submitter', 'subcaptain' => 'subcaptain', 'oppcaptain' => 'oppcaptain');
  }
  $form['ultnotify']['ult_challengeexpirenotify'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Provide "Game Challenge Expired" notifications to:'),
    '#options' => $options,
    '#description' => t('Who should be notified when a game result challenge has expired'),
    '#default_value' => variable_get('ult_challengeexpirenotify', $default),
  );
  $form['ult'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Game settings'),
  );
  $form['ult']['ult_matchstart'] = array(
    '#type' => 'ult_main_time',
    '#title' => t('Default Start Time for New Games'),
    '#default_value' => variable_get('ult_matchstart', array('hour' => '6', 'minute' => '00', 'meridiem' => 'pm')),
    '#required' => TRUE,
    '#description' => t('Typical game start time to be used as a default when adding new games'),
  );
  $form['ult']['ult_matchlength'] = array(
    '#type' => 'textfield',
    '#title' => t('Scheduled Game Length'),
    '#default_value' => variable_get('ult_matchlength', 90),
    '#description' => t('The average length of a league game, in minutes (used for back-to-back scheduling purposes)'),
    '#required' => TRUE,
    '#size' => 5,
  );
  return system_settings_form($form);
}

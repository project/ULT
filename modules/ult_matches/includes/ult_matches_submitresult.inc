<?php
/**
 * @file
 * Game Result submission functions for the ULT_Matches module.
 */
// ********************************************************
// *                                                      *
// *                 Page Definitions                     *
// *                                                      *
// ********************************************************
/**
 * Generates the 'Submit Result' page
 *
 * @param $gamenode
 *   A node object representing the game for which results are being submitted
 *
 * @return $output
 *   The rendered 'submit result' page content
 */
 function ult_matches_submitresult_page($gamenode) {
  $output .= _ult_main_returnlink($gamenode);
  $output .= '<fieldset><legend>';
  if ($gamenode->field_ult_gameresult[0]['value'] == 'Yes') {
    $output .= t('Challenge Game Result');
  }
  else { $output .= t('Enter Final Game Score'); }
  $output .= '</legend>';
  $output .= '<div class="ult_gameinfo_left">';
  if ($gamenode->field_ult_gameresult[0]['value'] == 'Yes') {
    $output .= ult_gameresult_theme(_ult_matches_getresultnid($gamenode->nid));
    $output .= t('Enter Challenge:') . '<br />';
    $output .= drupal_get_form('ult_matches_submitscore_form', $gamenode, 'challenge');
  }
  else {
    $output .= drupal_get_form('ult_matches_submitscore_form', $gamenode);
  }
  $output .= '<br />';
  $output .= '</div>';
  $output .= '<div class="ult_gameinfo_right">';
  $output .= '<br />';
  $output .= '<fieldset><legend>' . t('Game Information') . '</legend>';
  $output .= content_view_field(content_fields('field_ult_gamedate', 'ult_game'), $gamenode);
  $output .= content_view_field(content_fields('field_ult_gameleague', 'ult_game'), $gamenode);
  if (module_exists('ult_fields')) {
    $output .= content_view_field(content_fields('field_ult_gamefieldsite', 'ult_game'), $gamenode);
    $output .= content_view_field(content_fields('field_ult_gamefield', 'ult_game'), $gamenode);
  }
  $output .= '</fieldset>';
  $output .= '</div>';
  $output .= '</fieldset>';
  return $output;
}

/**
 * Generates the 'Challenge Result' page
 *
 * @param $gamenode
 *   A node object representing the game for which results are being challenged
 *
 * @return $output
 *   The rendered 'challenge result' page content
 */
 function ult_matches_challengeresult_page($gamenode) {
  $output .= _ult_main_returnlink($gamenode);
  $output .= ult_gameresult_theme(_ult_matches_getresultnid($gamenode->nid));
  $output .= drupal_get_form('ult_matches_submitscore_form', $gamenode, 'challenge');
  return $output;
}

/**
 * Generates the Cancel/Postpone/Reschedule Game page
 *
 * @param $gamenode
 *   A node object representing the game being cancelled/postponed/challenged
 *
 * @return $output
 *   The rendered 'Cancel Game' page content
 */
function ult_matches_cancelgame_page($gamenode) {
  $output .= _ult_main_returnlink($gamenode);
  $output .= ult_game_theme($gamenode->nid);
  $output .= drupal_get_form('ult_matches_cancelgame_form', $gamenode);
  return $output;
}

// ********************************************************
// *                                                      *
// *                 Form Definitions                     *
// *                                                      *
// ********************************************************
/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 * @param $gamenode
 *   A node object representing the game for which results are being submitted
 * @param $action
 *   A flag determining whether this is a normal entry or a result challenge
 *   Valid values are 'regular' and 'challenge'.  Defaults to 'regular'
 *
 * @return
 *   A form array containing the form elements to be displayed
 */
function ult_matches_submitscore_form($form_state, $gamenode, $action = 'regular') {
  if (!isset($form_state['storage']['confirm'])) {
    $form = array();
    if ($action == 'challenge') {
      $description = t('You are challenging the previously submitted game result.  Please enter your game result details using the above form.');
      $form['ult'] = array(
        '#type' => 'markup',
        '#value' => $description,
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
    }
    $form['result']['gameid'] = array(
      '#type' => 'value',
      '#value' => $gamenode->nid,
    );
    $form['result']['hometeam'] = array(
      '#type' => 'markup',
      '#value' => t('<strong>Home:</strong> ') . ult_teamname_theme($gamenode->field_ult_gamehometeam[0]['nid'], TRUE),
    );
    $form['result']['homescore'] = array(
      '#type' => 'textfield',
      '#default_value' => 0,
      '#size' => 5,
    );
    $form['result']['homespirit'] = array('#type' => 'markup', '#value' => '');
    if (variable_get('ult_allowdefaults', 1) == 1) {
      $form['result']['homedefault'] = array(
        '#type' => 'checkbox',
        '#title' => t('Home Default'),
      );
    }
    $form['result']['awayteam'] = array(
      '#type' => 'markup',
      '#value' => t('<strong>Away:</strong> ') . ult_teamname_theme($gamenode->field_ult_gameawayteam[0]['nid'], TRUE),
    );
    $form['result']['awayscore'] = array(
      '#type' => 'textfield',
      '#default_value' => 0,
      '#size' => 5,
    );
    $form['result']['awayspirit'] = array('#type' => 'markup', '#value' => '');
    if (variable_get('ult_allowdefaults', 1) == 1) {
      $form['result']['awaydefault'] = array(
        '#type' => 'checkbox',
        '#title' => t('Away Default'),
      );
    }
    $form['result']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit Score'),
    );
    return $form;
  }
  else {
    $form = array();
    $form['opaction'] = array( '#type' => 'hidden', '#value' => $action, );
    $form['gameid'] = array( '#type' => 'hidden', '#value' => $gamenode->nid, );
    $form['hometeam'] = array( '#type' => 'hidden', '#value' => $gamenode->field_ult_gamehometeam[0]['nid'], );
    $form['awayteam'] = array( '#type' => 'hidden', '#value' => $gamenode->field_ult_gameawayteam[0]['nid'], );
    $form['homescore'] = array( '#type' => 'hidden', '#value' => $form_state['values']['homescore'], );
    $form['awayscore'] = array( '#type' => 'hidden', '#value' => $form_state['values']['awayscore'], );
    if (variable_get('ult_allowdefaults', 1) == 1) {
      $form['homedefault'] = array( '#type' => 'hidden', '#value' => $form_state['values']['homedefault'], );
      $form['awaydefault'] = array( '#type' => 'hidden', '#value' => $form_state['values']['awaydefault'], );
    }
    $form['gameid'] = array( '#type' => 'hidden', '#value' => $gamenode->nid);
    // Determine which team is submitting the score
    global $user;
    $submitteam = 'unknown';
    if (user_access('administer ULT leagues')) {
      $submitteam = 'admin';
    }
    elseif (variable_get('ult_enablecaptains', 1) == 1) {
      if (_ult_main_iscaptain($gamenode->field_ult_gamehometeam[0]['nid'], $user->uid)) {
        $submitteam = 'home';
      }
      elseif (_ult_main_iscaptain($gamenode->field_ult_gameawayteam[0]['nid'], $user->uid)) {
        $submitteam = 'away';
      }
      elseif (module_exists('ult_rosters')) {
        if (_ult_rosters_ismember($gamenode->field_ult_gamehometeam[0]['nid'], $user->uid)) {
          $submitteam = 'home';
        }
        elseif (_ult_rosters_ismember($gamenode->field_ult_gameawayteam[0]['nid'], $user->uid)) {
          $submitteam = 'away';
        }
      }
    }
    $form['submitteam'] = array( '#type' => 'hidden', '#value' => $submitteam );
    $desc = t("You have entered the following as the score for this game: ") . "<br />";
    $desc .= ult_teamname_theme($gamenode->field_ult_gamehometeam[0]['nid']) . ": " . $form_state['values']['homescore'];
    if (variable_get('ult_allowdefaults', 1) == 1) {
      if ($form_state['values']['homedefault'] == 1) {
        $desc .= t(" (defaulted)");
      }
    }
    $desc .= "<br />";
    $desc .= ult_teamname_theme($gamenode->field_ult_gameawayteam[0]['nid']) . ": " . $form_state['values']['awayscore'];
    if (variable_get('ult_allowdefaults', 1) == 1) {
      if ($form_state['values']['awaydefault'] == 1) {
        $desc .= t(" (defaulted)");
      }
    }
    $desc .= "<br />";
    $desc .= '<br />';
    $desc .= t('Is this correct?');
    if ($action == 'challenge') {
      return confirm_form($form, t("Challenge Game Result"), 'ult/game/' . $gamenode->nid, $desc, t('Confirm'), t('Cancel'));
    }
    else {
      return confirm_form($form, t("Confirm Game Result"), 'ult/game/' . $gamenode->nid, $desc, t('Confirm'), t('Cancel'));
    }
  }
}


/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 * @param $gamenode
 *   A node object representing the game for which results are being submitted
 *
 * @return
 *   A form array containing the form elements to be displayed
 */
function ult_matches_cancelgame_form($form_state, $gamenode) {
  $form = array();
  $form['gameid'] = array(
    '#type' => 'value',
    '#value' => check_plain($gamenode->nid),
  );
  if (!isset($form_state['storage']['confirm'])) {
    $description = t('Use the buttons below to mark this game as "cancelled", "postponed", or "rescheduled".');
    $form['ult'] = array(
      '#type' => 'fieldset',
      '#title' => t('Cancel/Postpone/Reschedule Game'),
      '#description' => $description,
      '#collapsible' => FALSE,
    );
    if (variable_get('ult_allowcancelgame', 1) == 1) {
      $form['ult']['cancelbutton'] = array(
        '#type' => 'submit',
        '#value' => t('Cancel Game'),
        '#name' => 'cancel',
      );
    }
    if (variable_get('ult_allowpostponegame', 1) == 1) {
      $form['ult']['postponebutton'] = array(
        '#type' => 'submit',
        '#value' => t('Postpone Game'),
        '#name' => 'postpone',
      );
    }
    if (variable_get('ult_allowreschedulegame', 1) == 1) {
      $defaultstart = variable_get('ult_matchstart', array('hour' => '6', 'minute' => '00', 'meridiem' => 'pm'));
      $current_date = date_format_date(date_make_date('now', 'NULL', 'DATE_UNIX'), 'custom', 'Y-m-d');
      $current_date .= ' ' . ult_main_gettime($defaultstart, 'hour') . ':' . ult_main_gettime($defaultstart, 'minute');
      $form['ult']['rescheduledate'] = array(
        '#type' => 'date_popup',
        '#title' => t('Reschedule Date'),
        '#date_format' => 'Y-m-d h:i A',
        '#date_year_range' => '0,+1',
        '#date_increment' => 15,
        //'#default_value' => $current_date,
      );
      if (module_exists('ult_fields')) {
        // Get available fields for this league
        $options = array();
        if (variable_get('ult_fieldassignments', 1) == 1) {
          $options = _ult_fields_getassignedfields($gamenode->field_ult_gameleague[0]['nid']);
          if (count($options) > 0) {
            $form['ult']['reschedulefield'] = array(
              '#type' => 'select',
              '#title' => t('Reschedule field'),
              '#options' => _ult_main_convert_options_to_text($options),
            );
          }
        }
      }
      $form['ult']['reschedulebutton'] = array(
        '#type' => 'submit',
        '#value' => t('Reschedule Game'),
        '#name' => 'reschedule',
      );
    }
    return $form;
  }
  else {
    // Determine which button was pressed
    $action = check_plain($form_state['storage']['action']);
    $form['action'] = array( '#type' => 'value', '#value' => $action, );
    if (($action == "reschedule") && ($form_state['storage']['rescheduledate'])) {
      $form['rescheduledate'] = array( '#type' => 'value', '#value' => $form_state['storage']['rescheduledate'], );
    }
    $form['hometeam'] = array( '#type' => 'hidden', '#value' => $gamenode->field_ult_gamehometeam[0]['nid'], );
    $form['awayteam'] = array( '#type' => 'hidden', '#value' => $gamenode->field_ult_gameawayteam[0]['nid'], );
    // Determine which team is submitting the score
    global $user;
    $submitteam = 'unknown';
    if (user_access('administer ULT leagues')) {
      $submitteam = 'admin';
    }
    elseif (variable_get('ult_enablecaptains', 1) == 1) {
      if (_ult_main_iscaptain($gamenode->field_ult_gamehometeam[0]['nid'], $user->uid)) {
        $submitteam = 'home';
      }
      elseif (_ult_main_iscaptain($gamenode->field_ult_gameawayteam[0]['nid'], $user->uid)) {
        $submitteam = 'away';
      }
      elseif (module_exists('ult_rosters')) {
        if (_ult_rosters_ismember($gamenode->field_ult_gamehometeam[0]['nid'], $user->uid)) {
          $submitteam = 'home';
        }
        elseif (_ult_rosters_ismember($gamenode->field_ult_gameawayteam[0]['nid'], $user->uid)) {
          $submitteam = 'away';
        }
      }
    }
    $form['submitteam'] = array( '#type' => 'hidden', '#value' => $submitteam );
    switch ($action) {
      case 'cancel':
        $desc .= t('You are about to mark this game as "cancelled".');
        $desc .= '<br />';
        $desc .= t('An empty game result will be created to reflect this, and you will no longer be able to submit a score for this game.');
        $desc .= '<br />';
        $desc .= t('Note:  This action can not be undone.');
        $desc .= '<br /><br />';
        $desc .= t('Are you sure you wish to register this game as "cancelled"?');
        return confirm_form($form, t("Cancel Game"), 'ult/game/' . $gamenode->nid, $desc, t('Cancel Game'), t('Go Back'));
        break;
      case 'postpone':
        $desc .= t('You are about to mark this game as "postponed".');
        $desc .= '<br />';
        $desc .= t('An empty game result will be created to reflect this, and you will no longer be able to submit a score for this game.');
        $desc .= '<br />';
        $desc .= t('A new game entry will be created if/when the game has been rescheduled, and the eventual score will be recorded against that entry.');
        $desc .= '<br />';
        $desc .= t('Note:  This action can not be undone.');
        $desc .= '<br /><br />';
        $desc .= t('Are you sure you wish to register this game as "postponed"?');
        return confirm_form($form, t("Postpone Game"), 'ult/game/' . $gamenode->nid, $desc, t('Postpone Game'), t('Go Back'));
        break;
      case 'reschedule':
        $desc .= t('You are about to mark this game as "rescheduled"');
        if ($form_state['storage']['rescheduledate']) {
          $desc .= t(' for ') . date_format_date(date_make_date($form_state['storage']['rescheduledate']), long);
        }
        $desc .= '.<br />';
        $desc .= t('An empty game result will be created for this game to reflect the rescheduled status');
        if ((variable_get('ult_autocreaterescheduledgame', 0) == 1) && ($form_state['storage']['rescheduledate'])) {
          $desc .= t(', and a new game entry will be created on the reschedule date');
        }
        $desc .= t('.  The eventual score should be recorded against the new entry created on the reschedule date.');
        $desc .= '<br />';
        $desc .= t('Note:  This action can not be undone.');
        $desc .= '<br /><br />';
        $desc .= t('Are you sure you wish to register this game as "rescheduled"?');
        return confirm_form($form, t("Reschedule Game"), 'ult/game/' . $gamenode->nid, $desc, t('Reschedule Game'), t('Go Back'));
        break;
    }
  }
}


// ********************************************************
// *                                                      *
// *                 Form Validation                      *
// *                                                      *
// ********************************************************
/**
 * Validate the ult_matches_submitscore_form form
 */
function ult_matches_submitscore_form_validate($form_id, &$form_state) {
  // $action = $form_state['values']['opaction'];
  // TODO:  Check that user has permission to submit/challenge results
  // TODO:  Check that result has not already been submitted
  // TODO:  Check ties/defaults allowed
}

/**
 * Validate the ult_matches_cancelgame_form form
 */
function ult_matches_cancelgame_form_validate($form, &$form_state) {
}


// ********************************************************
// *                                                      *
// *                 Form Submission                      *
// *                                                      *
// ********************************************************
/**
 * Submit the 'ult_matches_submitscore_form' form
 *
 * Process the submitted 'ult_matches_submitscore_form' form
 */
function ult_matches_submitscore_form_submit($form_id, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  else {
    $action = $form_state['values']['opaction'];
    // process result
    $gameid = $form_state['values']['gameid'];
    $hometeam = $form_state['values']['hometeam'];
    $awayteam = $form_state['values']['awayteam'];
    $homename = ult_teamname_theme($hometeam);
    $awayname = ult_teamname_theme($awayteam);
    $homescore = $form_state['values']['homescore'];
    $awayscore = $form_state['values']['awayscore'];
    $submitteam = $form_state['values']['submitteam'];
    if ($action == 'challenge') {
      $resulttype = "challenge";
    }
    else {
      $resulttype = "regular";
    }
    // Determine win/loss status
    if (($homescore == $awayscore) && (($resulttype == "regular") || ($resulttype == "challenge"))) {
      $homeresult = "draw";
      $awayresult = "draw";
    }
    elseif (($homescore > $awayscore) && (($resulttype == "regular") || ($resulttype == "challenge"))) {
      $homeresult = "win";
      $awayresult = "loss";
    }
    elseif (($awayscore > $homescore) && (($resulttype == "regular") || ($resulttype == "challenge"))) {
      $homeresult = "loss";
      $awayresult = "win";
    }
    // Check for default and modify score accordingly
    if (variable_get('ult_allowdefaults', 1) == 1) {
      $homedefault = $form_state['values']['homedefault'];
      $awaydefault = $form_state['values']['awaydefault'];
      if ((($homedefault == 1) || ($awaydefault == 1)) && (($resulttype == "regular") || ($resulttype == "challenge"))) {
        if ($resulttype == "regular") {
          $resulttype = "default";
        }
        // If default, set default score
        if (($homedefault == 1) && ($awaydefault == 1)) {
          $homescore = variable_get('ult_defaultlossscore', 0);
          $awayscore = variable_get('ult_defaultlossscore', 0);
          $homeresult = "defaultloss";
          $awayresult = "defaultloss";
        }
        elseif ($homedefault == 1) {
          $homescore = variable_get('ult_defaultlossscore', 0);
          $awayscore = variable_get('ult_defaultwinscore', 1);
          $homeresult = "defaultloss";
          $awayresult = "defaultwin";
        }
        elseif ($awaydefault == 1) {
          $homescore = variable_get('ult_defaultwinscore', 1);
          $awayscore = variable_get('ult_defaultlossscore', 0);
          $homeresult = "defaultwin";
          $awayresult = "defaultloss";
        }
      }
    }
    // Create ult_gameresult node item.
    global $user;
    module_load_include('inc', 'node', 'node.pages');
    $node->type = 'ult_gameresult';
    node_object_prepare($node);
    $node->uid = $user->uid;
    if ($action == "challenge") {
      $node->title = drupal_html_to_text(t("Challenge: ") . $homename . '(' . $homescore . ') vs. ' . $awayname . '(' . $awayscore . ')');
    }
    else {
      $node->title = drupal_html_to_text($homename . '(' . $homescore . ') vs. ' . $awayname . '(' . $awayscore . ')');
    }
    $node->body = "";
    $node->teaser = "";
    $node->filter = variable_get('filter_default_format', 1);
    $node->format = FILTER_FORMAT_DEFAULT;
    $node->language = '';
    $node->revision = 0;
    $node->promote = 0;
    $node->created = time();
    $node->comment= 0;
    $node->field_ult_gameid[0]['nid'] = $gameid;
    $node->field_ult_resulttype[0]['value'] = $resulttype;
    $node->field_ult_homescore[0]['value'] = $homescore;
    $node->field_ult_awayscore[0]['value'] = $awayscore;
    $node->field_ult_homeresult[0]['value'] = $homeresult;
    $node->field_ult_awayresult[0]['value'] = $awayresult;
    $node->field_ult_submitteam[0]['value'] = $submitteam;
    node_save($node);
    unset($form_state['storage']);
    $form_state['redirect'] = 'ult/game/' . $form_state['values']['gameid'];
    // Send Notifications (if necessary)
    if ($action == "challenge") {
      _ult_matches_process_newchallengenotify($node);
    }
    else {
      _ult_matches_process_newresultnotify($node);
    }
    // TODO:  Change value of resulttype in original submission
    // TODO:  Ensure user does not already have an outstanding challenge
  }
}

/**
 * Submit ult_matches_cancelgame_form form
 *
 * @param $form form to submit
 * @param $form_state A keyed array containing the current state of the form.
 */
function ult_matches_cancelgame_form_submit($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = TRUE;
    // Determine Button
    if ($form_state['clicked_button']['#name'] == 'cancel') {
      $form_state['storage']['action'] = 'cancel';
    }
    elseif ($form_state['clicked_button']['#name'] == 'postpone') {
      $form_state['storage']['action'] = 'postpone';
    }
    elseif ($form_state['clicked_button']['#name'] == 'reschedule') {
      $form_state['storage']['action'] = 'reschedule';
      if ($form_state['values']['rescheduledate']) {
        $form_state['storage']['rescheduledate'] = $form_state['values']['rescheduledate'];
      }
      else {
        drupal_set_message(t('No reschedule date supplied. Switching to "postponed" state.'));
        $form_state['storage']['action'] = 'postpone';
      }
    }
    $form_state['rebuild'] = TRUE;
  }
  else {
    // Process Submission
    $action = $form_state['values']['action'];
    $gameid = check_plain($form_state['values']['gameid']);
    $hometeam = $form_state['values']['hometeam'];
    $awayteam = $form_state['values']['awayteam'];
    $homename = ult_teamname_theme($hometeam);
    $awayname = ult_teamname_theme($awayteam);
    $submitteam = $form_state['values']['submitteam'];
    if (($action == 'cancel') && ($form_state['storage']['action'] = $action)) {
      $resulttype = 'canceled';
    }
    elseif (($action == 'postpone') && ($form_state['storage']['action'] = $action)) {
      $resulttype = 'postponed';
    }
    elseif (($action == 'reschedule') && ($form_state['storage']['action'] = $action)) {
      $resulttype = 'rescheduled';
    }
    $homeresult = $resulttype;
    $awayresult = $resulttype;
    // Create ult_gameresult node item.
    global $user;
    module_load_include('inc', 'node', 'node.pages');
    $node->type = 'ult_gameresult';
    node_object_prepare($node);
    $node->uid = $user->uid;
    $node->title = ucwords($resulttype) . ": " . drupal_html_to_text($homename . ' vs. ' . $awayname);
    $node->body = "";
    $node->teaser = "";
    $node->filter = variable_get('filter_default_format', 1);
    $node->format = FILTER_FORMAT_DEFAULT;
    $node->language = '';
    $node->revision = 0;
    $node->promote = 0;
    $node->created = time();
    $node->comment= 0;
    $node->field_ult_gameid[0]['nid'] = $gameid;
    $node->field_ult_resulttype[0]['value'] = $resulttype;
    $node->field_ult_submitteam[0]['value'] = $submitteam;
    $node->field_ult_homeresult[0]['value'] = $homeresult;
    $node->field_ult_awayresult[0]['value'] = $awayresult;
    if (($action == 'reschedule') && ($form_state['storage']['rescheduledate'])) {
      $node->field_ult_rescheduledate[0]['value'] = $form_state['storage']['rescheduledate'];
      if (variable_get('ult_autocreaterescheduledgame', 0) == 1) {
        // Create rescheduled game node if necessary
        $oldnode = node_load($gameid);
        $newnode = new stdClass();
        $newnode->type = 'ult_game';
        node_object_prepare($newnode);
        $newnode->uid = $user->uid;
        $newnode->title = drupal_html_to_text($homename . ' vs. ' . $awayname);
        $newnode->body = "";
        $newnode->teaser = "";
        $newnode->filter = variable_get('filter_default_format', 1);
        $newnode->format = FILTER_FORMAT_DEFAULT;
        $newnode->language = '';
        $newnode->revision = 0;
        $newnode->promote = 0;
        $newnode->created = time();
        $newnode->comment= 0;
        $newnode->field_ult_gameleague = $oldnode->field_ult_gameleague;
        $text_date = $form_state['storage']['rescheduledate'];
        $newnode->field_ult_gamedate[0]['value'] = $text_date;
        $newnode->field_ult_gamehometeam = $oldnode->field_ult_gamehometeam;
        $newnode->field_ult_gameawayteam = $oldnode->field_ult_gameawayteam;
        $newnode->field_ult_gameresult[0]['value'] = "No";
        node_save($newnode);
        // Update reschedule Nid on game result node
        $node->field_ult_reschedulegameid[0]['nid'] = $newnode->nid;
      }
    }
    node_save($node);
    if (($action == 'reschedule') && ($form_state['storage']['rescheduledate'])) {
      $message = t("Game rescheduled for ") . date_format_date(date_make_date($form_state['storage']['rescheduledate']), 'long') . '.  ';
      if (variable_get('ult_autocreaterescheduledgame', 0) == 1) {
        $message .= t('The new game entry can be viewed at ') . l('ult/game/' . $newnode->nid, 'ult/game/' . $newnode->nid);
        if (module_exists('ult_fields')) {
          $message .= '<br />' . t('Note:  No field has been set for the rescheduled game.  You may wish to manually update the field on the new game entry.');
        }
      }
      // Send notifications if necessary
      _ult_matches_process_newresultnotify($node, $message);
      drupal_set_message($message);
    }
    else {
      // Send notifications if necessary
      _ult_matches_process_newresultnotify($node);
    }
    unset($form_state['storage']);
    $form_state['redirect'] = 'ult/game/' . $form_state['values']['gameid'];
  }
}

/**
 * Processes notifications for a new result submission
 *
 * @param $node
 *   A node object representing the submitted game result
 * @param $message
 *   A message to be prefixed to the notification body
 */
function _ult_matches_process_newresultnotify($node, $message = '') {
  $destinations = variable_get('ult_resultnotify', array());
  if (($destinations['league'] == 'league') || ($destinations['original'] == 'original') || ($destinations['subcaptain'] == 'subcaptain')|| ($destinations['oppcaptain'] == 'oppcaptain')) {
    // Set 'to' address
    $to = _ult_matches_notify_generatetoaddr($destinations, $node);
    // Set 'from' address
    if (variable_get('ult_sendfromleagueemail', 0) == 1) {
      $from = variable_get('ult_leaguecoordemail', '');
    }
    if ($from == '') {
      $from = variable_get('site_mail', ini_get('sendmail_from'));
    }
    // Set 'Subject'
    $params['subject'] = check_plain(variable_get('ult_orgname', 'ULT')) . ' - ' . t('Notice - New Game Results Submitted');
    // Set 'Body'
    $params['body'] = _ult_matches_newgameresult_text($node);
    // Append the reschedule date if passed in $message.
    if ($message != '') {
      $params['body'] .= drupal_wrap_mail($message);
    }
    // Send Email
    $emailresult = drupal_mail('ult_matches', 'newresultnotify', $to, language_default(), $params, $from, TRUE);
    // Validate result
    if ($emailresult['result'] != TRUE) {
      watchdog('ULT_matches', 'A problem was encountered while sending "New Game Result" notification email for game @gameid', array('@gameid' => $node->nid), WATCHDOG_WARNING );
    }
    else {
      watchdog('ULT_matches', '"New Game Result" Notification sent for game @gameid', array('@gameid' => $node->nid), WATCHDOG_NOTICE );
    }
  }
}

/**
 * Processes notifications for a new result challenge
 *
 * @param $node
 *   A node object representing the challenged game result
 */
function _ult_matches_process_newchallengenotify($node) {
  $destinations = variable_get('ult_challengenotify', array());
  if (($destinations['league'] == 'league') || ($destinations['submitter'] == 'submitter') ||($destinations['original'] == 'original') || ($destinations['subcaptain'] == 'subcaptain')|| ($destinations['oppcaptain'] == 'oppcaptain')) {
    // Set 'to' address
    $to = _ult_matches_notify_generatetoaddr($destinations, $node);
    // Set 'from' address
    if (variable_get('ult_sendfromleagueemail', 0) == 1) {
      $from = variable_get('ult_leaguecoordemail', '');
    }
    if ($from == '') {
      $from = variable_get('site_mail', ini_get('sendmail_from'));
    }
    // Set 'Subject'
    $params['subject'] = check_plain(variable_get('ult_orgname', 'ULT')) . ' - ' . t('Notice - New Game Result Challenge Submitted');
    // Set 'Body'
    $params['body'] = _ult_matches_newchallenge_text($node);
    // Send Email
    $emailresult = drupal_mail('ult_matches', 'newchallengenotify', $to, language_default(), $params, $from, TRUE);
    // Validate result
    if ($emailresult['result'] != TRUE) {
      watchdog('ULT_matches', 'A problem was encountered while sending "New Game Result Challenge" notification email for game @gameid', array('@gameid' => $node->nid), WATCHDOG_WARNING );
    }
    else {
      watchdog('ULT_matches', '"New Game Result Challenge" Notification sent for game @gameid', array('@gameid' => $node->nid), WATCHDOG_NOTICE );
    }
  }
}

/**
 * Theme function for the 'submit score' form
 *
 * Used to arrange form fields into a nicely formatted scoreboard table
 */
function theme_ult_matches_submitscore_form($form) {
  $headers = array(t('Teams'), t('Score'), '');
  if (module_exists('ult_spirit') && (variable_get('ult_spirit_format', 1) == 0)) {
    $headers = array(t('Teams'), t('Score'), t('Spirit'), '');
  }
  $rows = array(
    array(drupal_render($form['result']['hometeam']), drupal_render($form['result']['homescore']), drupal_render($form['result']['homespirit']), drupal_render($form['result']['homedefault'])),
    array(drupal_render($form['result']['awayteam']), drupal_render($form['result']['awayscore']), drupal_render($form['result']['awayspirit']), drupal_render($form['result']['awaydefault'])),
  );
  $output .= theme('table', $headers, $rows);
  $output .= drupal_render($form);
  return $output;
}

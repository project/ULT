<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
  <?php if ($page == 0): ?>
    <!--  <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>  -->
  <?php endif; ?>
  <?php print _ult_main_returnlink($node, 'GameParent'); ?>
    <div class="content clear-block">

    <fieldset class="ult_fieldset">
      <div class="ult_gameinfo_left">
        <?php if ($results): ?>
          <?php foreach ($results as $result) { ?>
              <?php print $result; ?>
          <?php } ?>
        <?php else : ?>
          <table>
            <tr><th>Teams</th></tr>
            <tr class="odd">
              <?php if ($homelogo || $awaylogo): ?>
                <td class="ult_gameinfo_logo">
                  <?php if ($homelogo) {   print theme('imagecache', 'ult_logothumb', $homelogo); } ?>
                </td>
              <?php endif; ?>
              <td class="ult_gameinfo_teamname">
                <?php print $hometeamname_rendered; ?>
              </td>
            </tr>
            <tr class="even">
              <?php if ($homelogo || $awaylogo): ?>
                <td class="ult_gameinfo_logo">
                  <?php if ($awaylogo) {   print theme('imagecache', 'ult_logothumb', $awaylogo); } ?>
                </td>
              <?php endif; ?>
              <td class="ult_gameinfo_teamname">
                <?php print $awayteamname_rendered;?>
              </td>
            </tr>
          </table>
        <?php endif; ?>
      </div>
      <div class="ult_gameinfo_right">
        <fieldset class="ult-fieldset"><legend><?php print t("Game Information"); ?></legend>
          <div class="ult_gameinfo_date">
            <?php print $field_ult_gamedate_rendered; ?>
          </div>
          <div class="ult_gameinfo_league">
            <div class="field field-type-nodereference field-field-ult-gameleague">
              <div class="field-items">
                <div class="field-item odd">
                  <div class="field-label-inline-first">
                    <?php print $leaguelabel; ?>:&nbsp;
                  </div>
                  <?php print $leagueurl; ?>
                </div>
              </div>
            </div>
          </div>
          <?php if ($field_ult_gamefieldsite_rendered): ?>
            <div class="ult_gameinfo_field">
              <div class="field field-type-nodereference field-field-ult-gamefield">
                <div class="field-items">
                  <div class="field-item odd">
                    <div class="field-label-inline-first">
                      <?php print t('Field Site'); ?>:&nbsp;
                    </div>
                    <?php print $fieldurl; ?>
                  </div>
                  <?php if ($field_ult_gamefield_rendered): ?>
                  <?php print $field_ult_gamefield_rendered; ?>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          <?php endif; ?>
          <br />
          <div class="ult_gameinfo_actions">
            <ul>
            <?php if ($submiturl): ?>
              <li><?php print $submiturl; ?></li>
            <?php endif; ?>
            <?php if ($cancelurl): ?>
              <li><?php print $cancelurl; ?></li>
            <?php endif; ?>
            <?php if ($challengeurl): ?>
              <li><?php print $challengeurl; ?></li>
            <?php endif; ?>
            <?php if ($links): ?>
              <li><?php print $links; ?></li>
            <?php endif; ?>
            </ul>
          </div>
        </fieldset>
      </div>
      <div class="clear-block"></div>
    </fieldset>
    </div>
  <div class="clear-block"></div>
</div>

<?php

/**
 * @file
 * Page generation functions related to the ULT_Matches module.
 *
 * Dynamic select box code taken from 'examples - dynamic dropdown' module
 */

/**
 * Generates the league/division 'Games Summary' page
 *
 * @param $node
 *   A node object representing the league/division being referenced
 *
 * @return $output
 *   The rendered 'add game' page content
 */
function ult_matches_addgame_page($node) {
  $output .= _ult_main_returnlink($node);
  $output .= drupal_get_form('_ult_matches_addgame_form', $node);
  $output .= '<div class="ult-upcominggames-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Scheduled Games') . '</legend>';
  $output .= views_embed_view('ult_upcominggames', 'default', $node->nid);
  $output .= '</fieldset></div>';
  return $output;
}

/**
 * Implements hook_form().
 *
 * @param $form_state
 *    The form state array.
 *
 * @return
 *    An array containing the form elements to be displayed in the node edit form.
 */
function _ult_matches_addgame_form($form_state, $node) {
  $form = array();
  $form['ult'] = array(
    '#type' => 'fieldset',
    '#title' => 'Add Single Game',
    '#collapsible' => FALSE,
  );
  $form['ult']['league'] = array(
    '#type' => 'hidden',
    '#value' => check_plain($node->nid),
  );
  // Build list of teams in this division
  $options = _ult_main_convert_options_to_text(_ult_main_getteamsbyleague($node->nid));
  $teamnids = array_keys($options);
  $defaulthome = array_shift($teamnids);
  $defaultaway = array_shift($teamnids);
  unset($teamnids);
  // Put teamlist in storage for use in 'submit'
  $form_state['storage']['teams'] = $options;
  // Generate team selection boxes
  $form['ult']['hometeam'] = array(
    '#type' => 'select',
    '#title' => t('"Home" Team'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['hometeam']) ? $form_state['values']['hometeam'] : $defaulthome,
  );
  $form['ult']['awayteam'] = array(
    '#type' => 'select',
    '#title' => t('"Away" Team'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => isset($form_state['values']['awayteam']) ? $form_state['values']['awayteam'] : $defaultaway,
  );
  // Create default date using 'default game start time' variables
  $defaultstart = variable_get('ult_matchstart', array('hour' => '6', 'minute' => '00', 'meridiem' => 'pm'));
  $current_date = date_format_date(date_make_date('now', 'NULL', 'DATE_UNIX'), 'custom', 'Y-m-d');
  $current_date .= ' ' . ult_main_gettime($defaultstart, 'hour') . ':' . ult_main_gettime($defaultstart, 'minute');
  $form['ult']['gamedate'] = array(
    '#type' => 'date_popup',
    '#title' => t('Game date'),
    '#date_format' => 'Y-m-d H:i',
    '#date_year_range' => '-1:+3',
    '#date_increment' => 15,
    '#default_value' => isset($form_state['values']['gamedate']) ? $form_state['values']['gamedate'] : $current_date,
  );
  // Field Assignment (if required)
  if (module_exists('ult_fields')) {
    // Get available fields for this league
    $options = array();
    if (variable_get('ult_fieldassignments', 1) == 1) {
      $options = _ult_main_convert_options_to_text(_ult_fields_getassignedfields($node->nid));
      $nofieldmsg = t('ULT Fields module installed, but no fields have been assigned to this league.');
    }
    else {
      $options = _ult_main_convert_options_to_text(_ult_fields_getallfieldsites());

      $nofieldmsg = t('ULT Fields module installed, but no fields have yet been created on this site.');
    }
    if (count($options) > 0) {
      $fieldnids = array_keys($options);
      $master_selection = !empty($form_state['values']['gamefield']) ? $form_state['values']['gamefield'] : $fieldnids[0];
      $form['ult']['gamefield'] = array(
        '#type' => 'select',
        '#title' => t('Field'),
        '#options' => $options,
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#default_value' => $master_selection,
        '#ahah' => array(
          'path' => 'ult/callbacks/fieldsitelist',
          'wrapper' => 'ult_dependent-dropdown-wrapper',
        ),
        '#attributes' => array('class' => 'ult_master-dropdown'),
      );
      // Button hidden by css if JS is enabled
      $form['ult']['select_field_num'] = array(
        '#type' => 'submit',
        '#value' => t('Select Field'),
        '#attributes' => array('class' => 'ult_next-button'),
        '#submit' => array('ult_matches_dropdown_continue'),
      );
      $form['ult']['ult_dependent_dropdown_holder'] = array(
        '#tree' => TRUE,
        '#prefix' => '<div id="ult_dependent-dropdown-wrapper">',
        '#suffix' => '</div>',
      );
      $form['ult']['ult_dependent_dropdown_holder']['fieldnumber'] = array(
        '#type' => 'select',
        '#title' => t('Field number at site'),
        '#options' => _ult_matches_addgame_getfieldoptions($master_selection),
        '#default_value' => isset($form_state['values']['ult_dependent_dropdown_holder']['fieldnumber']) ? $form_state['values']['ult_dependent_dropdown_holder']['fieldnumber'] : '',
      );
    }
    else {
      $form['ult']['nofields'] = array(
        '#type' => 'markup',
        '#value' => $nofieldmsg,
        '#prefix' => '<div>',
        '#suffix' => '</div>',
      );
    }
  }
  $form['ult']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Game'),
  );
  return $form;
}

/**
 * An AHAH callback for datafilling the 'field number' based on which 'field'
 *   is selected.
 */
function ult_matches_addgame_getfieldcallback() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form_state['ahah_submission'] = TRUE;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  $changed_elements = $form['ult']['ult_dependent_dropdown_holder'];
  // Prevent duplicate wrappers
  unset($changed_elements['#prefix'], $changed_elements['#suffix']);
  $output .= drupal_render($changed_elements);
  drupal_json(array( 'status' => TRUE, 'data' => $output, ));
}

/**
 * Submit handlers for 'continue to dependent dropdown' button
 */
function ult_matches_dropdown_continue($form, &$form_state) {
  // Prevent other submit handlers/hooks from firing
  unset($form_state['submit_handlers']);
  // Rebuild form, which should bring in proper fieldnumber values
  $form_state['rebuild'] = TRUE;
}

/**
 * Validates the _ult_matches_addgame_form form
 *
 * @param $form
 *    form to validate
 * @param $form_state
 *    A keyed array containing the current state of the form.
 */
function _ult_matches_addgame_form_validate($form, &$form_state) {
  // If an AHAH submission, it's just the dropdown working
  if (!empty($form_state['ahah_submission'])) {
    return;
  }
  if ($form_state['values']['hometeam'] == $form_state['values']['awayteam']) {
    form_set_error('awayteam', t('A team may not play itself.  Please ensure you have selected two different teams.'));
  }
  // TODO:  Ensure teams are not already scheduled for this time

}

/**
 * Submits the _ult_matches_addgame_form form
 *
 * @param $form
 *    form to submit
 * @param $form_state
 *    A keyed array containing the current state of the form.
 */
function _ult_matches_addgame_form_submit($form, &$form_state) {
  // If an AHAH submission, it's just the dropdown working
  if (!empty($form_state['ahah_submission'])) {
    return;
  }
  // Not an AHAH callback ... process form.
  $homenid = check_plain($form_state['values']['hometeam']);
  $awaynid = check_plain($form_state['values']['awayteam']);
  $homename = $form_state['storage']['teams'][$homenid];
  $awayname = $form_state['storage']['teams'][$awaynid];
  // Create ult_match node item.
  global $user;
  module_load_include('inc', 'node', 'node.pages');
  $node->type = 'ult_game';
  node_object_prepare($node);
  $node->uid = $user->uid;
  $node->title = $homename . ' vs. ' . $awayname;
  $node->body = "";
  $node->teaser = "";
  $node->filter = variable_get('filter_default_format', 1);
  $node->format = FILTER_FORMAT_DEFAULT;
  $node->language = '';
  $node->revision = 0;
  $node->promote = 0;
  $node->created = time();
  $node->comment= 0;
  $node->field_ult_gameleague[0]['nid'] = check_plain($form_state['values']['league']);
  $node->field_ult_gamedate[0]['value'] = check_plain($form_state['values']['gamedate']);
  $node->field_ult_gamehometeam[0]['nid'] = $homenid;
  $node->field_ult_gameawayteam[0]['nid'] = $awaynid;
  $node->field_ult_gameresult[0]['value'] = 'No';
  if ((module_exists('ult_fields')) && ($form_state['values']['gamefield'])) {
    $node->field_ult_gamefieldsite[0]['nid'] = $form_state['values']['gamefield'];
    if ($form_state['values']['ult_dependent_dropdown_holder']['fieldnumber']) {
      $node->field_ult_gamefield[0]['value'] = $form_state['values']['ult_dependent_dropdown_holder']['fieldnumber'];
    }
    else {
      $node->field_ult_gamefield[0]['value'] = '1';
    }
  }
  node_save($node);
  drupal_set_message("Game added:  " . check_plain($node->title) . " [" . date_format_date(date_make_date($node->field_ult_gamedate[0]['value']), 'small') . "]");
  unset($form_state['storage']);
}

/**
 * Helper function to populate the 'field numbers' dropdown for the selected field
 */
function _ult_matches_addgame_getfieldoptions($fieldnid = '') {
  // $fieldnid should have the nid of our field.
  if ($fieldnid == '') {
    return array('1');
  }
  // Get potential fields from database (table 'ult_fields')
  $sql = 'Select max(field) from {ult_fields} where nid = %d';
  $result = db_result(db_query($sql, $fieldnid));
  $fieldnums = array();
  for ($i = 1; $i < $result + 1; $i++) {
    $fieldnums[$i] = $i;
  }
  return $fieldnums;
}

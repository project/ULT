<?php

/**
 * @file
 * Administrative settings for ult_timeslots module
 */

/**
 * Implements hook_form().
 *
 * @return An array containing the form elements to be displayed in the settings form.
 */
function ult_timeslots_config_form() {
  $form = array();
  $form['timeslots'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Timeslot settings'),
  );
  $form['timeslots']['ult_ts_allfields'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow selection of individual fields per site when creating timeslots'),
    '#default_value' => variable_get('ult_ts_allfields', 0),
  );
  return system_settings_form($form);
}










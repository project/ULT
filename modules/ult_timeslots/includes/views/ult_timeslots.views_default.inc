<?php

/**
 * @file
 * Default view description for ULT Timeslot related views
 */

/**
 * Defines the default views for the 'ult_timeslots' module
 */
function ult_timeslots_views_default_views() {
  // ********************************************************
  // *                                                      *
  // *                View Name                             *
  // *                                                      *
  // ********************************************************
  $views = array();
  // Begin copy and paste from the Export tab of the view
  $view = new view;
  $view->name = 'ult_timeslots';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'fieldnid' => array(
      'id' => 'fieldnid',
      'table' => 'ult_timeslots',
      'field' => 'fieldnid',
    ),
  ));
  $handler->override_option('fields', array(
    'start' => array(
      'label' => 'Date',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'custom',
      'custom_date_format' => 'D, M jS, Y',
      'exclude' => 0,
      'id' => 'start',
      'table' => 'ult_timeslots',
      'field' => 'start',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'start_1' => array(
      'label' => 'Start Time',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'custom',
      'custom_date_format' => 'g:i a ',
      'exclude' => 0,
      'id' => 'start_1',
      'table' => 'ult_timeslots',
      'field' => 'start',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'end' => array(
      'label' => 'End Time',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'custom',
      'custom_date_format' => 'g:i a ',
      'exclude' => 0,
      'id' => 'end',
      'table' => 'ult_timeslots',
      'field' => 'end',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'ts_id' => array(
      'label' => 'Timeslot ID',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 1,
      'id' => 'ts_id',
      'table' => 'ult_timeslots',
      'field' => 'ts_id',
      'relationship' => 'none',
    ),
    'fieldnid' => array(
      'label' => 'Field Nid',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => 'ult/field/[fieldnid]',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'fieldnid',
      'table' => 'ult_timeslots',
      'field' => 'fieldnid',
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => 'Field Site',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 1,
        'path' => 'ult/field/[fieldnid]',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'fieldnid',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'fieldnum' => array(
      'label' => 'Field #',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'fieldnum',
      'table' => 'ult_timeslots',
      'field' => 'fieldnum',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'status' => array(
      'label' => 'Status',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'status',
      'table' => 'ult_timeslots',
      'field' => 'status',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
  ));
  $handler->override_option('sorts', array(
    'start' => array(
      'order' => 'ASC',
      'granularity' => 'minute',
      'id' => 'start',
      'table' => 'ult_timeslots',
      'field' => 'start',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'title' => array(
      'order' => 'ASC',
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'fieldnid',
    ),
    'fieldnum' => array(
      'order' => 'ASC',
      'id' => 'fieldnum',
      'table' => 'ult_timeslots',
      'field' => 'fieldnum',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'ult_fieldsite' => 'ult_fieldsite',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'ts_id' => array(
      'operator' => 'not empty',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'ts_id',
      'table' => 'ult_timeslots',
      'field' => 'ts_id',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'view ULT timeslots',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('empty_format', '1');
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('items_per_page', 0);
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'asc',
    'columns' => array(
      'start' => 'start',
      'start_1' => 'start_1',
      'end' => 'end',
      'ts_id' => 'ts_id',
      'fieldnid' => 'fieldnid',
      'title' => 'title',
      'fieldnum' => 'fieldnum',
      'status' => 'status',
    ),
    'info' => array(
      'start' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'start_1' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'end' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'ts_id' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'fieldnid' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'title' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'fieldnum' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'status' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'start',
  ));
  $handler = $view->new_display('page', 'By Field', 'page_1');
  $handler->override_option('arguments', array(
    'fieldnid' => array(
      'default_action' => 'not found',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'node',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'fieldnid',
      'table' => 'ult_timeslots',
      'field' => 'fieldnid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '51' => 0,
        '52' => 0,
      ),
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'ult_fieldsite' => 'ult_fieldsite',
        'forum' => 0,
        'page' => 0,
        'profile' => 0,
        'story' => 0,
        'ult_division' => 0,
        'ult_game' => 0,
        'ult_gameresult' => 0,
        'ult_league' => 0,
        'ult_profile' => 0,
        'ult_roster' => 0,
        'ult_team' => 0,
        'ult_waiver' => 0,
      ),
      'validate_argument_node_access' => 1,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '1' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('title', 'Timeslots by Field');
  $handler->override_option('empty', 'No timeslots have yet been created on this field');
  $handler->override_option('path', 'ult/field/%/timeslots');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Game Timeslots',
    'description' => '',
    'weight' => '6',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler = $view->new_display('page', 'By Status', 'page_2');
  $handler->override_option('arguments', array(
    'status' => array(
      'default_action' => 'ignore',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '%1 Timeslots',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'glossary' => 0,
      'limit' => '0',
      'case' => 'none',
      'path_case' => 'none',
      'transform_dash' => 0,
      'id' => 'status',
      'table' => 'ult_timeslots',
      'field' => 'status',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '51' => 0,
        '52' => 0,
      ),
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'forum' => 0,
        'page' => 0,
        'profile' => 0,
        'story' => 0,
        'ult_division' => 0,
        'ult_fieldsite' => 0,
        'ult_game' => 0,
        'ult_gameresult' => 0,
        'ult_league' => 0,
        'ult_profile' => 0,
        'ult_roster' => 0,
        'ult_team' => 0,
        'ult_waiver' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '1' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('empty', 'No timeslots found with that status.');
  $handler->override_option('path', 'ult/fields/timeslots/%');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => 'View',
    'description' => '',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler = $view->new_display('block', 'With Filters', 'block_1');
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'ult_fieldsite' => 'ult_fieldsite',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'ts_id' => array(
      'operator' => 'not empty',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'ts_id',
      'table' => 'ult_timeslots',
      'field' => 'ts_id',
      'relationship' => 'none',
    ),
    'title' => array(
      'operator' => '=',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'title_op',
        'identifier' => 'title',
        'label' => 'Field Site',
        'optional' => 1,
        'remember' => 0,
      ),
      'case' => 1,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'fieldnid',
    ),
    'fieldnum' => array(
      'operator' => '=',
      'value' => array(
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'fieldnum_op',
        'identifier' => 'fieldnum',
        'label' => 'Field Number',
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'fieldnum',
      'table' => 'ult_timeslots',
      'field' => 'fieldnum',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'start' => array(
      'operator' => '=',
      'value' => array(
        'type' => 'date',
        'value' => '',
        'min' => '',
        'max' => '',
      ),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 1,
        'operator' => 'start_op',
        'identifier' => 'start',
        'label' => 'Start Date/Time (eg. Mar 1, 2011 6pm)',
        'optional' => 1,
        'remember' => 0,
      ),
      'id' => 'start',
      'table' => 'ult_timeslots',
      'field' => 'start',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);


  // Add view to list of views to provide
  $views[$view->name] = $view;

  return $views;
}

<?php

/**
 * @file
 * Views_data description for the Ult_Timeslots table
 */

/**
 * Implements hook_views_data()
 */
function ult_timeslots_views_data() {
  $data['ult_timeslots']['table']['group'] = t('ULT Timeslots');
  $data['ult_timeslots']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'fieldnid',
    ),
  );
  $data['ult_timeslots']['ts_id'] = array(
    'title' => t('Timeslot ID'),
    'help' => t('Unique identifier for this timeslot.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['ult_timeslots']['start'] = array(
    'title' => t('Start'),
    'help' => t('Starting date/time for this timeslot'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['ult_timeslots']['end'] = array(
    'title' => t('End'),
    'help' => t('Ending date/time for this timeslot'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['ult_timeslots']['fieldnid'] = array(
    'title' => t('Field Nid'),
    'help' => t('The Nid of the Field this timeslot belongs to.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Field Id'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );
  $data['ult_timeslots']['fieldnum'] = array(
    'title' => t('Field Number'),
    'help' => t('Individual field number for this field site.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['ult_timeslots']['gamenid'] = array(
    'title' => t('Game Nid'),
    'help' => t('The Nid of the game currently assigned to this timeslot.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Game Id'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );
  $data['ult_timeslots']['leaguenid'] = array(
    'title' => t('League Id'),
    'help' => t('The Id of the League the currently assigned game belongs to.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('League/Division Id'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );
  $data['ult_timeslots']['status'] = array(
    'title' => t('Timeslot Status'),
    'help' => t('The current status (available, booked) of this timeslot'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  return $data;
}

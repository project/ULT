<?php

/**
 * @file
 * Timeslot related functions for ult_timeslots module
 */

/**
 * Generates the 'Delete ULT Timeslots' page
 */
function ult_timeslots_deletetimeslots_page() {
  $output .= '<div class="ult-timeslotcreate-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Delete Timeslots') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  $output .= drupal_get_form('ult_timeslots_timeslotfilter_form');
  $output .= drupal_get_form('ult_timeslots_deletetimeslots_form');
  $output .= '</div></fieldset></div>';
  return $output;
}

/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 *
 * @return
 *   An array containing the form elements to be displayed in the node edit form.
 */
function ult_timeslots_timeslotfilter_form($form_state) {
  $session = $_SESSION['ult_deletetimeslot_filter'];
  $session = is_array($session) ? $session : array();
  $names = array();
  $form = array();
  if (isset($session['date'])) {
    $form['filters']['timeslotdatevalue'] = array(
      '#value' => t('Date: ') . $session['date'],
    );
  }
  else {
    $names['timeslotdatekey'] = 'Timeslot Date';
    $form['filters']['timeslotdate'] = array(
      '#type' => 'date_popup',
      '#date_format' => 'Y-m-d',
      '#date_year_range' => '-3+3',
      '#required' => FALSE,
    );
  }
  if (isset($session['fieldsite'])) {
    $form['filters']['fieldsitevalue'] = array(
      '#value' => t('Field Site: ') . ult_fieldsite_theme($session['fieldsite'], TRUE),
    );
  }
  else {
    $names['fieldsitekey'] = 'Field Site';
    $form['filters']['fieldsite'] = array(
      '#type' => 'select',
      '#options' => _ult_main_convert_options_to_text(_ult_fields_getallfieldsites()),
    );
  }
  if (isset($session['fieldnum'])) {
    $form['filters']['fieldnumvalue'] = array(
      '#value' => t('Field Number: ') . $session['fieldnum'],
    );
  }
  else {
    $names['fieldnumkey'] = 'Field Number';
    $form['filters']['fieldnum'] = array(
      '#type' => 'select',
      '#options' => range(0, ult_fields_getmaxfieldnum()),
    );
  }
  $form['filters']['filter'] = array(
    '#type' => 'radios',
    '#options' => $names,
  );
  $form['filters']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (count($session)) {
    $form['filters']['buttons']['filterreset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
  }
  $form['#theme'] = 'ult_timeslots_timeslotfilter';
  return $form;
}

/**
 * Validate ult_timeslots_timeslotfilter_form form
 *
 * @param $form form to validate
 * @param $form_state A keyed array containing the current state of the form.
 */
function ult_timeslots_timeslotfilter_form_validate($form, &$form_state) {
  if ($form_state['values']['op'] == 'Filter') {
    if ($form_state['values']['filter'] == '') {
      form_set_error('filter', t("Please select at least one argument to filter on"));
    }
    elseif (($form_state['values']['filter'] == 'timeslotdatekey') && !isset($form_state['values']['timeslotdate'])) {
      form_set_error('timeslotdate', t("Please select a date to filter on"));
    }
  }
}

/**
 * Submit ult_timeslots_timeslotfilter_form form
 *
 * @param $form form to submit
 * @param $form_state A keyed array containing the current state of the form.
 */
function ult_timeslots_timeslotfilter_form_submit($form, &$form_state) {
  if ($form_state['values']['op'] == 'Reset') {
    unset($_SESSION['ult_deletetimeslot_filter']);
    drupal_set_message(t("Filters Cleared"));
  }
  elseif ($form_state['values']['filter'] == 'timeslotdatekey') {
    if (isset($form_state['values']['timeslotdate'])) {
      $date = date_format_date(date_make_date($form_state['values']['timeslotdate']), 'custom', 'Y-m-d');
      $_SESSION['ult_deletetimeslot_filter']['date'] = $date;
    }
  }
  elseif ($form_state['values']['filter'] == 'fieldsitekey') {
    if (isset($form_state['values']['fieldsite'])) {
      $_SESSION['ult_deletetimeslot_filter']['fieldsite'] = $form_state['values']['fieldsite'];
    }
  }
  elseif ($form_state['values']['filter'] == 'fieldnumkey') {
    if (isset($form_state['values']['fieldnum'])) {
      $_SESSION['ult_deletetimeslot_filter']['fieldnum'] = $form_state['values']['fieldnum'];
    }
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Implementation of hook_form().
 *
 * @param $form_state The form state array.
 * @return An array containing the form elements to be displayed in the node edit form.
 */
function ult_timeslots_deletetimeslots_form($form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    // Set limit for pager
    $limit = 25;
    // Define table header
    $header = array(
      '', //note empty value, will use this later
      array('data' => t('Date'), 'field' => 'start', 'sort' => 'asc'),
      array('data' => t('Start Time'), 'field' => 'start'),
      array('data' => t('End Time'), 'field' => 'end'),
      array('data' => t('Field Site'), 'field' => 'fieldnid'),
      array('data' => t('Field #'), 'field' => 'fieldnum'),
      array('data' => t('Status'), 'field' => 'status'),
    );
    $sql = "SELECT * FROM {ult_timeslots} where (status = 'Available')";
    // Allows sorting
    $tablesort = tablesort_sql($header);
    $attribs = array();
    // Check for filter
    if (isset($_SESSION['ult_deletetimeslot_filter'])) {
      if (isset($_SESSION['ult_deletetimeslot_filter']['date'])) {
        $date = check_plain($_SESSION['ult_deletetimeslot_filter']['date']);
        // Check date is in proper format
        // Needs to be in YYYY-MM-DD format
        if (!((strlen($date) == 10) && (preg_match("/^20[0-9][0-9]-[0|1][0-9]-[0|1|2|3][0-9]$/", $date)))) {
          unset($date);
          unset($_SESSION['ult_deletetimeslot_filter']);
        }
        else {
          $sql .= " AND (DATE(FROM_UNIXTIME(start)) = '%s')";
          $attribs['date'] = TRUE;
        }
      }
      if (isset($_SESSION['ult_deletetimeslot_filter']['fieldsite'])) {
        $field = check_plain($_SESSION['ult_deletetimeslot_filter']['fieldsite']);
        $sql .= " AND (fieldnid = %d)";
        $attribs['site'] = TRUE;
      }
      if (isset($_SESSION['ult_deletetimeslot_filter']['fieldnum'])) {
        $fieldnum = check_plain($_SESSION['ult_deletetimeslot_filter']['fieldnum']);
        $sql .= " AND (fieldnum = %d)";
        $attribs['num'] = TRUE;
      }
      // Determine which results to include in the query
      if ($attribs['date']) {
        if ($attribs['site']) {
          if ($attribs['num']) {
            // Filter on DATE + SITE + NUM
            $result = pager_query($sql . $tablesort, $limit, 0, NULL, $date, $field, $fieldnum);
          }
          else {
            // Filter on DATE + SITE
            $result = pager_query($sql . $tablesort, $limit, 0, NULL, $date, $field);
          }
        }
        else {
          if ($attribs['num']) {
            // Filter on DATE + NUM
            $result = pager_query($sql . $tablesort, $limit, 0, NULL, $date, $fieldnum);
          }
          else {
            // Filter on DATE
            $result = pager_query($sql . $tablesort, $limit, 0, NULL, $date);
          }
        }
      }
      else {
        if ($attribs['site']) {
          if ($attribs['num']) {
            // Filter on SITE + NUM
            $result = pager_query($sql . $tablesort, $limit, 0, NULL, $field, $fieldnum);
          }
          else {
            // Filter on SITE
            $result = pager_query($sql . $tablesort, $limit, 0, NULL, $field);
          }
        }
        else {
          if ($attribs['num']) {
            // Filter on NUM
            $result = pager_query($sql . $tablesort, $limit, 0, NULL, $fieldnum);
          }
        }
      }
    }
    else {
      // No filter
      $result = pager_query($sql . $tablesort, $limit, 0, NULL);
    }
    $form = array();
    while ($item = db_fetch_object($result)) {
      // Add each user id to my checkboxes array. Only keys, no values
      $key = $item->ts_id;
      $checkboxes[$key] = '';
      $form['tsid'][$key] = array(
        '#type' => 'hidden',
        '#value' => $key,
      );
      $form['fieldsite'][$key] = array(
        '#value' => ult_fieldsite_theme($item->fieldnid, TRUE),
      );
      $form['fieldnum'][$key] = array(
        '#value' => $item->fieldnum,
      );
      $form['fieldstatus'][$key] = array(
        '#value' => $item->status,
      );
      $form['startdate'][$key] = array(
        '#value' => format_date($item->start, 'custom', 'D M jS Y'),
      );
      $form['starttime'][$key] = array(
        '#value' => format_date($item->start, 'custom', 'g:i A'),
      );
      $form['endtime'][$key] = array(
        '#value' => format_date($item->end, 'custom', 'g:i A'),
      );
    }
    $form['checkboxes'] = array(
      '#type' => 'checkboxes',
      '#options' => $checkboxes,
    );
    $form['pager'] = array(
      '#value' => theme('pager', NULL, $limit, 0),
    );
    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Timeslots'),
    );
    $form['#theme'] = 'ult_timeslots_deletetimeslots';
    return $form;
  }
  else {
    $timeslots = $form_state['values']['checkboxes'];
    $form['timeslots'] = array(
      '#prefix' => '<ul>',
      '#suffix' => '</ul>',
      '#tree' => TRUE,
    );
    foreach ($timeslots as $tid) {
      $title = ult_timeslot_theme($tid);
      $form['timeslots'][$tid] = array(
        '#type' => 'hidden',
        '#value' => $tid,
        '#prefix' => '<li>',
        '#suffix' => $title . "</li>\n",
      );
    }
    return confirm_form($form, t('Are you sure you want to delete these items?'), 'ult/fields/timeslots',
      t('This action cannot be undone.'), t('Delete all'), t('Cancel')
    );
  }
}

/**
 * Validates the ult_timeslots_deletetimeslots_form form
 *
 * @param $form
 *   form to validate
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function ult_timeslots_deletetimeslots_form_validate($form, &$form_state) {
  // TODO:  Check to ensure all deleted timeslots are 'available'
  if (!isset($form_state['storage']['confirm'])) {
    foreach ($form_state['values']['checkboxes'] as $timeslot) {
      if ($timeslot != 0) {
        $foundtimeslot = TRUE;
        break;
      }
    }
    if (!$foundtimeslot) {
      form_set_error('checkboxes', t("Please select at least one timeslot to delete"));
    }
  }
}

/**
 * Processes the submitted ult_timeslots_deletetimeslots_form form
 *
 * @param $form
 *   form to submit
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function ult_timeslots_deletetimeslots_form_submit($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = TRUE;
    $form_state['rebuild'] = TRUE;
  }
  else {
    // Process Result
    foreach ($form_state['values']['timeslots'] as $key => $timeslot) {
      if (($key == $timeslot) && ($key > 0)) {
        ult_timeslots_deletetimeslot($timeslot);
      }
    }
    unset($form_state['storage']['confirm']);
  }
}

/**
 * Theme function: Renders the 'timeslot filters' form
 */
function theme_ult_timeslots_timeslotfilter($form) {
  if (isset($form['filters']['timeslotdate'])) {
    $rows[] = array(
      drupal_render($form['filters']['filter']['timeslotdatekey']),
      drupal_render($form['filters']['timeslotdate']),
    );
  }
  else {
    $rows[] = array(
      array('data' => drupal_render($form['filters']['timeslotdatevalue']), 'colspan' => 2),
    );
  }
  if (isset($form['filters']['fieldsite'])) {
    $rows[] = array(
      drupal_render($form['filters']['filter']['fieldsitekey']),
      drupal_render($form['filters']['fieldsite']),
    );
  }
  else {
    $rows[] = array(
      array('data' => drupal_render($form['filters']['fieldsitevalue']), 'colspan' => 2),
    );
  }
  if (isset($form['filters']['fieldnum'])) {
    $rows[] = array(
      drupal_render($form['filters']['filter']['fieldnumkey']),
      drupal_render($form['filters']['fieldnum']),
    );
  }
  else {
    $rows[] = array(
      array('data' => drupal_render($form['filters']['fieldnumvalue']), 'colspan' => 2),
    );
  }
  $content = theme('table', array(), $rows);
  $content .= drupal_render($form);
  $element = array(
    '#title' => t('Timeslot Filters'),
    '#value' => $content,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $output = theme('fieldset', $element);
  return $output;
}

/**
 * Theme function: Renders the 'delete timeslots' form
 */
function theme_ult_timeslots_deletetimeslots($form) {
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Date'), 'field' => 'start', 'sort' => 'asc'),
    array('data' => t('Start Time'), 'field' => 'start'),
    array('data' => t('End Time'), 'field' => 'end'),
    array('data' => t('Field Site'), 'field' => 'fieldnid'),
    array('data' => t('Field #'), 'field' => 'fieldnum'),
    array('data' => t('Status'), 'field' => 'status'),
  );

  if (!empty($form['checkboxes']['#options'])) {
    foreach (element_children($form['tsid']) as $key) {
      $rows[] = array(
        drupal_render($form['checkboxes'][$key]),
        drupal_render($form['startdate'][$key]),
        drupal_render($form['starttime'][$key]),
        drupal_render($form['endtime'][$key]),
        drupal_render($form['fieldsite'][$key]),
        drupal_render($form['fieldnum'][$key]),
        drupal_render($form['fieldstatus'][$key]),
      );
    }
  }
  else {
    $rows[] = array(array('data' => '<div class="error">' . t('No timeslots found') . '</div>', 'colspan' => 7));
  }
  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }
  $output .= drupal_render($form);
  return $output;
}

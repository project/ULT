<?php

/**
 * @file
 * Page generation functions for ULT_Scheduling module
 */

/**
 * Generates the 'ULT Timeslots' page
 */
function ult_timeslots_timeslots_page() {
  $output .= '<div class="ult-timeslotcreate-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Timeslots Overview') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  $output .= t('Timeslots are a means of tracking all of the "potential" time/field combinations where a game may be played. ');
  $output .= t(' For example, a league which plays on rented fields may create game timeslots to represent each of their rented field times. ');
  $output .= '<br /><br />';
  $output .= t('To view the timeslots for a given field, use the "Game Timeslots" tab on the page for that field.  ');
  $output .= t('To view other game timeslot combinations, use the form below.');
  $output .= '</div></fieldset></div>';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Timeslots') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  $output .= views_embed_view('ult_timeslots', 'block_1');
  $output .= '</div>';
  $output .= '</fieldset>';
  return $output;
}

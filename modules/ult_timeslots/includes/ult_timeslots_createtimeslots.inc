<?php

/**
 * @file
 * Timeslot related functions for ult_timeslots module
 */

/**
 * Generates the 'Create ULT Timeslots' page
 */
function ult_timeslots_createtimeslots_page() {
  $output .= '<div class="ult-timeslotcreate-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Create Timeslots') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  $output .= drupal_get_form('ult_timeslots_createtimeslots_form');
  $output .= '</div></fieldset></div>';
  return $output;
}

/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 * @param $leaguenid
 *   For filtering field listing by 'assigned fields', but this has been
 *   removed from the call in ult_timeslots_createtimeslot_page - not currently used.
 *
 * @return
 *   An array containing the form elements to be displayed in the node edit form.
 */
function ult_timeslots_createtimeslots_form($form_state, $leaguenid = NULL) {
  // Get Field Listing
  if (variable_get('ult_ts_allfields', 0) == 1) {
    $fieldlist = _ult_fields_getallfields($leaguenid);
    foreach ($fieldlist as $key => $ultfield) {
      $options[$key] = check_plain($ultfield['site']) . " (#" . $ultfield['ultfield'] . ")";
    }
    $description = t('Choose which fields this timeslot should be associated with');
  }
  else {
    if ($leaguenid) {
      $options = _ult_fields_getassignedfields($leaguenid);
    }
    else {
      $options = _ult_fields_getallfieldsites();
    }
    $description = t('Choose which field sites this timeslot should be associated with');
  }
  $form['fieldcontainer'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field Listing'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['fieldcontainer']['fields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select fields'),
    '#options' => $options,
    '#required' => TRUE,
    '#description' => $description,
    '#prefix' => '<div class="ult_fieldlistcontainer">',
    '#suffix' => '</div>',
  );
  // Generate the default date value for the 'date' field
  $current_date = date_format_date(date_make_date('now'), 'custom', 'Y-m-d ');
  $default_time = variable_get('ult_matchstart', array('hour' => '6', 'minute' => '00', 'meridiem' => 'pm'));
  if ($default_time['meridiem'] == 'pm') {
    if ($default_time['hour'] < 12) {
      $hour = str_pad($default_time['hour'] + 12, 2, "0", STR_PAD_LEFT);
    }
  }
  else {
    if ($default_time['hour'] == 12) {
      $hour = "00";
    }
    else {
      $hour = str_pad($default_time['hour'] + 12, 2, "0", STR_PAD_LEFT);
    }
  }
  $minute = str_pad($default_time['minute'], 2, "0", STR_PAD_LEFT);
  $default_time = $hour . ':' . $minute;
  $date = $current_date . $default_time;
  $format = 'Y-m-d H:i';
  $form['mydate'] = array(
    '#tree' => TRUE,
  );
  $form['mydate']['dateitem'] = array(
    '#type' => 'date_popup',
    '#title' => t('Start Date'),
    '#date_format' => variable_get('date_format_short', 'm/d/Y - H:i'),
    '#default_value' => $date,
    '#date_year_range' => '-1+3',
    '#required' => TRUE,
    '#tree' => TRUE,
  );
  $form['mydate']['rrule'] = array(
    '#type' => 'date_repeat_rrule',
    '#default_value' => isset($instance->repeat_rule) ? $instance->repeat_rule : '',
    '#date_timezone' => date_default_timezone_name(),
    '#date_format' => variable_get('date_format_short', 'm/d/Y - H:i'),
    '#date_year_range' => '-1:+3',
    '#date_repeat_widget' => 'date_popup',
    '#date_repeat_collapsed' => TRUE,
    '#weight' => 2,
  );
  $form['duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeslot Length'),
    '#default_value' => variable_get('ult_matchlength', 90),
    '#description' => t('The duration of a single game timeslot (or time between back-to-back timeslots), in minutes.'),
    '#required' => TRUE,
    '#size' => 5,
  );
  $form['num_backtoback'] = array(
    '#type' => 'textfield',
    '#title' => t('# of timeslots (back-to-back)'),
    '#default_value' => '1',
    '#description' => t('Given the start time and duration, how many back-to-back timeslots should be created for each day.'),
    '#required' => TRUE,
    '#size' => 5,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create game slot'),
    '#prefix' => '<br />',
  );
  return $form;
}

/**
 * Validates the ult_timeslots_createtimeslots_form form
 *
 * @param $form form to validate
 * @param $form_state A keyed array containing the current state of the form.
 */
function ult_timeslots_createtimeslots_form_validate($form, &$form_state) {
  $rrule = $form_state['values']['mydate']['rrule'];
  $parts = split("\n", $rrule);
  foreach ($parts as $part) {
    if (substr($part, 0, 6) == "RRULE:") {
      $parsed_rrule = date_ical_parse_rrule('', $part);
      // Check for repeating dates
      if ($parsed_rrule['FREQ'] != "NONE") {
        if (!isset($parsed_rrule['UNTIL'])) {
          form_set_error('mydate', t('If using repeating dates, please provide an "Until" date.'));
        }
      }
    }
  }
}

/**
 * Processes the submitted ult_timeslots_createtimeslots_form form
 *
 * @param $form
 *   form to submit
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function ult_timeslots_createtimeslots_form_submit($form, &$form_state) {
  // Determine which fields to include
  $fids = $form_state['values']['fields'];
  $fieldids = array();
  foreach ($fids as $fid) {
    if ($fid != 0) {
      $fieldids[] = intval($fid);
    }
  }
  // Determine start date & time
  $startdate = $form_state['values']['mydate']['dateitem'];
  $starttime = substr($startdate, 11, 8);
  // Break rule into components
  $exceptions = array();
  $additions = array();
  $rrule = $form_state['values']['mydate']['rrule'];
  $parts = split("\n", $rrule);
  foreach ($parts as $part) {
    if (substr($part, 0, 6) == "RRULE:") {
      $rule = $part;
    }
    elseif (substr($part, 0, 7) == "EXDATE:") {
      $parsed_exceptions = date_ical_parse_exceptions('', $part);
      foreach ($parsed_exceptions as $exception) {
        if (is_array($exception)) {
          $exceptions[] = date_format_date(date_make_date($exception['datetime']), 'custom', 'Y-m-d');
        }
      }
    }
    elseif (substr($part, 0, 6) == "RDATE:") {
      $parsed_additions = date_ical_parse_exceptions('', $part);
      foreach ($parsed_additions as $addition) {
        if (is_array($addition)) {
          // Modify the time to match our 'start time'
          $additions[] = date_make_date(substr($addition['datetime'], 0, 11) . $starttime);
        }
      }
    }
  }
  $parsed_rrule = date_ical_parse_rrule('', $rule);
  // Check for repeating dates
  if ($parsed_rrule['FREQ'] != "NONE") {
    // Calculate end date
    if (isset($parsed_rrule['UNTIL'])) {
      // Validation should ensure an 'until' date was entered if needed
      $enddate = date_make_date($parsed_rrule['UNTIL']['datetime'], $parsed_rrule['UNTIL']['tz']);
    }
    else {
      drupal_set_message(t("Validation error"));
    }
    // Generate list of dates from startdate, enddate, rules, exceptions, and additions.
    $dates = date_repeat_calc($rule, $startdate, $parsed_rrule['UNTIL']['datetime'], $exceptions, 'UTC', $additions);
  }
  else {
    // No repeating dates, just pass back startdate
    $dates = array($startdate);
  }
  // Duration and Back-to-back timeslot count
  $duration = $form_state['values']['duration'];
  $b2b = $form_state['values']['num_backtoback'];
  // Create duplicate date records for back-to-back timeslots
  if ($b2b > 1) {
    $newdates = $dates;
    for ($i=1; $i < $b2b; $i++) {
      foreach ($dates as $date) {
        $newdates[] = date_format_date(date_make_date($date . "+ " . ($i * $duration) . "minutes"), 'custom', 'Y-m-d H:i:s');
      }
    }
    $dates = $newdates;
  }
  $records = array();
  foreach ($dates as $date) {
    $starttime = strtotime($date);
    $endtime = strtotime($date . "+ " . $duration . " minutes");
    foreach ($fieldids as $fieldid) {
      if (variable_get('ult_ts_allfields', 0) == 1) {
        // Individual field IDs passed - obtain site/field numbers
        $sql = "Select nid, field from {ult_fields} where fid = %d";
        $result = db_query($sql, $fieldid);
        while ($data = db_fetch_array($result)) {
          $fieldnid = $data['nid'];
          $fieldnum = $data['field'];
        }
        $records[] = array('start' => $starttime, 'end' => $endtime, 'fieldnid' => $fieldnid, 'fieldnum' => $fieldnum, 'status' => 'Available');
      }
      else {
        // Fieldsite nid's passed - create timeslot for each field
        $sql = "Select field from {ult_fields} where nid = %d";
        $result = db_query($sql, $fieldid);
        while ($data = db_fetch_array($result)) {
          $fieldnum = $data['field'];
          $records[] = array('start' => $starttime, 'end' => $endtime, 'fieldnid' => $fieldid, 'fieldnum' => $fieldnum, 'status' => 'Available');
        }
      }
    }
  }
  // TODO:  Add League
  // Check for conflicts with existing timeslots
  $conflictdates = array();
  $writtendates = array();
  foreach ($records as $record) {
    // Ensure field timeslot doesn't conflict
    if (!_ult_timeslots_hasconflicts($record)) {
      // No conflicts. Write to db
      drupal_write_record('ult_timeslots', $record);
      $writtendates[] = array(format_date($record['start'], 'small'), format_date($record['end'], 'small'), ult_field_theme($record['fieldnid'], $record['fieldnum']));
    }
    else {
      // Conflicting timeslot.  Add to exceptions array
      $conflictdates[] = array(format_date($record['start'], 'small'), format_date($record['end'], 'small'), ult_field_theme($record['fieldnid'], $record['fieldnum']));
    }
  }
  // Generate output message
  if (count($writtendates) > 0) {
    $output = '<fieldset class="collapsible collapsed"><legend>';
    $output .= count($writtendates) . t(" new timeslots created.");
    $output .= '</legend>';
    $output .= theme_table(array(t("Start"), t("End"), t("Field")), $writtendates);
    $output .= '</fieldset>';
    drupal_set_message($output);
  }
  if (count($conflictdates) > 0) {
    $output = (count($conflictdates) . t(" timeslots could not be created, due to conflicts/overlap with already existing timeslots. "));
    $output .= '<fieldset class="collapsible collapsed"><legend>';
    $output .= t("View conflicting timeslots");
    $output .= '</legend>';
    $output .= theme_table(array(t("Start"), t("End"), t("Field")), $conflictdates);
    $output .= '</fieldset>';
    drupal_set_message($output);
  }
}

/**
 * Returns whether a given timeslot record conflicts with any existing records
 */
function _ult_timeslots_hasconflicts($record) {
  $sql = "Select count(*) from {ult_timeslots} where (fieldnid = %d and fieldnum = %d
    and (((start < %d) and (end >= %d))
    or ((start <= %d) and (end > %d))
    or ((start >= %d) and (end <= %d))))";
  $result = db_result(db_query($sql, $record['fieldnid'], $record['fieldnum'], $record['end'], $record['end'], $record['start'], $record['start'], $record['start'], $record['end']));
  if ($result > 0) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

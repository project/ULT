<?php

/**
 * @file
 * Page generation functions related to the ULT_Standings module.
 *
 */

/**
 * Generates the league/division standings page
 *
 * @param $node
 *   The league/division node object
 *
 * @return $output
 *   The rendered page output
 */
function ult_standings_byleague($node) {
  if (!(in_array($node->type, array('ult_league', 'ult_division')))) {
    // We know this check will fail, but doing it this way gives us a single location
    // to modify error handling for wrong content types
    _ult_main_checktype($node, 'ult_league');
  }
  $output .= _ult_main_returnlink($node);
// Called from menu - therefore, we display all sessions if needed
  if (variable_get('ult_standings_sessions', 1) == 1) {
    // Display all sessions
    // Get current session
    $session = ult_standings_getcurrentsession($node->nid);
    for ($i=$session; $i>0; $i--) {
      $output .= '<fieldset class="collapsible"><legend>';
      $output .= t('@league Standings - Session !session', array('@league' => $node->title, '!session' => $i));
      $output .= '</legend>';
      $output .= '<div class="fieldset-wrapper">';
      $output .= ult_standings_league_standings($node, $i);
      $output .= '</div></fieldset>';
    }
  }
  else {
    // Display all as one session
    $output .= '<fieldset class="collapsible"><legend>' . t('Division Standings') . '</legend>';
    $output .= '<div class="fieldset-wrapper">';
    $output .= ult_standings_league_standings($node);
    $output .= '</div></fieldset>';
  }
  return $output;
}

/**
 * Generates the standings table for a given division/session combo
 *
 * @param $node
 *   The league/division node for which we're generating stats
 * @param $session_id
 *   The session number for which we're generating stats
 *
 * @return $output
 *   The rendered standings table (or a 'standings not available' message)
 */
function ult_standings_league_standings($node, $session_id=0) {
  if (!(in_array($node->type, array('ult_league', 'ult_division')))) {
    // We know this check will fail, but doing it this way gives us a single location
    // to modify error handling for wrong content types
    _ult_main_checktype($node, 'ult_league');
  }
  // Generate standings header
  $header = array("Team");
  if (variable_get('ult_standings_showgamesplayed', 1) == 1) {
    $header[] = t("Games Played");
  }
  $header[] = t("Wins");
  $header[] = t("Losses");
  if (variable_get('ult_standings_showties', 1) == 1) {
    $header[] = t("Ties");
  }
  if (variable_get('ult_standings_showdefaults', 0) == 1) {
    $header[] = t("Defaults");
  }
  if (variable_get('ult_standings_showpfpa', 1) == 1) {
    $header[] = t("Points For");
    $header[] = t("Points Against");
  }
  if (variable_get('ult_standings_showtotal', 1) == 1) {
    $header[] = t("Total");
  }
  if (variable_get('ult_standings_showpercent', 1) == 1) {
    $header[] = t("Percentage");
  }
  if (variable_get('ult_standings_showspirit', 0) == 1) {
    $header[] = t("Average Spirit");
  }
  // Initialize Rows Array
  $rows = array();
  // Determine display order
  $disporder = variable_get('ult_standings_disporder', 1);
  switch ($disporder) {
    case 0:
      $order = "team";
      break;
    case 2:
      $order = "gamesplayed desc";
      break;
    case 3:
      $order = "wins desc";
      break;
    case 4:
      $order = "percentage Desc";
      break;
    case 5:
      $order = "pointsfor Desc";
      break;
    case 5:
      $order = "pointsagainst asc";
      break;
    case 1:
    default:
      $order = "totalpoints desc";
      break;
  }
  // Initialize Session
  if ($session_id == 0) {
    // No session specified ... get all sessions
    $sql = "Select a.team_id, a.gamesplayed, a.wins, a.losses, a.ties, a.defaults, a.pointsfor, a.pointsagainst, a.totalpoints, a.spiritavg, b.title as team, FORMAT((a.wins / a.gamesplayed), 3) as percentage from {ult_standings_results} a, {node} b where a.league_id = %d and a.team_id = b.nid Order by %s";
    $result = db_query($sql, $node->nid, $order);
  }
  else {
    $session = intval(check_plain($session_id));
    $sql = "Select a.team_id, a.gamesplayed, a.wins, a.losses, a.ties, a.defaults, a.pointsfor, a.pointsagainst, a.totalpoints, a.spiritavg, b.title as team, FORMAT((a.wins / a.gamesplayed), 3) as percentage from {ult_standings_results} a, {node} b where a.league_id = %d and a.session = %d and a.team_id = b.nid Order by %s";
    $result = db_query($sql, $node->nid, $session, $order);
  }
  // Load standings for division
  while ($data = db_fetch_object($result)) {
    $rows[$data->team_id]['name'] = $data->team;
    if (variable_get('ult_standings_showgamesplayed', 1) == 1) {
      $rows[$data->team_id]['gp'] += $data->gamesplayed;
    }
    $rows[$data->team_id]['w'] += $data->wins;
    $rows[$data->team_id]['l'] += $data->losses;
    if (variable_get('ult_standings_showties', 1) == 1) {
      $rows[$data->team_id]['t'] += $data->ties;
    }
    if (variable_get('ult_standings_showdefaults', 0) == 1) {
      $rows[$data->team_id]['d'] += $data->defaults;
    }
    if (variable_get('ult_standings_showpfpa', 1) == 1) {
      $rows[$data->team_id]['pf'] += $data->pointsfor;
      $rows[$data->team_id]['pa'] += $data->pointsagainst;
    }
    if (variable_get('ult_standings_showtotal', 1) == 1) {
      $rows[$data->team_id]['tp'] += $data->totalpoints;
    }
    if (variable_get('ult_standings_showpercent', 1) == 1) {
      if ($rows[$data->team_id]['gp'] > 0) {
        $percent = number_format(($rows[$data->team_id]['w'] / $rows[$data->team_id]['gp']), 3);
        $rows[$data->team_id]['p'] = $percent;
      }
      else {
        $rows[$data->team_id]['p'] = 0;
      }
    }
    if (variable_get('ult_standings_showspirit', 0) == 1) {
      $rows[$data->team_id]['sp'] = $data->spiritavg;
    }
  }
  // Do not output a table if we have no data for it
  if (count($rows) > 1) {
    $output = theme('table', $header, $rows);
  }
  else {
    $output .= t("No Standings Available.") . "<br /><br />";
  }
  return $output;
}

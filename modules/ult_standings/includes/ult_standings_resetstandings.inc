<?php

/**
 * @file
 * Page generation functions related to the ULT_Standings module.
 *
 */

/**
 * Generates the league/division "standings reset" page
 *
 * @param $node
 *   The node object for the league/division being referenced
 *
 *  @return $output
 *    The rendered page output
 *
 */
function ult_standings_resetstandings($node) {
  // Check that a league/division node was passed
 if (!(in_array($node->type, array('ult_league', 'ult_division')))) {
    // We know this check will fail, but doing it this way gives us a single location
    // to modify error handling for wrong content types
    _ult_main_checktype($node, 'ult_league');
  }
  // Render reset confirmation form
  $output .= _ult_main_returnlink($node);
  $output .= drupal_get_form('ult_standings_standings_reset_form', $node);
  return $output;
}

/**
 * Implementation of hook_form()
 */
function ult_standings_standings_reset_form($form_state, $node) {
  $form['reset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Standings Reset'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['reset']['division'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );
  // TODO:  Surround reset section with a fieldset to separate from other admin functions
/*
  $form['resetdate'] = array(
    '#type' => 'date_popup',
    '#title' => t('Select the start date for the new standings session'),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-1+1',
    '#required' => true,
  );
*/
  $confirmtext = "<br />";
  $confirmtext .= t("Some leagues choose to shuffle teams to re-balance the divisions at various points during a season. ");
  $confirmtext .= t("ULT accomodates this through the use of 'Standings sessions'.");
  $confirmtext .= "<br /><br />";
  $confirmtext .= t("Click on the button below to start a new session, which will reset the standings and begin tracking with the new teams. ");
  $confirmtext .= t("Note:  There is no confirmation page, and this can not be undone!");
  $confirmtext .= "<br /><br />";
  $form['reset']['confirmtext'] = array(
    '#type' => 'markup',
    '#value' => $confirmtext,
    '#prefix' => '<div>',
  );
  if ($node->type = 'ult_division') {
    $buttontext = t("Start New Standings Session for Division: @division", array('@division' => $node->title));
  }
  else {
    $buttontext = t("Start New Standings Session for League: @league", array('@league' => $node->title));
  }
  $form['reset']['submit'] = array(
    '#type' => 'submit',
    '#value' => $buttontext,
    '#suffix' => '</div>',
  );
  return $form;
}

/**
 * Implements hook_form_validate()
 */
function ult_standings_standings_reset_form_validate($form, &$form_state) {
  // Check that a league/division node was passed
  $lid = $form_state['values']['division'];
  $node = node_load($lid);
  if (!(in_array($node->type, array('ult_league', 'ult_division')))) {
    // We know this check will fail, but doing it this way gives us a single location
    // to modify error handling for wrong content types
    _ult_main_checktype($node, 'ult_league');
  }
}

/**
 * Implements hook_form_submit()
 */
function ult_standings_standings_reset_form_submit($form, &$form_state) {
  $lid = $form_state['values']['division'];
  ult_standings_newsession($lid);
}


/**
 * Updates Session for a given division
 *
 * Initiates a new session and updates related database tables
 *
 * @param $lid
 *   The node id of the node object representing the referenced league/division
 */
function ult_standings_newsession($lid) {
  $lid = intval(check_plain($lid));
  // Check that passed node is actually a division
  $node = node_load($lid);
  if (!(in_array($node->type, array('ult_league', 'ult_division')))) {
    // We know this check will fail, but doing it this way gives us a single location
    // to modify error handling for wrong content types
    _ult_main_checktype($node, 'ult_league');
  }
  // Get time for session start/end
  $currtime = time();
  // Determine Current Session ID
  $sql = "Select max(session) from {ult_standings_sessions} where league_id = %d";
  if ($session = db_result(db_query($sql, $lid))) {
    // Close current session in Session Table
    $sql = "Update {ult_standings_sessions} set enddate = %d where league_id = %d and session = %d";
    if (!($result = db_query($sql, $currtime, $lid, $session))) {
      watchdog('ult_standings', t("DB error closing new league standings session for league !lid", array('!lid' => $lid)), WATCHDOG_ERROR );
    }
  }
  else {
    // No existing initial session, create entry and close session
    $session = 1;
    $sql = "Insert into {ult_standings_sessions} (league_id, session, startdate, enddate) values (%d, %d, %d, %d)";
    if (!($result = db_query($sql, $lid, $session, $node->created, $currtime))) {
      watchdog('ult_standings', t("DB error closing new division standings session for division !lid", array('!lid' => $lid)), WATCHDOG_ERROR );
    }
  }
  // Update existing session
  ult_standings_updatestandings($lid, $session);
  // Create new Session entry
  $sql =  "Insert into {ult_standings_sessions} (league_id, session, startdate) values (%d, %d, %d)";
  if (!($result = db_query($sql, $lid, intval($session)+1, $currtime ))) {
        watchdog('ult_standings', t("DB error opening new division standings session for division !lid", array('!lid' => $lid)), WATCHDOG_ERROR );
  }
  drupal_set_message(t("New standings session created."));
}


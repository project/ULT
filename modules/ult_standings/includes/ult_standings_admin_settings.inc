<?php

/**
 * @file
 * Administrative settings for ULT_Standings module
 */

/**
 * Implements hook_form().
 *
 * @return
 *   An array containing the form elements to be displayed in the settings form.
 */
function ult_standings_settings_form($form_state) {
  $form = array();
  /* - Calculation Parameters
   *    - Standings by League
   *    - Standings by Division
   *    - Both                          */
/*  $form['ult_standings_format'] = array(
    '#type' => 'radios',
    '#title' => t('Standings Format'),
    '#default_value' => variable_get('ult_standings_format', 0),
    '#options' => array('0'=>t('Standings by Division'), '1'=>t('Standings by League'), '2'=>t('Standings by both League and Division')),
    '#description' => t('Provide a single standings table for the entire league, individual standings by division, or both?'),
    '#disabled' => true,
    '#weight' => -25,
  );
*/
  /* - Standings Calculation
   *    - Include All Games in a single session
   *    - Break season into multiple 'sessions' (to accomodate tier shuffles)  */
  $form['ult_standings_sessions'] = array(
    '#type' => 'radios',
    '#title' => t('Standings Calculation'),
    '#default_value' => variable_get('ult_standings_sessions', 1),
    '#options' => array('0' => t('Include all games in a single session'), '1' => t('Break season into multiple sessions (to accomodate tier/division shuffles)')),
    '#description' => t('Should ULT include all games in a single standings calculation, or divide the season into multiple "sessions"?'),
    '#weight' => -19,
  );
  /* - Update Frequency
   *    - Hourly
   *    - Once Daily
   *    - Twice Daily
   *    - Weekly                */
  $form['ult_standings_update'] = array(
    '#type' => 'radios',
    '#title' => t('Standings Update Frequency'),
    '#default_value' => variable_get('ult_standings_update', 1),
    '#options' => array('0' => t('Hourly'), '1' => t('Daily'), '3' => t('Weekly')),
    '#description' => t('How often should ULT update the standings table?'),
    '#disabled' => TRUE,
    '#weight' => -17,
  );
  /* - Display 'Games Played' Column */
  $form['ult_standings_showgamesplayed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display "Games Played"'),
    '#default_value' => variable_get('ult_standings_showgamesplayed', 1),
    '#description' => t('Include a "Games Played" column in the standings output?'),
    '#weight' => -15,
  );
  /* - Display 'Ties' Column */
  $form['ult_standings_showties'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display "Ties"'),
    '#default_value' => variable_get('ult_standings_showties', 1),
    '#description' => t('Include a "Ties" column in the standings output?'),
    '#weight' => -13,
  );
  /* - Display 'Defaults' Column */
  $form['ult_standings_showdefaults'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display "Defaults"'),
    '#default_value' => variable_get('ult_standings_showdefaults', 0),
    '#description' => t('Include a "Defaults" column in the standings output?'),
    '#weight' => -11,
  );
  /* - Display Points 'for' and 'against' */
  $form['ult_standings_showpfpa'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display "Points For/Against"'),
    '#default_value' => variable_get('ult_standings_showpfpa', 1),
    '#description' => t('Include "Points For" and "Points Against" columns in the standings output?'),
    '#weight' => -9,
  );
  /* - Display 'total' points
   *    - Points per win
   *    - Points per tie
   *    - Points per loss
   *    - Deduct points for default    */
  $form['ult_standings_showtotal'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display "Total Points"'),
    '#default_value' => variable_get('ult_standings_showtotal', 1),
    '#description' => t('Include a "Total Points" column (based on points per game/win/tie) in the standings output?'),
    '#weight' => -7,
  );

  $form['points'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('"Total Point" Game Values'),
    '#weight' => -5,
  );

  $form['points']['ult_standings_ptperwin'] = array(
    '#type' => 'textfield',
    '#title' => t('Points per win'),
    '#size' => '5',
    '#default_value' => variable_get('ult_standings_ptperwin', 2),
  );
  $form['points']['ult_standings_ptpertie'] = array(
    '#type' => 'textfield',
    '#title' => t('Points per tie'),
    '#size' => '5',
    '#default_value' => variable_get('ult_standings_ptpertie', 1),
  );
  $form['points']['ult_standings_ptperloss'] = array(
    '#type' => 'textfield',
    '#title' => t('Points per loss'),
    '#size' => '5',
    '#default_value' => variable_get('ult_standings_ptperloss', 0),
  );
  $form['points']['ult_standings_ptperdefault'] = array(
    '#type' => 'textfield',
    '#title' => t('Points deducted for defaulted games'),
    '#default_value' => variable_get('ult_standings_ptperdefault', 0),
  );
  /* - Display Percentage */
  $form['ult_standings_showpercent'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Percentage'),
    '#default_value' => variable_get('ult_standings_showpercent', 1),
    '#description' => t('Include a "Win Percentage" column in the standings output?'),
    '#weight' => -3,
  );
  /* - Display Order */
  $options = array('0' => t('Alphabetical'), '1' => t('Total Points'), '2' => t('Games Played'), '3' => t('Total Wins'), '4' => t('Percentage'), '5' => t('Points For'), '6' => t('Points Against'));
  $form['ult_standings_disporder'] = array(
    '#type' => 'radios',
    '#title' => t('Display Teams Ordered by'),
    '#default_value' => variable_get('ult_standings_disporder', 1),
    '#options' => $options,
    '#description' => t('In what order shall teams be listed in the standings display?'),
    '#weight' => -1,
  );
  return system_settings_form($form);
}

<?php

/**
 * @file
 * Page generation functions related to Ult_Divisions module.
 */

/**
 * Generates the 'Create ULT Divisions' page
 *
 * @param $node
 *   A $node object representing the league in which the division is being created
 *
 * @return $output
 *   The rendered 'Create Division' page
 */
function ult_divisions_division_create_page($node) {
  _ult_main_checktype($node, 'ult_league');
  $output .= _ult_main_returnlink($node);
  $output .= '<div class="ult-divisioncreate-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t("Create New Division") . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  global $user;
  $node = new stdClass();
  $node->name = $user->name;
  $node->uid = $user->uid;
  $node->type = 'ult_division';
  module_load_include('inc', 'node', 'node.pages');
  $output .= drupal_get_form('ult_division_node_form', $node);
  $output .= '</div></fieldset></div>';
  return $output;
}

/**
 *  Generates the 'edit' page for an individual 'ULT Division' node
 *
 *  @param $node
 *    The node object for the division being edited
 *
 *  @return $output
 *    The rendered 'edit division' page
 */
function ult_divisions_division_edit_page($node) {
  _ult_main_checktype($node, 'ult_division');
  $output .= _ult_main_returnlink($node);
  $output .= '<div id="ult-divisionedit-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Division Information') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  module_load_include('inc', 'node', 'node.pages');
  $output .= drupal_get_form('ult_division_node_form', $node);
  $output .= '</div></fieldset></div>';
  return $output;
}

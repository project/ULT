<?php
// $Id$

/**
 * @file
 * 'Assign Fields' page for a given league/division.
 */
// ********************************************************
// *                                                      *
// *         Page Declarations                            *
// *                                                      *
// ********************************************************
/**
 * Generates the 'Assign Fields' page for a given league/division
 *
 * @param $node
 *   A node object representing the parent league/division to which the
 *     fields are being assigned
 *
 * @return $output
 *   The rendered 'Assign Fields' page
 */
function ult_fields_assignfields_page($node) {
  drupal_add_css(drupal_get_path('module', 'ult_main') . '/ult_main.css');
  $output .= _ult_main_returnlink($node);
  $output .= drupal_get_form('ult_fields_assignfields_form', $node);
  return $output;
}

// ********************************************************
// *                                                      *
// *         Form Declarations                            *
// *                                                      *
// ********************************************************
/**
 * Implements of hook_form().
 *
 * @param $form_state
 *   The form state array.
 * @param $node
 *    A node object representing the parent league/division to which the
 *     fields are being assigned
 *
 * @return An array containing the form elements to be displayed in the node edit form.
 */
function ult_fields_assignfields_form($form_state, $node) {
  $form = array();
  $form['ult'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field Assignment'),
  );
  // Retrieve field sites
  $fids = array();
  $sql = "Select nid from {node} where type='ult_fieldsite'";
  $result = db_query($sql);
  while ($data = db_fetch_object($result)) {
    $fids[] = $data->nid;
  }
  // Ensure at least one field exists
  if (count($fids) == 0) {
    $form['ult']['nofields'] = array(
      '#type' => 'markup',
      '#value' => t('There have not yet been any fields created on this site.  Please create at least one field (i.e. content type "ult_fieldsite") before attempting to assign fields.'),
    );
    return $form;
  }
  // Get Active fids
  $checkedfids = array();
  $checkedfids = _ult_fields_getassignedfields($node->nid);
  // Build Form
  $options = array();
  foreach ($fids as $fid) {
    $options[$fid] = ult_fieldsite_theme($fid, FALSE);
  }
  $form['ult']['leagueid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );
  $form['ult']['availfields'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Available Fields'),
    '#default_value' => array_keys($checkedfids),
    '#options' => $options,
    '#description' => t('Check each field which you would like to make available to this division.'),
  );
  $form['ult']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Assign Fields'),
  );
  return $form;
}

// ********************************************************
// *                                                      *
// *         Form Submission                              *
// *                                                      *
// ********************************************************
/**
 * Submits the ult_fields_assignfields_form form
 *
 * @param $form form to submit
 * @param $form_state A keyed array containing the current state of the form.
 */
function ult_fields_assignfields_form_submit($form, &$form_state) {
  // Delete the existing field associations for this division
  $lid = $form_state['values']['leagueid'];
  $sql = "Delete from {ult_fieldassignments} where lid = %d";
  db_query($sql, $lid);
  foreach ($form_state['values']['availfields'] as $fid) {
    if ($fid != 0) {
      $entry = array('lid' => $lid, 'fid' => $fid);
      drupal_write_record('ult_fieldassignments', $entry);
    }
  }
  drupal_set_message(t("Field assignments updated"));
}

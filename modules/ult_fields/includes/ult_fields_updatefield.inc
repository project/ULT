<?php

/**
 * @file
 * 'Update field conditions' routines for the ULT_Fields module.
 */
// ********************************************************
// *                                                      *
// *         Page Declarations                            *
// *                                                      *
// ********************************************************
/**
 * Generates the 'update field conditions' page for an individual fieldsite
 *
 * @param $node
 *   The node object for the fieldsite being updated
 *
 * @return $output
 *   The rendered 'update fieldsite conditions' page
 */
function _ult_fields_field_update_page($node) {
  _ult_main_checktype($node, 'ult_fieldsite');
  $output .= _ult_main_returnlink($node);
  $output .= drupal_get_form('ult_fields_updatefield_form', $node);
  return $output;
}

// ********************************************************
// *                                                      *
// *         Form Declarations                            *
// *                                                      *
// ********************************************************
/**
 * Implements hook_form().
 *
 * @param $form_state The form state array.
 * @param $node
 *   The node object for the fieldsite being updated
 *
 * @return array
 *   An array containing the form elements to be displayed in the node edit form.
 */
function ult_fields_updatefield_form($form_state, $node) {
  $form = array();
  $description = t('Use the drop-down form below to update the current field condition status for this field.');
  $description .= '<br />';
  $description .= t('Field: ');
  $description .= check_plain($node->title);
  $description .= '<br />';
  $description .= t('Current Status: ');
  $description .= $node->field_ult_fieldrating[0]['value'];
  $form['ult'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update Field Conditions'),
    '#description' => $description,
  );
  $form['ult']['fieldid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );
  $form['ult']['status'] = array(
    '#type' => 'hidden',
    '#value' => $node->field_ult_fieldrating[0]['value'],
  );
  $description = t('Please provide a rating which represents the current condition of this field');
  $options = array(
    t('A - Field is top-quality') => t('A - Field is top-quality'),
    t('B - Field is in good condition') => t('B - Field is in good condition'),
    t('C - Field is acceptable for use') => t('C - Field is acceptable for use'),
    t('D - Field is in poor condition') => t('D - Field is in poor condition'),
    t('F - Field is in an unplayable condition') => t('F - Field is in an unplayable condition'),
    t('? - Field is in unknown condition') => t('? - Field is in unknown condition')
  );
  $form['ult']['newstatus'] = array(
    '#type' => 'select',
    '#title' => t('Current Status'),
    '#options' => $options,
    '#description' => $description,
  );
  $form['ult']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update Field Conditions'),
  );
  return $form;
}

// ********************************************************
// *                                                      *
// *         Form Validation                              *
// *                                                      *
// ********************************************************
/**
 * Validates the ult_fields_updatefield_form form
 *
 * @param $form form to validate
 * @param $form_state A keyed array containing the current state of the form.
 */
function ult_fields_updatefield_form_validate($form, &$form_state) {
  if ($form_state['values']['newstatus'] == $form_state['values']['status']) {
    form_set_error('newstatus', t('Your selection matches the currently reported condition for that field.  Please choose a different status if you wish to update the condition of this field.'));
  }
}

// ********************************************************
// *                                                      *
// *         Form Submission                              *
// *                                                      *
// ********************************************************
/**
 * Submit the ult_fields_updatefield_form form
 *
 * @param $form form to submit
 * @param $form_state A keyed array containing the current state of the form.
 */
function ult_fields_updatefield_form_submit($form, &$form_state) {
  $nid = check_plain($form_state['values']['fieldid']);
  $status = check_plain($form_state['values']['newstatus']);
  $node = node_load($nid);
  _ult_main_checktype($node, 'ult_fieldsite');
  $node->field_ult_fieldrating[0]['value'] = $status;
  node_save($node);
  drupal_set_message(t('Field conditions updated for @title.', array('@title' => $node->title)));
  $form_state['redirect'] = 'ult/field/' . $node->nid;
}

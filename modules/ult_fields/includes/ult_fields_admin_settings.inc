<?php

/**
 * @file
 * Administrative settings for the ULT_Fields module
 */

/**
 * Implements hook_form().
 *
 * @return An array containing the form elements to be displayed in the settings form.
 */
function ult_fields_config_form() {
  $form = array();
  $form['ult'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Field settings'),
  );
  // Per league/division field assignments
  $form['ult']['ult_fieldassignments'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow per league/division field assignments'),
    '#default_value' => variable_get('ult_fieldassignments', 1),
    '#description' => t('Allows administrators to assign specific fields to each league or division.') . '<br />' . t('If not set, all fields will available for all leagues.')
  );
  // User-updated field conditions
  $form['ult']['ult_fieldratings'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow users to update field conditions'),
    '#default_value' => variable_get('ult_fieldratings', 1),
    '#description' => t("If checked, users with the 'update field conditions' permission will be able to update field condition status via the site.")
  );
  return system_settings_form($form);
}

<?php

/**
 * @file
 * 'Edit fieldsite' page generation function for the ULT_Fields module.
 */
// ********************************************************
// *                                                      *
// *         Page Declarations                            *
// *                                                      *
// ********************************************************
/**
 * Generates the 'edit field' page for an individual 'ULT Field' node
 *
 * @param $node
 *   The fieldsite node being edited
 *
 * @return $output
 *   The rendered 'edit fieldsite' page
 */
function _ult_fields_field_edit_page($node) {
  _ult_main_checktype($node, 'ult_fieldsite');
  $output .= _ult_main_returnlink($node);
  $output .= '<div id="ult-fieldedit-container">';
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('Edit Field Information') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  module_load_include('inc', 'node', 'node.pages');
  $output .= drupal_get_form('ult_fieldsite_node_form', $node);
  $output .= '</div></fieldset></div>';
  return $output;
}

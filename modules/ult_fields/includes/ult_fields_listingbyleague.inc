<?php

/**
 * @file
 * 'Field listing' page generation function for the ULT_Fields module.
 */
// ********************************************************
// *                                                      *
// *         Page Declarations                            *
// *                                                      *
// ********************************************************
/**
 * Generates the 'field listing' page for an individual league/division
 *
 * @param $node
 *   A node object representing the container league/division
 *
 * @return $output
 *   The rendered 'field listing' page
 */
function _ult_fields_league_fields_page($node) {
  $output .= _ult_main_returnlink($node);
  $output .= '<fieldset class="collapsible"><legend>' . t('Fields Available to this ');
  if ($node->type == 'ult_league') {
    $output .= t('League');
  }
  elseif ($node->type == 'ult_division') {
    $output .= t('Division');
  }
  $output .= '</legend>';
  $output .= '<div class="fieldset-wrapper">';
  if (variable_get('ult_fieldassignments', 1) == 1) {
    // Return a view showing all fields used in this league/division
    $output .= views_embed_view('ult_fieldlist_byleague', 'default', $node->nid);
  }
  else {
    $output .= views_embed_view('ult_fieldlist', 'default');
  }
  $output .= '</div></fieldset>';
  return $output;
}

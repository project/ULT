<?php

/**
 * @file
 * Views_data description for the Ult_fields 'Field Assignments' table
 */

/**
 * Implementation of hook_views_data()
 */
function ult_fields_views_data() {
  $data['ult_fieldassignments']['table']['group'] = t('ULT Fields');
  $data['ult_fieldassignments']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'lid',
    ),
  );
  $data['ult_fieldassignments']['lid'] = array(
    'title' => t('League id'),
    'help' => t('The nid of the League this assignment belongs to.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('League Id'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );

  $data['ult_fieldassignments']['fid'] = array(
    'title' => t('Field id'),
    'help' => t('The nid of the field this assignment belongs to.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Field Id'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );
  return $data;
}


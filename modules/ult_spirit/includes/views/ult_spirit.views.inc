<?php

/**
 * @file
 * Views related functions for the ULT Spirit component of the 'Ultimate League Tool' suite
 */

/**
 * Implements hook_views_data()
 */
function ult_spirit_views_data() {
  $data = array();
  $data['ult_spirit_results']['table']['group'] = t('ULT Spirit');
  $data['ult_spirit_results']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'game_id',
    ),
  );
  $data['ult_spirit_results']['game_id'] = array(
    'title' => t('Game ID'),
    'help' => t('The nid matching a particular game'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Game ID'),
    ),
  );
  $data['ult_spirit_results']['home'] = array(
    'title' => t('Home Spirit'),
    'help' => t("Home team's spirit score."),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['ult_spirit_results']['away'] = array(
    'title' => t('Away Spirit'),
    'help' => t("Away team's spirit score."),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}

/**
 * Implementation of hook_views_default_views_alter().
 *
 * Adds spirit scores to the 'recent games' view
 */
function ult_spirit_views_default_views_alter(&$views) {
  // Only override the 'recent games' view
  if (array_key_exists('ult_recentgames', $views)) {
    // Only override if the settings allow the immediate display of spirit scores
    if (variable_get('ult_spirit_display', 2) == 1) {
      $views['ult_recentgames']->display['default']->display_options['relationships']['game_id'] = array(
        'label' => 'Game Spirit',
        'required' => 0,
        'id' => 'game_id',
        'table' => 'ult_spirit_results',
        'field' => 'game_id',
        'relationship' => 'field_ult_gameid_nid',
      );
      $fieldarray = $views['ult_recentgames']->display['default']->display_options['fields'];
      $home = array('home' => array(
        'label' => 'Home Spirit',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 1,
        'empty_zero' => 1,
        'set_precision' => FALSE,
        'precision' => 0,
        'decimal' => '.',
        'separator' => ',',
        'prefix' => '',
        'suffix' => '',
        'exclude' => 0,
        'id' => 'home',
        'table' => 'ult_spirit_results',
        'field' => 'home',
        'relationship' => 'game_id',
      ));
      // Insert Home Spirit in proper location
      $tmp1 = array_slice($fieldarray, 0, 7, TRUE);
      $tmp2 = array_slice($fieldarray, 7, (count($fieldarray) - 7));
      $fieldarray = array_merge($tmp1, $home, $tmp2);
      // Append Away Spirit on end of array
      $fieldarray['away'] = array(
        'label' => 'Away Spirit',
        'alter' => array(
          'alter_text' => 0,
          'text' => '',
          'make_link' => 0,
          'path' => '',
          'link_class' => '',
          'alt' => '',
          'prefix' => '',
          'suffix' => '',
          'target' => '',
          'help' => '',
          'trim' => 0,
          'max_length' => '',
          'word_boundary' => 1,
          'ellipsis' => 1,
          'html' => 0,
          'strip_tags' => 0,
        ),
        'empty' => '',
        'hide_empty' => 1,
        'empty_zero' => 1,
        'set_precision' => FALSE,
        'precision' => 0,
        'decimal' => '.',
        'separator' => ',',
        'prefix' => '',
        'suffix' => '',
        'exclude' => 0,
        'id' => 'away',
        'table' => 'ult_spirit_results',
        'field' => 'away',
        'relationship' => 'game_id',
      );
      $views['ult_recentgames']->display['default']->display_options['fields'] = $fieldarray;
    }
  }
}

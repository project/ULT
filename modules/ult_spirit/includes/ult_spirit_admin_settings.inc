<?php

/**
 * @file
 * Administrative settings for the ULT_Spirit module
 */

/**
 * Implements hook_form().
 *
 * @return
 *   An array containing the form elements to be displayed in the settings form.
 */
 function ult_spirit_settings_form($form_state) {
  $form = array();
  $form['ult'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Spirit Score settings'),
  );
  /*
   * Format:
   * - Number from 1 to 10
   * - Use RUFDC Spirit form
   */
  $form['ult']['ult_spirit_format'] = array(
    '#type' => 'radios',
    '#title' => t('Spirit Score Format'),
    '#default_value' => variable_get('ult_spirit_format', 1),
    '#options' => array('0' => t('Number between 1 and 10'), '1' => t('Use 5 Question Spirit Form')),
    '#description' => t('In what format should spirit scores be entered?'),
  );
  /*
   * Display Spirit Scores
   * - Never
   * - Immediately
   * - After Both teams have submitted
   */
  $form['ult']['ult_spirit_display'] = array(
    '#type' => 'radios',
    '#title' => t('Spirit Score Display'),
    '#default_value' => variable_get('ult_spirit_display', 2),
    '#options' => array('0' => t('Never (hide spirit scores)'), '1' => t('Immediately upon submission'), '2' => t('Only after both teams have submitted')),
    '#description' => t('Select if/when spirit scores should be displayed on the site'),
  );
    /*
   * Who can enter Spirit?
   * - Captains only
   * - Any Player on Team
   */
  $options = array('0' => t('Admin only'), '1' => t('Permission based ("Submit Spirit Results" permission)'));
  if (variable_get('ult_enablecaptains', 1) == 1) {
    $options[2] = t('Opposing Team Captain');
  }
  if (module_exists('ult_rosters')) {
    $options[3] = t('Any Player from the opposing team');
  }
  $form['ult']['ult_spiritsubmitter'] = array(
    '#type' => 'radios',
    '#title' => t('Who can enter Spirit Scores?'),
    '#default_value' => variable_get('ult_spiritsubmitter', 1),
    '#options' => $options,
    '#description' => t('Please select who will be allowed to enter a spirit score rating for a given team.'),
  );
  /*
   * Allow Editing of existing spirit score
   * - True / False
   */
  $form['ult']['ult_spirit_canedit'] = array(
    '#type' => 'radios',
    '#title' => t('Allow users to update spirit scores "after the fact"?'),
    '#default_value' => variable_get('ult_spirit_canedit', 1),
    '#options' => array('0' => t('Allow'), '1' => t('Admin only')),
    '#description' => t("Determines whether users with the 'alter spirit score' permission can change a spirit score after the initial submission."),
  );
  $form['ult']['defaultedgames'] = array(
    '#type' => 'fieldset',
    '#title' => t('Spirit handling for defaulted games (not yet implemented)'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  /*
   * Enable Spirit Penalties for not entering score
   * - True / False
   */
  $form['ult']['defaultedgames']['ult_spirit_defaultpenalty'] = array(
    '#type' => 'radios',
    '#title' => t('Enforce a spirit penalty for defaulted games?'),
    '#options' => array('0' => t('Apply spirit score penalty when a game is defaulted'), '1' => t('Do not apply a spirit score penalty when a game is defaulted')),
    '#default_value' => variable_get('ult_spirit_defaultpenalty', 1),
    '#description' => t('A spirit score penalty may be assessed against a team who can not field a team for a particular game'),
    '#disabled' => TRUE,
  );
  $form['ult']['defaultedgames']['ult_spirit_defaultpenaltyamount'] = array(
    '#type' => 'textfield',
    '#title' => t('Spirit Penalty amount for defaulted games?'),
    '#default_value' => variable_get('ult_spirit_defaultpenaltyamount', -3),
    '#description' => t('Only applies if spirit penalties are enabled above'),
    '#disabled' => TRUE,
  );
  /*
   * Allow Submission of Spirit Score for Default Games
   * - True
   * - False (with default)
   */
  $form['ult']['defaultedgames']['ult_spirit_defaults'] = array(
    '#type' => 'radios',
    '#title' => t('Spirit score handling for defaulted games'),
    '#default_value' => variable_get('ult_spirit_defaults', 1),
    '#options' => array('0' => t('Allow spirit score entries'), '1' => t('Assign default spirit score to defaulting team'), '2' => t('Assign default spirit score to both teams')),
    '#description' => t('Determines whether users can change a spirit score after the initial submission.'),
    '#disabled' => TRUE,
  );
  $form['ult']['defaultedgames']['ult_spirit_defaultingscore'] = array(
    '#type' => 'textfield',
    '#title' => t('Default spirit score to assign the defaulting team'),
    '#default_value' => variable_get('ult_spirit_defaultingscore', 0),
    '#disabled' => TRUE,
    '#description' => t('Default spirit score to assign the team who defaulted'),
  );
  $form['ult']['defaultedgames']['ult_spirit_defaultedscore'] = array(
    '#type' => 'textfield',
    '#title' => t('Default spirit score to assign the team who was defaulted against'),
    '#default_value' => variable_get('ult_spirit_defaultedscore', 5),
    '#description' => t('Default spirit score to assign the team who was defaulted against'),
    '#disabled' => TRUE,
  );
  return system_settings_form($form);
}

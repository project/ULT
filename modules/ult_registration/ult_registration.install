<?php

/**
 * @file
 * Install, update, and uninstall functions for the Ult_registration module
 */

/**
 * Implements hook_requirements().
 *
 * Requirements:
 *   - install_profile_api exists (doesn't need to be enabled)
 *   - content_copy exists (doesn't need to be enabled)
 */
function ult_registration_requirements($phase) {
  if ($phase == 'install') {
    $requirements = array();
    // Check that required modules files exist
    // Content Copy requirement
    if (!(file_exists('./' . drupal_get_path('module', 'content') . '/modules/content_copy/content_copy.module'))) {
      $requirements['ult_main_contentcopy_warning'] = array(
        'title' => t('Missing Dependency - '),
        'value' => t('Content Copy module not found'),
        'severity' => REQUIREMENT_ERROR,
        'description' => t('Installation of the "ULT Registration" module requires that the "Content_Copy" module exists on your server (though it need not be activated).'),
      );
    }
    // Install Profile API requirement
    if (!(file_exists('./' . drupal_get_path('module', 'install_profile_api') . '/contrib/content_copy.inc'))) {
      $requirements['ult_main_profileapi_warning'] = array(
        'title' => t('Missing Dependency - '),
        'value' => t('Install Profile API module not found'),
        'severity' => REQUIREMENT_ERROR,
        'description' => t('Installation of the "ULT Registration" module requires that the "Install Profile API" module exists on your server (though it need not be activated).'),
      );
    }
    return $requirements;
  }
}

// ********************************************************
// *                                                      *
// *                   INSTALL ROUTINES                   *
// *                                                      *
// ********************************************************

/**
* Implements hook_install()
*/
function ult_registration_install() {
  // **** Create/Update Required Content Types ****
  module_load_include('inc', 'ult_main', 'includes/ult_main_cckbuilder');
  _ult_main_create_ccktype('ult_team', 'ult_registration');
  _ult_main_create_ccktype('ult_league', 'ult_registration');
  if (module_exists('ult_divisions')) {
    _ult_main_create_ccktype('ult_division', 'ult_registration');
  }
}

/**
 * Implements hook_enable()
 * - Updates required 'registration status' value for teams that don't have one already set
 */
function ult_registration_enable() {
  // Update 'Registration Status' of any teams that do not have a 'status' set
  $sql = "UPDATE {content_type_ult_team} a SET a.field_ult_teamstatus_value = 'Initial Creation' where a.field_ult_teamstatus_value IS NULL";
  db_query($sql);
  // Update 'League Status' of any leagues/divisions that do not have a 'status' set
  // Check if seperate table exists for leaguestatus field or not
  if (db_table_exists('content_field_ult_leaguestatus')) {
    // Seperate field table, so run query against table content_field_ult_leaguestatus
    $sql = "UPDATE {content_field_ult_leaguestatus} a SET a.field_ult_leaguestatus_value = 'Initial' where a.field_ult_leaguestatus_value IS NULL";
  }
  else {
    // Seperate field table doesn't exist, so status is a column in content_type_ult_league
    $sql = "UPDATE {content_type_ult_league} a SET a.field_ult_leaguestatus_value = 'Initial' where a.field_ult_leaguestatus_value IS NULL";
  }
  db_query($sql);
}

// ********************************************************
// *                                                      *
// *                 UNINSTALL ROUTINES                   *
// *                                                      *
// ********************************************************

/**
* Implements hook_uninstall()
*/
function ult_registration_uninstall() {
  // Remove any variables set by the module
  variable_del('ult_register_deadlines');
  variable_del('ult_register_payments');
  variable_del('ult_register_approval');
  variable_del('ult_register_requirepayment');
  variable_del('ult_register_requireroster');
}

<?php

/**
 * @file
 * 'Register team' registration page and related functions
 */

/********************************************************
 *                                                      *
 *                Page Callbacks                        *
 *                                                      *
 ********************************************************/
/**
 * Generates the 'Register team' page
 *
 * @param $teamnode
 *   A node object representing the team being registered
 *
 * @return $output
 *   The rendered 'register team' page
 */
function ult_registrations_registerteam_page($teamnode) {
  _ult_main_checktype($teamnode, 'ult_team');
  $output .= _ult_main_returnlink($teamnode);
  if ($teamnode->field_ult_teamregistered[0]['value'] != "Registered") {
    $output .= drupal_get_form('ult_registrations_teamregister_form', $teamnode);
  }
  else {
    $output .= '<fieldset class="ult-fieldset"><legend>' . t('Registration Status') . '</legend>';
    $output .= '<div class="ult-fieldset-content">';
    $output .= _ult_registrations_teamregtable($teamnode);
    // Determine whether user can submit final registration
    if (variable_get('ult_register_approval', 1) == 0) {
      // If finalized, provide 'finalize Registration' button
      if ((_ult_registrations_checkprereqs($teamnode)) && ($teamnode->field_ult_teamstatus[0]['value'] != "Registration Rejected")) {
        // Render final registration form
        $output .= drupal_get_form('ult_registrations_finalizeregistration_form', $teamnode);
      }
    }
    $output .= '</div>';
    $output .= '</fieldset>';
    // Output Administrator registration form
    if (user_access('administer ULT leagues')) {
      $output .= drupal_get_form('ult_registrations_teamregadmin_form', $teamnode);
    }
  }
  return $output;
}

/********************************************************
 *                                                      *
 *                Form Declaration                      *
 *                                                      *
 ********************************************************/
/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 * @param $teamnode
 *   A node object representing the team being registered
 *
 * @return
 *   An array containing the form elements to be displayed in the form.
 */
function ult_registrations_teamregister_form($form_state, $teamnode) {
  $form = array();
  $description = t('By clicking the button below, you will be indicating your intention to register this team for inclusion in this league.  ');
  $description .= t('This is the first step of the registration process.');
  $description .= '<br /><br />';
  $description .= t('Please note, however, that this does not guarantee you a spot in the league, which may also be dependent on other requirements (such as the collection of a registration fee and/or submission of a completed roster, as an example).');
  $description .= '<br /><br />';
  $description .= t('If you are sure you want to continue, click the button below.  ');
  $description .= t('If this is not your intention, please use the back button on your browser to return to the previous screen.');
  $form['registercontainer'] = array( '#type' => 'fieldset', '#title' => t('Register Team'), '#description' => $description, );
  $form['registercontainer']['nodeid'] = array( '#type' => 'hidden', '#value' => $teamnode->nid, );
  $form['registercontainer']['register'] = array( '#type' => 'submit', '#name' => 'register', '#value' => t('Submit Registration'), '#prefix' => '<div>', '#suffix' => '</div>');
  return $form;
}

/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 * @param $teamnode
 *   A node object representing the team being registered
 *
 * @return
 *   An array containing the form elements to be displayed in the form.
 */
function ult_registrations_finalizeregistration_form($form_state, $teamnode) {
  $form = array();
  $form['nodeid'] = array('#type' => 'hidden', '#value' => $teamnode->nid);
  $form['submit'] = array('#type' => 'submit', '#value' => t('Confirm Final Registration'), );
  return $form;
}

/**
 * Implements hook_form().
 *
 * @param $form_state
 *   The form state array.
 * @param $node
 *   A node object representing the team being registered
 *
* @return
*   An array containing the form elements to be displayed in the form.
 */
function ult_registrations_teamregadmin_form($form_state, $node) {
  $form['teamregadmin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Registration Administration'),
    '#collapsible' => FALSE,
  );
  $form['teamregadmin']['nodeid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );
  $form['teamregadmin']['register'] = array(
    '#type' => 'submit',
    '#name' => 'register',
    '#value' => t('Registration Received'),
    '#disabled' => TRUE,
  );
  if ($node->field_ult_teamrostersubmitted[0]['value'] != "Roster Received") {
    $form['teamregadmin']['roster'] = array(
      '#type' => 'submit',
      '#name' => 'roster',
      '#value' => t('Roster Received'),
    );
  }
  else {
    $form['teamregadmin']['removeroster'] = array(
      '#type' => 'submit',
      '#name' => 'removeroster',
      '#value' => t('Remove Roster Status'),
    );
  }
  if ($node->field_ult_teampaid[0]['value'] != "Payment Received") {
    $form['teamregadmin']['payment'] = array(
      '#type' => 'submit',
      '#name' => 'payment',
      '#value' => t('Payment Received'),
    );
  }
  else {
    $form['teamregadmin']['removepayment'] = array(
      '#type' => 'submit',
      '#name' => 'removepayment',
      '#value' => t('Remove Paid Status'),
    );
  }
  $form['teamregadmin']['spacer'] = array(
    '#type' => 'markup',
    '#value' => '<br /><br />',
  );
  $form['teamregadmin']['label'] = array(
    '#type' => 'markup',
    '#value' => '<div class="ult_smaller_heading">' . t('Registration Decision') . '</div>',
  );
  $form['teamregadmin']['accept'] = array(
    '#type' => 'submit',
    '#name' => 'accept',
    '#value' => t('Accept/Confirm Registration'),
    '#disabled' => TRUE,
  );
  $form['teamregadmin']['reject'] = array(
    '#type' => 'submit',
    '#name' => 'reject',
    '#value' => t('Reject Registration'),
    '#disabled' => TRUE,
  );
  if ($node->field_ult_teamregistered[0]['value'] != "Registered") {
    $form['teamregadmin']['register']['#disabled'] = FALSE;
  }
  if ($node->field_ult_teamstatus[0]['value'] != "Registered") {
    $form['teamregadmin']['accept']['#disabled'] = FALSE;
  }
  if ($node->field_ult_teamstatus[0]['value'] != "Registration Rejected") {
    $form['teamregadmin']['reject']['#disabled'] = FALSE;
  }
  if (_ult_registrations_checkprereqs($node)) {
    $form['teamregadmin']['#description'] = t('Note:  All registration pre-requisites satisfied');
  }
  return $form;
}

/********************************************************
 *                                                      *
 *                Form Submission                       *
 *                                                      *
 ********************************************************/
/**
 * Processes submitted ult_registrations_teamregister_form form
 */
function ult_registrations_teamregister_form_submit($form, &$form_state) {
  $nid = check_plain($form_state['values']['nodeid']);
  $node = node_load(array('nid' => $nid));
  if ($node->type == 'ult_team') {
    $node->field_ult_teamregistered[0]['value'] = 'Registered';
    $node->field_ult_teamregisterdate[0]['value'] = date("Y-m-d H:i:s");
    if (($node->field_ult_teamstatus[0]['value'] == '') || ($node->field_ult_teamstatus[0]['value'] == 'Initial Creation')) {
      $node->field_ult_teamstatus[0]['value'] = 'Registration Pending';
    }
    drupal_set_message(t("Registration request submitted for team: ") . check_plain($node->title));
    node_save($node);
  }
}

/**
 * Processes the submitted ult_regiestrations_finalizeregistration form
 */
function ult_registrations_finalizeregistration_form_submit($form, &$form_state) {
  $nid = check_plain($form_state['values']['nodeid']);
  $node = node_load(array('nid' => $nid));
  if ($node->type == 'ult_team') {
    $node->field_ult_teamstatus[0]['value'] = 'Registered';
    drupal_set_message(t("Registration finalized for team: ") . check_plain($node->title));
    node_save($node);
  }
}

/**
 * Processes the submitted ult_registrations_teamregadmin_form form
 */
function ult_registrations_teamregadmin_form_submit($form, &$form_state) {
  $nid = check_plain($form_state['values']['nodeid']);
  $node = node_load(array('nid' => $nid));
  $action = check_plain($form_state['clicked_button']['#name']);
  if ($node->type == 'ult_team') {
    switch ($action) {
      case 'register':
        $node->field_ult_teamregistered[0]['value'] = 'Registered';
        $node->field_ult_teamregisterdate[0]['value'] = date("Y-m-d H:i:s");
        if ($node->field_ult_teamstatus[0]['value'] == 'Initial Creation') {
          $node->field_ult_teamstatus[0]['value'] = 'Registration Pending';
        }
        break;
      case 'roster':
        $node->field_ult_teamrostersubmitted[0]['value'] = 'Roster Received';
        $node->field_ult_teamrosterdate[0]['value'] = time();
        break;
      case 'removeroster':
        $node->field_ult_teamrostersubmitted[0]['value'] = 'False';
        $node->field_ult_teamrosterdate[0]['value'] = '';
        break;
      case 'payment':
        $node->field_ult_teampaid[0]['value'] = 'Payment Received';
        $node->field_ult_teampaiddate[0]['value'] = time();
        break;
      case 'removepayment':
        $node->field_ult_teampaid[0]['value'] = 'False';
        $node->field_ult_teampaiddate[0]['value'] = '';
        break;
      case 'accept':
        if ($node->field_ult_teamstatus[0]['value'] != 'Registered') {
          $node->field_ult_teamstatus[0]['value'] = 'Registered';
        }
        break;
      case 'reject':
        if ($node->field_ult_teamstatus[0]['value'] != 'Registration Rejected') {
          $node->field_ult_teamstatus[0]['value'] = 'Registration Rejected';
          // TODO:  Trigger Rejection Email to Team Captain
        }
        break;
    }
    node_save($node);
  }
}

/********************************************************
 *                                                      *
 *                Private Functions                     *
 *                                                      *
 ********************************************************/
/**
 * Generates the registration status table for the current team
 *
 * @param $node
 *   A node object representing the current team
 *
 * @return $output
 *   A rendered table containing the team's registration status
 */
function _ult_registrations_teamregtable($node) {
  $rows = array();
  $rows[0][0] = t('Status: ');
  if ($node->field_ult_teamstatus[0]['value'] != "Registered") {
    $rows[0][1] = '<span class="ult_important">' . $node->field_ult_teamstatus[0]['value'] . '</span>';
  }
  else {
    $rows[0][1] = '<span class="ult_notice">' . $node->field_ult_teamstatus[0]['value'] . '</span>';
  }
  if ($node->field_ult_teamregistered[0]['value'] == "Registered") {
    $rows[] = array(t('Initial Registration Submitted: '), content_format('field_ult_teamregisterdate', $node->field_ult_teamregisterdate[0], 'long'));
  }
  if (variable_get('ult_register_payments', 1) == 1) {
    if ($node->field_ult_teampaid[0]['value'] == "Payment Received") {
      $rows[] = array(t('Payment Received: '), content_format('field_ult_teampaiddate', $node->field_ult_teampaiddate[0], 'long'));
    }
    else {
      $rows[] = array(t('Payment Received: '), '<span class="ult_important">' . t('No') . '</span>');
    }
  }
  if ((module_exists('ult_rosters')) && (variable_get('ult_submitrosters', 0) == 1)) {
    if ($node->field_ult_teamrostersubmitted[0]['value'] == "Roster Received") {
      $rows[] = array(t('Roster Received: '), content_format('field_ult_teamrosterdate', $node->field_ult_teamrosterdate[0], 'long'));
    }
    else {
      $rows[] = array(t('Roster Received: '), '<span class="ult_important">' . t('No') . '</span>');
    }
  }
  if (variable_get('ult_register_approval', 1) == 1) {
    if ($node->field_ult_teamstatus[0]['value'] != "Registered") {
      $rows[] = array(t('Administrator Approval:'), '<span class="ult_important">' . t('Awaiting Confirmation') . '</span>');
    }
    else {
      $rows[] = array(t('Administrator Approval:'), t('Confirmed'));
    }
  }
  if ($node->field_ult_teamstatus[0]['value'] != 'Registered') {
    // Check registration deadline (if enforced)
    if (variable_get('ult_register_deadlines', 1) == 2) {
      if (_ult_registrations_deadlineexpired($node)) {
        drupal_set_message(t("This team's registration was not finalized prior to the expiry of the registration deadline.  Please contact an administrator to discuss any unsatisfied registration requirements."), 'error');
      }
    }
  }
  return theme_table(array(), $rows);
}

/**
 * Checks a team's registration pre-req requirements
 *
 * @param $teamnode
 *   A node object representing the given team
 *
 * @return boolean
 *   TRUE or FALSE depending on whether the registration pre-reqs have been
 *     satisfied
 */
function _ult_registrations_checkprereqs($teamnode) {
  // Check that all prereq conditions are met
  $prereqs = TRUE;
  if ((variable_get('ult_register_payments', 1) == 1)
  && (variable_get('ult_register_requirepayment', 1) == 1)
  && ($teamnode->field_ult_teampaid[0]['value'] != 'Payment Received')) {
    $prereqs = FALSE;
  }
  if ((variable_get('ult_submitrosters', 0) == 1)
  && (variable_get('ult_register_requireroster', 0) == 1)
  && ($teamnode->field_ult_teamrostersubmitted[0]['value'] != 'Roster Received')) {
    $prereqs = FALSE;
  }
  if ((variable_get('ult_register_deadlines', 1) == 2)
  && (_ult_registrations_deadlineexpired($teamnode))) {
    $prereqs = FALSE;
  }
  if ($teamnode->field_ult_teamstatus[0]['value'] == 'Registered') {
    $prereqs = FALSE;
  }
  return $prereqs;
}

/**
 * Checks whether a teams registration deadline has expired
 *
 * @param $teamnode
 *   A node object representing the referenced team
 *
 * @return boolean
 *   TRUE or FALSE, depending on the league registration deadline value
 */
function _ult_registrations_deadlineexpired($teamnode) {
  if (variable_get('ult_register_deadlines', 1) == 0) {
    return FALSE;
  }
  $parentnode = node_load($teamnode->field_ult_teamparent[0]['nid']);
  if ($parentnode->type == 'ult_division') {
    $parentnode = node_load($parentnode->field_ult_divisionleague[0]['nid']);
  }
  if ($parentnode->field_ult_leaguedeadline[0]['value'] == '') {
    return FALSE;
  }
  $deadline = strtotime($parentnode->field_ult_leaguedeadline[0]['value']);
  $current = time();
  if ($current < $deadline) {
    return FALSE;
  }
  return TRUE;
}

<?php

/**
 * @file
 * Page generation functions for the ult_registration module
 */

/********************************************************
 *                                                      *
 *                Page Callbacks                        *
 *                                                      *
 ********************************************************/
/**
 * Generates the 'registration status' page
 *
 * @param $node
 *   The node object for the parent league/division who's status is queried
 *
 * @return $output
 *   The rendered 'registration status' page
 */
function ult_main_league_regstatus_page($node) {
  $output .= _ult_main_returnlink($node);
  $output .= '<fieldset class="ult-fieldset"><legend>' . t('League Registration Status') . '</legend>';
  $output .= '<div class="ult-fieldset-content">';
  $output .= '<div id="ult_currentstatus">' . t('Current Status:') . ' <span id="ult_currentstatus_value">';
  if ($node->field_ult_leaguestatus[0]['value']) {
    $output .= $node->field_ult_leaguestatus[0]['value'];
  }
  else {
    $output .= t('Initial');
  }
  $output .= '</span></div>';
  $output .= drupal_get_form('ult_registration_changestatus_form', $node, $node->field_ult_leaguestatus[0]['value']);
  $output .= _ult_registration_leaguestatus_defs();
  $output .= '</div></fieldset>';
  return $output;
}

/**
 * Generates the 'Register'/'Add Team' page
 *
 * @param $node
 *   The rendered 'Registration'/'Add Team' page
 */
function ult_registration_leagueregistration_page($node) {
  if ($node->type == 'ult_division') {
    $newteamlink = 'ult/division/' . $node->nid . '/newteam';
  }
  elseif ($node->type == 'ult_league') {
    $newteamlink = 'ult/league/' . $node->nid . '/newteam';
  }
  $output .= _ult_main_returnlink($node);
  if ($node->field_ult_leaguestatus[0]['value'] == "Locked") {
    // Output 'registration closed' or 'private league'
    $output .= '<div id="ult_division-registrationstatus">';
    $output .= '<fieldset class="collapsible"><legend>' . t('Registration Status') . '</legend>';
    $output .= '<div><br /><span class="ult_note">' . t('Registration for this division is currently closed.') . '</span><br /><br /></div>';
    if (user_access('administer ULT leagues')) {
      $output .= l(t('Click here to create a new team'), $newteamlink);
    }
    $output .= '</fieldset>';
    $output .= '</div>';
  }
  else {
    // Display 'Add Team' and Registration options
    $output .= '<div id="ult_division-registration">';
    $output .= '<fieldset class="collapsible"><legend>' . t('Registration Options') . '</legend>';
    $output .= '<div><br />';
    $output .= t('This division is currently open for registration.');
    $output .= '<br /><br />';
    $output .= t('The first step of the registration process is to create a team entry and submit it via this site.');
    $output .= '<br /><br />';
    global $user;
    if ($user->uid == 0) {
      $output .= l(t('Please login to create a new team'), 'user/login');
    }
    else {
      $output .= l(t('Click here to create a new team'), $newteamlink);
    }
    $output .= '<br /><br />';
    $output .= t('You may also be required to create and submit a team roster and payment before your registration is considered complete.');
    $output .= '<br /><br />';
    $output .= t('Be aware that the registration deadline for this league is currently: ');
    if ($node->type == 'ult_league') {
      $output .= date_format_date(date_make_date($node->field_ult_leaguedeadline[0]['value']), 'medium');
    }
    else {
      $leaguenode = node_load($node->field_ult_divisionleague[0]['nid']);
      $output .= date_format_date(date_make_date($leaguenode->field_ult_leaguedeadline[0]['value']), 'medium');
    }
    $output .= '.<br /><br /></div>';
    $output .= '</fieldset>';
    $output .= '</div>';
  }
  return $output;
}

/**
* Implements hook_form()
*
* Form used to update thet status of a given league or division
*
* @param $form_state
* @param $node
*   The node object representing the league or division being updated
* @param $currentstatus
*   A string representing the current status of the division/node.  Defaults to 'Initial'
*
*  @return $form
*/
function ult_registration_changestatus_form($form_state, $node, $currentstatus = 'Initial') {
  // Ensure passed node is a valid league or division object
  if (!($currentstatus)) {
    $currentstatus = 'Initial';
  }
  if ((!in_array($node->type, array('ult_league', 'ult_division'))) || (!in_array(check_plain($currentstatus), array('Initial', 'Open', 'Locked', 'Active', 'Closed', 'Archived')))) {
    drupal_set_message(t('An error was encountered while creating the "Change Status" form.'), 'warning');
    return FALSE;
  }
  // Determine valid state transition values
  if ($currentstatus == 'Initial') {
    $options = array('Open Registrations', 'Lock');
  }
  elseif ($currentstatus == 'Open') {
    $options = array('Lock', 'Activate');
  }
  elseif ($currentstatus == 'Locked') {
    $options = array('Open Registrations', 'Activate');
  }
  elseif ($currentstatus == 'Active') {
    $options = array('Lock', 'Close');
  }
  elseif ($currentstatus == 'Closed') {
    $options = array('Activate', 'Archive');
  }
  elseif ($currentstatus == 'Archived') {
    $options = array('Restore');
  }

  // Define Form
  $form['status'] = array( '#type' => 'hidden', '#value' => check_plain($currentstatus), );
  $form['nodeid'] = array( '#type' => 'hidden', '#value' => $node->nid, );
  $form['actions'] = array( '#type' => 'fieldset', '#title' => t('Change Status'), '#collapsible' => TRUE, '#collapsed' => FALSE, );
  if ($node->type == 'ult_league') {
    $form['actions']['#description'] = t('NOTE: Changing the status of a league will also modify the status of all divisions within that league!') . '<br /><br />';
  }
  if (in_array('Open Registrations', $options)) {
    $form['actions']['open'] = array( '#type' => 'submit', '#name' => 'Open', '#value' => t('Open Registrations'), );
  }
  if (in_array('Lock', $options)) {
    $form['actions']['lock'] = array( '#type' => 'submit', '#name' => 'Locked', '#value' => t('Lock'), );
  }
  if (in_array('Activate', $options)) {
    $form['actions']['activate'] = array( '#type' => 'submit', '#name' => 'Active', '#value' => t('Activate'), );
  }
  if (in_array('Close', $options)) {
    $form['actions']['close'] = array( '#type' => 'submit', '#name' => 'Closed', '#value' => t('Close'), );
  }
  if (in_array('Archive', $options)) {
    $form['actions']['archive'] = array( '#type' => 'submit', '#name' => 'Archived', '#value' => t('Archive'), );
  }
  if (in_array('Restore', $options)) {
    $form['actions']['close'] = array( '#type' => 'submit', '#name' => 'Closed', '#value' => t('Restore'), );
  }
  return $form;
}

/**
 * Validates the ult_registration_changestatus_form form
 */
function ult_registration_changestatus_form_validate($form, &$form_state) {
  // Check current status and target status, and see if valid path
  // Check that the node is a valid node type
}

/**
 * Processes the submitted ult_registration_changestatus_form form
 */
function ult_registration_changestatus_form_submit($form, &$form_state) {
  $nid = check_plain($form_state['values']['nodeid']);
  $status = check_plain($form_state['clicked_button']['#name']);
  $nodeitem = node_load($nid);
  $nodeitem->field_ult_leaguestatus[0]['value'] = $status;
  node_save($nodeitem);
  // If League Item, set state on all divisions as well.
  if (($nodeitem->type == 'ult_league') && (module_exists('ult_divisions'))) {
    // Get all divisions for a given league
    $sql = "Select nid from {content_type_ult_division} where field_ult_divisionleague_nid = %d";
    $result = db_query($sql, $nodeitem->nid);
    $divisions = array();
    while ($data = db_fetch_object($result)) {
      $divisions[] = $data->nid;
    }
    // Reset all their statuses as well.
    foreach ($divisions as $div) {
      $divnodeitem = node_load($div);
      $divnodeitem->field_ult_leaguestatus[0]['value'] = $status;
      node_save($divnodeitem);
    }
  }
  if ($nodeitem->type == 'ult_division') {
    $form_state['redirect'] = "ult/division/" . $nid . "/regstatus";
  }
  elseif ($nodeitem->type == 'ult_league') {
    $form_state['redirect'] = "ult/league/" . $nid . "/regstatus";
  }
}

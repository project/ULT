<?php

/**
 * @file
 * Administrative settings for the Ult_Registration module
 */

/**
 * Implements hook_form().
 *
 * @return
 *   An array containing the form elements to be displayed in the settings form.
 */
function ult_registration_config_form() {
  $form = array();
  $form['ult'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Registration settings'),
  );
  // Enforce registration deadlines
  $options = array('0' => t('Disable registration deadlines'), '1' => t('Enforce deadlines for initial registration'), '2' => t('Enforce deadline for final registration'));
  $description = t('Determines whether to enable registration deadlines, and whether to the deadline applies to the initial or final registration step.');
  $form['ult']['ult_register_deadlines'] = array(
    '#type' => 'radios',
    '#title' => t('Enforce registration deadlines'),
    '#options' => $options,
    '#default_value' => variable_get('ult_register_deadlines', 0),
    '#description' => $description,
  );
  // Allow admins to track 'payment' status
  $description = t('Determines whether to allow administrators to (manually) track whether a team has paid a registration fee.');
  $form['ult']['ult_register_payments'] = array(
    '#type' => 'checkbox',
    '#title' => t('Track payment of registration fees'),
    '#default_value' => variable_get('ult_register_payments', 1),
    '#description' => $description,
  );
  // Registration Prerequisites container
  $form['ult']['ult_register_prereqs'] = array( '#type' => 'fieldset', '#title' => 'Registration Prerequisites', '#collapsible' => FALSE, );

  // Require administrator approval of registrations
  $description = t('Determines whether administrator approval is required before teams are can be considered fully registered.');
  $description .= '<br />';
  $description .= t('Recommend that this remain checked if there is an upper limit to the number of teams allowed to register in each league/division');
  $form['ult']['ult_register_prereqs']['ult_register_approval'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require administrator approval before finalizing registration'),
    '#default_value' => variable_get('ult_register_approval', 1),
    '#description' => $description,
  );

  // Require teams to be paid before finalizing registration
  $description = t('If the "Track Payments" feature is enabled, this setting determines whether teams must be marked as "paid" before finalizing registration.');
  $form['ult']['ult_register_prereqs']['ult_register_requirepayment'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require registration fee payment before finalizing registration'),
    '#default_value' => variable_get('ult_register_requirepayment', 1),
    '#description' => $description,
  );
  if (module_exists('ult_rosters')) {
    // Require finalize rosters before finalizing registration
    $description = t('Determines whether users must submit a finalized roster before finalizing registration.');
    $description .= '<br />';
    $description .= t('Note:  "Require teams to submit final rosters" must also be checked on the Team settings page.');
    $form['ult']['ult_register_prereqs']['ult_register_requireroster'] = array(
      '#type' => 'checkbox',
      '#title' => t('Require final roster submissions before finalizing registration'),
      '#default_value' => variable_get('ult_register_requireroster', 0),
      '#description' => $description,
    );
  }
  return system_settings_form($form);
}


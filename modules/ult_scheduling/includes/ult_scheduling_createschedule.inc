<?php


/**
 * @file
 * Schedule Creation functions related to the ULT_scheduling module.
 */

// ********************************************************
// *                                                      *
// *                 Page Definitions                     *
// *                                                      *
// ********************************************************
/**
 * Generates the 'create schedule' page
 */
function ult_scheduling_createschedule_page($node) {
  if (($node->type != "ult_league") && ($node->type != "ult_division")) {
    // We know checktype will fail, but I'm too lazy to duplicate the error code here.
    _ult_main_checktype($node, 'ult_league');
  }
  // Load Javascript
  drupal_add_js(drupal_get_path('module', 'ult_scheduling') . '/ult_scheduling.js');
  // Create variable to hold available field listing
  $availfields = FALSE;
  // Check if per-league/division assignments are enabled
  if (variable_get('ult_fieldassignments', 1) == 1) {
    // Get all fields for a given league/division
    $availfields = _ult_fields_getallfields($node->nid);
  }
  // Make sure we have fields
  if ($availfields === FALSE) {
    // No fields assigned to division, assume all fields on site
    $availfields = _ult_fields_getallfields();
    if ($availfields === FALSE) {
      // No fields on site ... Print status message and return.
      $output .= _ult_main_returnlink($node);
      $output .= t("No fields found.  Please create at least one field on the site before attempting to create a schedule.");
      return $output;
    }
  }
  // Create Header for Schedule Table
  $header = array(t('Date/Time'));
  // Build field keys for use in retrieving timeslots, and fill header
  foreach ($availfields as $key => $field) {
    $newkey = $field['nid'] . ' ' . $field['ultfield'];
    $fields[$newkey] = array(
      'fid' => $key,
      'key' => $newkey,
      'sitenid' => $field['nid'],
      'fieldnum' => $field['ultfield'],
      'title' => $field['site']
    );
    $fieldkeys[] = $newkey;
    $header[$newkey] = array( 'data' => $field['site'] . '<br />' . t('Field ') . $field['ultfield'], 'class' => 'ult-scheduling-field-' . $field['nid'] . '-' . $field['ultfield']);
  }
  // Get all teams for a division
  $teams = _ult_main_getteamsbyleague($node->nid);
  // Check that we have teams
  if (count($teams) < 2) {
    // If this is a league, and we have divisions, load teams from all divisions
    if (module_exists('ult_divisions') && _ult_divisions_containsdivision($node->nid)) {
      // Get divisions
      $divisions = ult_divisions_getdivsbyleague($node->nid);
      // Load teams from all divisions
      foreach ($divisions as $nid => $title) {
        $teams += _ult_main_getteamsbyleague($nid);
      }
    }
  }
  if (count($teams) < 2) {
    // Not enough teams to schedule a game.
    drupal_set_message(t('At least two teams are required in order to create a schedule.'));
    if ($node->type == 'ult_league') {
      drupal_goto('ult/league/' . $node->nid);
    }
    else {
      drupal_goto('ult/division/' . $node->nid);
    }
  }
  // Retrieve timeslots
  $sql = "Select * from {ult_timeslots} where CONCAT(fieldnid, ' ', fieldnum) IN (" . db_placeholders($fieldkeys, 'varchar') . ") and (start > %d) ORDER BY start ASC, gamenid DESC";
  $result = db_query($sql, $fieldkeys, time());
  while ($data = db_fetch_array($result)) {
    $tskey = $data['start'];
    if (!(isset($timeslots[$tskey]))) {
      // New timeslot
      $timeslots[$tskey] = array(
        'key' => $tskey,
        'datestring' => date('l, M dS', $data['start']) . '<br />' . date('g:i A', $data['start']),
        'fields' => $fields,
        'teams' => $teams,
        'unavail' => array(),
      );
    }
    // Add game/field/unavailable team status to timeslots array
    $timeslots[$data['start']]['tsid'] = $data['ts_id'];
    $timeslots[$data['start']]['fields'][$data['fieldnid'] . ' ' . $data['fieldnum']]['tsid'] = $data['ts_id'];
    $fieldkey = $data['fieldnid'] . ' ' . $data['fieldnum'];
    if (is_null($data['gamenid'])) {
      $timeslots[$data['start']]['fields'][$fieldkey]['hasgame'] = FALSE;
    }
    else {
      $timeslots[$data['start']]['fields'][$fieldkey]['hasgame'] = TRUE;
      $timeslots[$data['start']]['fields'][$fieldkey]['gamenid'] = $data['gamenid'];
      $gamenode = node_load($data['gamenid']);
      $hometeamnid = $gamenode->field_ult_gamehometeam[0]['nid'];
      $awayteamnid = $gamenode->field_ult_gameawayteam[0]['nid'];
      $timeslots[$data['start']]['fields'][$fieldkey]['hometeamnid'] = $hometeamnid;
      $timeslots[$data['start']]['fields'][$fieldkey]['awayteamnid'] = $awayteamnid;
      $timeslots[$data['start']]['unavail'][$hometeamnid] = $hometeamnid;
      $timeslots[$data['start']]['unavail'][$awayteamnid] = $awayteamnid;
    }
  }
  // Ensure we have timeslots
  if (!isset($timeslots)) {
    // No timeslots available.  Must be no open timeslots
    $output .= _ult_main_returnlink($node);
    $output .= t("No timeslots found.");
    $output .= "<br /><br />";
    $output .= t("Creating a schedule requires at least one field with available timeslots.");
    if (user_access('administer ULT timeslots')) {
      $output .= "<br /><br />";
      $output .= t("To create timeslots, please visit ");
      $output .= l('ult/fields/timeslots', 'ult/fields/timeslots');
    }
    return $output;
  }
  // Ensure at least one timeslot exists for a given field
  foreach ($availfields as $fieldkey => $field) {
    $fieldkey = $field['nid'] . ' ' . $field['ultfield'];
    // Set 'has timeslot' flag
    $hastimeslot = FALSE;
    // Loop through timeslots
    foreach ($timeslots as $key => $timeslot) {
      // Check if field exists for that timeslot
      if (isset($timeslot['fields'][$fieldkey]['tsid'])) {
        // If so, trip flag and exit
        $hastimeslot = TRUE;
        break;
      }
    }
    if (!$hastimeslot) {
      // If no timeslots for that field, crawl through rows, unsetting this field column in each
      unset($header[$fieldkey]);
      foreach ($timeslots as $key => $timeslot) {
        unset($timeslots[$key]['fields'][$fieldkey]);
      }
    }
  }
  // Check to ensure we still have fields with open timeslots
  if (count($header) < 2) {
    // No fields left.  Must be no open timeslots (we checked the 'no fields' situation earlier)
    $output .= _ult_main_returnlink($node);
    $output .= t("No timeslots found.");
    $output .= "<br /><br />";
    $output .= t("Creating a schedule requires at least one field with available timeslots.");
    if (user_access('administer ULT timeslots')) {
      $output .= "<br /><br />";
      $output .= t("To create timeslots, please visit ");
      $output .= l('ult/fields/timeslots', 'ult/fields/timeslots');
    }
    return $output;
  }
  // Create Table
  $tablerow = array();
  foreach ($timeslots as $key => $timeslot) {
    // Create table row
    $tablerow[$key] = array(
      'class' => 'ult_schedule_addgame_row',
      'id' => 'ult-schedule-addgame-row-' . $key,
      'data' => array('0' => $timeslot['datestring']),
    );
    // Create individual table cells
    foreach ($timeslot['fields'] as $fieldkey => $field) {
      $tablerow[$key]['data'][$fieldkey] = array(
        'id' => 'ult_schedule_addgame_' . $key . '_' . $field['sitenid'] . '_' . $field['fieldnum'],
        'class' => 'ult_gameslot ult_visible ult-scheduling-field' . '-' . $field['sitenid'] . '-' . $field['fieldnum'],
        //'data' => ult_scheduling_addgame_render_cellcontents($timeslot, $fieldkey),
        'data' => drupal_get_form('ult_scheduling_addgamepage_form_' . $key . '_'. $field['sitenid'] . '_' . $field['fieldnum'], $fieldkey, $timeslot),
      );
    }
  }
  // Render Page Output
  $output .= _ult_main_returnlink($node);
  $output .= drupal_get_form('ult_scheduling_scheduleactions_form', $node->nid, $timeslots);
  $output .= theme('table', $header, $tablerow);
  return $output;
}

/**
 * Implements hook_form().
 *
 * @param &$node
 *   The node being added or edited.
 * @param $form_state
 *   The form state array.
 *
 * @return
 *   An array containing the form elements to be displayed in the node edit form.
 */
function ult_scheduling_addgamepage_form($form_state, $fieldkey, $timeslot) {
  // If passed a timeslot in storage, use that instead
  //if (isset($form_state['storage']['timeslot'])) {
  //  $timeslot = $form_state['storage']['timeslot'];
  //}
  // Load Timeslot to see if status has changed
  $ts = ult_timeslots_gettimeslot($timeslot['fields'][$fieldkey]['tsid']);
  // Set 'hasgame' flag
  if (isset($ts['gamenid'])) {
    if (($timeslot['fields'][$fieldkey]['hasgame'] == FALSE)
    || ($ts['gamenid'] != $timeslot['fields'][$fieldkey]['gamenid'])) {
      // New game added
      $timeslot['fields'][$fieldkey]['hasgame'] = TRUE;
      $timeslot['fields'][$fieldkey]['gamenid'] = $ts['gamenid'];
      $teams = ult_scheduling_getteamsbygame($ts['game_id']);
      $timeslot['fields'][$fieldkey]['hometeamnid'] = $teams[0];
      $timeslot['fields'][$fieldkey]['awayteamnid'] = $teams[1];
      $timeslot['unavail'][$teams[0]] = $teams[0];
      $timeslot['unavail'][$teams[1]] = $teams[1];
    }
  }
  elseif (!(isset($ts['gamenid'])) && ($timeslot['fields'][$fieldkey]['hasgame'] == TRUE)) {
    $timeslot['fields'][$fieldkey]['hasgame'] = FALSE;
    unset($timeslot['fields'][$fieldkey]['gamenid']);
    unset($timeslot['unavail'][$timeslot['fields'][$fieldkey]['hometeamnid']]);
    unset($timeslot['unavail'][$timeslot['fields'][$fieldkey]['awayteamnid']]);
  }
  $field = $timeslot['fields'][$fieldkey];
  if (count($timeslot['unavail']) > 0) {
    $teams = array_diff_key($timeslot['teams'], $timeslot['unavail']);
  }
  else {
    $teams = $timeslot['teams'];
  }
  $form = array();
  $form['#cache'] = TRUE;
  // Set the action for the form (used in ahah callback to prevent AHAH from overwriting it during the ahah callback function)
  $form['#action'] = request_uri();
  $form['uri'] = array(
    '#type' => 'value',
    '#value' => request_uri(),
  );
  $form['tsid'] = array(
    '#type' => 'value',
    '#value' => $field['tsid'],
  );
  $form['fieldkey'] = array(
    '#type' => 'value',
    '#value' => $fieldkey,
  );
  if ($field['hasgame']) {
    $form_state['storage']['operation'] = 'delete';
    // Generate game details
    $gamenode = node_load($field['gamenid']);
    if ($gamenode->status == 0) {
      $content = l($gamenode->title, 'ult/game/' . $gamenode->nid) . '<br />(Pending)<br />';
    }
    else {
      $content = '<b>' . l($gamenode->title, 'ult/game/' . $gamenode->nid) . '</b><br />';
    }
    $form['gameinfo'] = array(
      '#type' => 'markup',
      '#value' => $content,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    // No AHAH - first need some way to update the rest of the select boxes to reflect the teams are now available;
    // As well as a solution for why the submit buttons are losing their 'AHAH' functionality the second time around
      //'#ahah' => array(
      //  'path' => 'ult/createschedule/addgame/callback',
      //  'wrapper' => 'ult_schedule_addgame_' . $timeslot['key'] . '_' . $field['sitenid'] . '_' . $field['fieldnum'],
      //),
    );
  }
  elseif (count($teams) > 1) {
    $form_state['storage']['operation'] = 'add';
    // Generate addgame form
    $teamnids = array_keys($teams);
    $defaulthome = array_shift($teamnids);
    $defaultaway = array_shift($teamnids);
    unset($teamnids);
    $form['hometeam'] = array(
      '#type' => 'select',
      '#title' => t('Home'),
      '#options' => _ult_main_convert_options_to_text($teams),
      '#required' => TRUE,
      '#default_value' => isset($form_state['values']['hometeam']) ? $form_state['values']['hometeam'] : $defaulthome,
    );
    $form['awayteam'] = array(
      '#type' => 'select',
      '#title' => t('Away'),
      '#options' => _ult_main_convert_options_to_text($teams),
      '#required' => TRUE,
      '#default_value' => isset($form_state['values']['awayteam']) ? $form_state['values']['awayteam'] : $defaultaway,
    );
    $form['operation'] = array(
      '#type' => 'value',
      '#value' => 'add',
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
      '#ahah' => array(
        'path' => 'ult/createschedule/addgame/callback',
        'wrapper' => 'ult_schedule_addgame_' . $timeslot['key'] . '_' . $field['sitenid'] . '_' . $field['fieldnum'],
      ),
    );
  }
  else {
    unset($form_state['storage']['operation']);
    // No teams available - skip form generation
    $form['gameinfo'] = array(
      '#type' => 'markup',
      '#value' => t('No teams available'),
    );
  }
  // Put timeslot in storage for use in 'submit'
  $form_state['storage']['timeslot'] = $timeslot;
  $form['#submit'][] = 'ult_scheduling_addgamepage_form_submit';
  $form['#validate'][] = 'ult_scheduling_addgamepage_form_validate';
  return $form;
}

/**
 * Custom AHAH callback for the 'create schedule' form
 *
 * Used to replace the add game form with game details when an 'add game'
 *   button is clicked
 */
function ult_scheduling_addgame_ahah_callback() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  drupal_process_form($form_id, $form, $form_state);
  $actionuri = $form_state['values']['uri'];
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  $form['#action'] = $actionuri;
  $output .= theme('status_messages') . drupal_render($form);
  print drupal_json(array('status' => TRUE, 'data' => $output));
  exit();
}

/**
 * Validates the ult_scheduling_addgamepage_form form
 *
 * @param $form form to validate
 * @param $form_state A keyed array containing the current state of the form.
 */
function ult_scheduling_addgamepage_form_validate($form, &$form_state) {
  if ($form_state['storage']['operation'] == 'add') {
    // Validate Adding Game
    $hometeam = $form_state['values']['hometeam'];
    $awayteam = $form_state['values']['awayteam'];
    // Make sure two different teams are entered
    if ($hometeam == $awayteam) {
      form_set_error('form', t('A team cannot play itself!'));
      return;
    }
    // Make sure both teams are available
    $sql = "Select gamenid from {ult_timeslots} where start = %d and gamenid IS NOT NULL";
    $result = db_query($sql, $form_state['storage']['timeslot']['key']);
    while ($data = db_fetch_array($result)) {
      $teams = ult_scheduling_getteamsbygame($data['gamenid']);
      if (in_array($hometeam, $teams)) {
        form_set_error('form', t('Team is already scheduled for this timeslot'));
        return;
      }
      elseif (in_array($awayteam, $teams)) {
        form_set_error('form', t('Team is already scheduled for this timeslot'));
        return;
      }
    }
  }
  elseif ($form_state['storage']['operation'] == 'delete') {
    // Validate Deleting Game
  }
}

/**
 * Process submitted ult_scheduling_addgamepage_form form
 *
 * @param $form
 *   form to submit
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function ult_scheduling_addgamepage_form_submit($form, &$form_state) {
  $timeslot = $form_state['storage']['timeslot'];
  $fieldkey = $form_state['values']['fieldkey'];
  $field = $timeslot['fields'][$fieldkey];
  if ($form_state['storage']['operation'] == 'add') {
    // Add Game
    $homenid = check_plain($form_state['values']['hometeam']);
    $awaynid = check_plain($form_state['values']['awayteam']);
    $homename = $form_state['storage']['timeslot']['teams'][$homenid];
    $awayname = $form_state['storage']['timeslot']['teams'][$awaynid];
    $ts_id = check_plain($form_state['values']['tsid']);
    // Create ult_match node item.
    global $user;
    module_load_include('inc', 'node', 'node.pages');
    $node->type = 'ult_game';
    node_object_prepare($node);
    $node->uid = $user->uid;
    $node->title = drupal_html_to_text($homename . ' vs. ' . $awayname);
    $node->body = "";
    $node->teaser = "";
    $node->filter = variable_get('filter_default_format', 1);
    $node->format = FILTER_FORMAT_DEFAULT;
    $node->language = '';
    $node->revision = 0;
    $node->promote = 0;
    $node->created = time();
    $node->comment = 0;
    $node->field_ult_gamehometeam[0]['nid'] = $homenid;
    $node->field_ult_gameawayteam[0]['nid'] = $awaynid;
    $node->field_ult_gameresult[0]['value'] = 'No';
    $node->field_ult_gamedate[0]['value'] = date_format_date(date_make_date($timeslot['key'], NULL, DATE_UNIX), 'custom', 'Y-m-d H:i');
    // Determine game 'parent' league/division
    $sql = "Select field_ult_teamparent_nid from {content_type_ult_team} where nid = %d";
    $league = db_result(db_query_range($sql, $homenid, 0, 1));
    $node->field_ult_gameleague[0]['nid'] = $league;
    $node->field_ult_gamefieldsite[0]['nid'] = $field['sitenid'];
    $node->field_ult_gamefield[0]['value'] = $field['fieldnum'];
    $node->status = 0;
    node_save($node);

    // Update the timeslot table
    $result = FALSE;
    if ($updatetimeslot = ult_timeslots_gettimeslot($ts_id)) {
      $updatetimeslot['gamenid'] = $node->nid;
      $updatetimeslot['leaguenid'] = $league;
      $updatetimeslot['status'] = 'Pending';
      $result = drupal_write_record('ult_timeslots', $updatetimeslot, 'ts_id');
    }
    // If timeslot retrieve/update fails, delete the unpublished game
    if (!$result) {
      drupal_set_message(t('Error encountered while saving game'), 'error');
      node_delete($node->nid);
      watchdog('ult_scheduling', t('Error encountered while adding game - updating timeslot failed for timeslot: ') . $ts_id);
    }

    //Put the new node id into $form_state['storage']
    $form_state['storage']['newgamenid'] = $node->nid;

    //Update $timeslot variable in $form_state['storage']
    $field['hasgame'] = TRUE;
    $field['hometeamnid'] = $homenid;
    $field['awayteamnid'] = $awaynid;
    $field['gamenid'] = $node->nid;
    $timeslot['fields'][$fieldkey] = $field;

    // Mark teams as unavailable
    $timeslot['unavail'][$homenid] = $homenid;
    $timeslot['unavail'][$awaynid] = $awaynid;

    $form_state['storage']['timeslot'] = $timeslot;
  }
  elseif ($form_state['storage']['operation'] = 'delete') {
    $gamenid = $field['gamenid'];
    node_delete($gamenid);
    // Clean up form_state['storage']
    unset($form_state['storage']['newgamenid']);
    // Remove teams from 'unavail' array
    unset($timeslot['unavail'][$field['hometeamnid']]);
    unset($timeslot['unavail'][$field['awayteamnid']]);
    // Update 'hasgame' and associated nids
    $field['hasgame'] = FALSE;
    unset($field['hometeamnid']);
    unset($field['awayteamnid']);
    unset($field['gamenid']);
    $timeslot['fields'][$fieldkey] = $field;
    $form_state['storage']['timeslot'] = $timeslot;
  }
  //unset($form_state['storage']['operation']);
  unset($form_state['storage']);
  $form_state['rebuild'] = FALSE;
}

/**
 * Implements hook_form().
 *
 * @param &$form_state
 *   The form state array.
 * @param $leagueid
 *   The id of the division/league associated with this form
 *
 * @return
 *   An array containing the form elements to be displayed in the form.
 */
function ult_scheduling_scheduleactions_form($form_state, $league_id, $timeslots) {
  $form['actions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Schedule Actions'),
    '#collapsible' => FALSE,
  );
  $form['actions']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset All Games'),
    '#name' => 'Reset',
  );
  $form['actions']['publish'] = array(
    '#type' => 'submit',
    '#value' => t('Publish Schedule'),
    '#name' => 'Publish',
  );
  $form['actions']['league'] = array(
    '#type' => 'hidden',
    '#default_value' => $league_id,
  );
  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Schedule Filters'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array(
      'class' => 'ult-scheduling-addgame-filter ult-hidden',
    ),
  );
  $timeslot_keys = array_keys($timeslots);
  $fields = $timeslots[$timeslot_keys[0]]['fields'];
  // Generate Field checkboxes (used to filter)
  $options = array();
  foreach ($fields as $fid => $field) {
    $options[$field['sitenid'] . '-' . $field['fieldnum']] = ult_fieldsite_theme($field['sitenid'], FALSE) . ' (Field ' . $field['fieldnum'] . ')';
    $defaults[] = $field['sitenid'] . '-' . $field['fieldnum'];
  }
  $form['filter']['filterbyfieldsite'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Filter by Fieldsite'),
    '#options' => $options,
    '#default_value' => $defaults,
    '#columns' => 4,
    '#theme' => 'ult_checkboxcolumns',
    '#attributes' => array(
      'class' => 'ult-filterfield-checkbox',
    ),
  );
  // Generate Timeslot checkboxes (used to filter)
  $options = array();
  foreach ($timeslots as $key => $timeslot) {
    //$options[$key] = date('l, M dS', $key) . '<br />' . date('g:i A', $key);
    $options[$key] = format_date($key, 'short');
    $defaults[] = $key;
  }
  $form['filter']['filterbytimeslot'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Filter by Timeslot'),
    '#options' => $options,
    '#default_value' => $defaults,
    '#columns' => 3,
    '#theme' => 'ult_checkboxcolumns',
    '#attributes' => array(
      'class' => 'ult-filtertimeslot-checkbox',
    ),
  );
  return $form;
}

/**
 * Processes the submitted 'ult_scheduling_scheduleactions_form' form
 *
 * Collects all unpublished games in this league belonging to the current user and
 *   publishes them (if action = 'Publish'), or deletes them (if action = 'Delete')
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function ult_scheduling_scheduleactions_form_submit($form, &$form_state) {
  global $user;
  // Get clicked button
  $action = $form_state['clicked_button']['#name'];
  $div_id = $form_state['values']['league'];
   // Get all unpublished games in this division
  $sql = "Select a.nid as game_id from {content_type_ult_game} a, {node} b where a.field_ult_gameleague_nid = %d and b.status = 0 and b.uid = %d and a.nid = b.nid";
  $result = db_query($sql, $div_id, $user->uid);
  $added = 0;
  $deleted = 0;
  while ($data = db_fetch_object($result)) {
    if ($action == 'Reset') {
      // Delete all unpublished games in this division and reset the associated gameslots
      node_delete($data->game_id);
      $deleted++;
    }
    elseif ($action == 'Publish') {
      // Set all unpublished games in this division to 'published'
      $node = node_load($data->game_id);
      $node->status = 1;
      node_save($node);
      $added++;
    }
  }
  if ($action == 'Publish') {
    drupal_set_message(t("!count games published.", array('!count' => $added)));
  }
}

<?php

/**
 * @file
 * Administrative settings for the ult_scheduling module
 */

/**
 * Implementation of hook_form().
 *
 * @return An array containing the form elements to be displayed in the settings form.
 */
function ult_scheduling_config_form() {
  $form = array();
  $form['scheduling'] = array(
    '#type' => 'fieldset',
    '#title' => t('ULT Scheduling settings'),
  );
  return system_settings_form($form);
}

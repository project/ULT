/**
 * Javascript include file for the 'create schedule' page
 * - Jquery functions
 */

Drupal.behaviors.ultScheduling = function(context) {
  // Show hidden filter form
  $(".ult-scheduling-addgame-filter").removeClass('ult-hidden');
  var leagueId = $(location).attr('href').split("/createschedule").shift().split("/").pop();
  // Set initial checkbox state
  $("input[type='checkbox'].ult-filterfield-checkbox").each(
    function() {
	  var fieldId = $(this).val();
	  var clearCheckbox = window.sessionStorage;
	  $(this).click(
    	function() {
    	  var fieldId = $(this).val();
          if ($(this).is(':checked')) {
    	    $(".ult-scheduling-field-"+fieldId).removeClass('ult-hidden').addClass('ult-visible');
    	    clearCheckbox['ult_createschedulefilter_'+leagueId+'_'+fieldId] = 'visible';
    	  }
    	  else {
    	    $(".ult-scheduling-field-"+fieldId).removeClass('ult-visible').addClass('ult-hidden');
    	    clearCheckbox['ult_createschedulefilter_'+leagueId+'_'+fieldId] = 'hidden';
    	  }
        }
      );
      if (clearCheckbox['ult_createschedulefilter_'+leagueId+'_'+fieldId] == 'hidden') {
       	$(this).attr('checked', false);
       	$(".ult-scheduling-field-"+fieldId).removeClass('ult-visible').addClass('ult-hidden');
      }
      else {
      	$(this).attr('checked', true);
      	$(".ult-scheduling-field-"+fieldId).removeClass('ult-hidden').addClass('ult-visible');
       	clearCheckbox['ult_createschedulefilter_'+leagueId+'_'+fieldId] = 'visible';
      }
    }
  );
  // Hide/Show timeslot when checkbox checked
  $("input[type='checkbox'].ult-filtertimeslot-checkbox").each(
    function() {
      var timeslotId = $(this).val();
      var clearCheckbox = window.sessionStorage;
      $(this).click(
    	function() {
          if ($(this).is(':checked')) {
    		$("#ult-schedule-addgame-row-"+timeslotId).removeClass('ult-hidden').addClass('ult-visible');
    		clearCheckbox['ult_createschedulefilter_'+leagueId+'_'+timeslotId] = 'visible';
          }
	      else {
    		$("#ult-schedule-addgame-row-"+timeslotId).removeClass('ult-visible').addClass('ult-hidden');
    		clearCheckbox['ult_createschedulefilter_'+leagueId+'_'+timeslotId] = 'hidden';
          }
    	}
      );
      if (clearCheckbox['ult_createschedulefilter_'+leagueId+'_'+timeslotId] == 'hidden') {
    	$(this).attr('checked', false);
    	$("#ult-schedule-addgame-row-"+timeslotId).removeClass('ult-visible').addClass('ult-hidden');
      }
      else {
    	$(this).attr('checked', true);
    	$(".ult-schedule-addgame-row-"+timeslotId).removeClass('ult-hidden').addClass('ult-visible');
    	clearCheckbox['ult_createschedulefilter_'+leagueId+'_'+timeslotId] = 'visible';
      }
    }
  );
}

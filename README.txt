*** Project Summary ***

ULT stands for 'Ultimate League Tool', a suite of custom modules used to
simplify the day-to-day management and operations of amateur sport leagues.

The tool (rather ambitiously) attempts to provide solutions for all aspects of
a league; including player registration, team roster management, game creation,
scheduling, field assignment, standings, online score submissions, and
communications tools. For a complete feature list, view this post in the Sports
Club (League) Management group on g.d.o.


*** History ***

ULT was first developed in the Spring of 2010 to organize and manage the
various leagues put on by the 'Regina Ultimate Flying Disc Club' (also known as
RUFDC). The module was put into production on the RUFDC website in April, and
used to manage two four-tier leagues for both the Spring and Summer seasons ...
consisting of over 700 players, almost 50 teams, and 431 games. The code posted
here is the second generation rewrite of that original module.

Development of the initial ULT module suite was sponsored by RUFDC, on the
condition that the finished product be made open-source (to ensure the
organization was not left stranded if they lost access to the primary
developer). Further inspiration came from the standalone 'LeagueRunner' project
current being used by other Ultimate Frisbee leagues across Canada.


*** Components ***

The following modules are currently included in the core 'ult_main'
distribution:

    ult_main:
        Core module
        Creates ult_league and ult_team content types
        Requires cck, views, text, optionwidgets, nodereference, and
          userreference modules
    ult_divisions:
        Adds support for 'multiple divisions/tiers' per league
        Creates ult_division content type
        Requires ult_main module
    ult_matches:
        Adds support for tracking individual matches between teams
        Creates ult_game and ult_gameresult content types
        Requires ult_main and date_popup modules
    ult_fields:
        Adds support for field/facility assignment and tracking
        Creates ult_fieldsite content type
        Requires ult_main, gmapfield, and location modules
    ult_profiles:
        Adds support for individual 'player profiles'
        Creates ult_profile content type
        Requires ult_main, content_profile, content_profile_registration,
          fieldgroup, and date modules
    ult_registration:
        Adds support for tracking status of team registrations
        Adds fields related to registration to the ult_division, ult_league,
          and ult_team content types
        Requires ult_main and date_popup modules
    ult_rosters:
        Adds support for tracking and management of team rosters
        Adds ult_roster content type
        Requires ult_main module
    ult_timeslots:
        Adds support for tracking 'game timeslots', which represent times
          available for scheduling games
        Adds ult_timeslot tables to database
    ult_scheduling:
        Adds ability to schedule multiple games in a single view for a given
          division/league
    ult_waivers:
        Adds display and tracking of player waiver forms before a player is
          added to a team
        Requires ult_main and ult_roster modules
    ult_logos:
        Adds support for league, division, and team logo images
        Requires ult_main, imagecache, and imagefield modules
    ult_spirit:
        Adds support for ultimate-frisbee 'spirit score' tracking
        Supports 'score out of 10', or customized 'survey' spirit scores
        Requires ult_main and ult_matches modules


*** Additional Module Support ***

ULT will also utilize functionality from the 'Contact' and 'RealName' modules,
if installed.


*** Installation Notes ***

A couple of notes regarding installation:

    1. ULT leverages code from the 'Install Profiles API' module to
       automatically generate the various content types it requires. Please
       ensure the 'Install Profiles API' exists on your server before
       attempting to activate any ULT components. (Note: The 'Install Profiles
       API' code must exist on your server, but the module does not need to be
       'enabled'.)

    2. To ensure that content types are created successfully, please enable any
       of the cck field module dependencies before enabling any ULT components.
       Attempting to enable them simultaneously may result in a
       REQUIREMENTS_ERROR pop-up, which will prevent the installation of the ULT
       module. If this occurs, simply re-enable the desired ULT component.
